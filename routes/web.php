<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\MusicController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\ArtistController;
use App\Http\Controllers\CarouselController;
use App\Http\Controllers\CatalogueController;
use App\Http\Controllers\ComposerController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\LabelController;
use App\Http\Controllers\PlayerController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\HeaderMusicController;
use App\Http\Controllers\HeaderComposerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('', [HomeController::class, 'index'])->name('index');

Route::get('/search/{search}', [SearchController::class, 'index'])->name('search');
Route::get('/composer/{page}/{shorts}', [CatalogueController::class, 'composer'])->name('composer2');
Route::get('/catalogue', [CatalogueController::class, 'indexTable'])->name('catalogue-table');
// Route::get('/catalogue/{page}', [CatalogueController::class, 'index'])->name('catalogue');
Route::get('/player/{id}/{play?}', [PlayerController::class, 'index'])->name('player');
Route::get('/article/{id}', [ArticleController::class, 'show'])->name('article-show');
Route::get('/page/{id}', [PagesController::class, 'show'])->name('page-show');

Route::get('/blog', function () {
return view('blog');
})->name('blog');

Route::get('/home', function () {
    return view('layouts/home');
})->name('home');

Route::get('/home2', function () {
    return view('home2');
})->name('home2');

Route::get('/catalogue2', function () {
    return view('catalogue2');
})->name('catalogue2');

Route::get('/hubungikami', function () {
    return view('hubungikami');
})->name('hubungi_kami');

Route::get('/video/{filename}', function ($filename) {
    $videosDir = base_path('public_html/app/video');
    if (file_exists($filePath = $videosDir."/".$filename)) {
        $stream = new \App\Http\VideoStream($filePath);

        return response()->stream(function() use ($stream) {
            $stream->start();
        });
    }else{
        \Log::debug("File doesn't exists - Video Stream");
    }
    return response("File doesn't exists", 404);
});


Route::get('/current-composer', function () {
    return view('current-composer');
})->name('current_composer');
// Route::get('/composer', function () {
//     return view('composer');
// })->name('composer2');
Route::get('/composer_share', function () {
    return view('composer_share');
})->name('composer_share');
Route::get('/slider', function () {
    return view('slider');
})->name('slider');

Route::get('/play/{music}', [MusicController::class, 'play'])->name('play');
Route::get('/plays/{music}', [MusicController::class, 'plays'])->name('plays');



Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/datatables', function () {
    return view('datatables');
})->name('datatables');

Route::post('/contact/post', [ContactController::class, 'create'])->name('contact-create');

Route::middleware(['auth:sanctum', 'verified'])->prefix('dashboard')->group(function () {

    Route::get('/user', [UsersController::class, 'index'])->name('user');
    Route::get('/user/create', [UsersController::class, 'create'])->name('user-create');
    Route::get('/user/update/{id}', [UsersController::class, 'update'])->name('user-update');
    Route::get('/user/read/{id}', [UsersController::class, 'read'])->name('user-read');
    Route::post('/user/delete', [UsersController::class, 'delete'])->name('user-delete');

    Route::get('/carousel', [CarouselController::class, 'index'])->name('carousel');
    Route::get('/carousel/create', [CarouselController::class, 'create'])->name('carousel-create');
    Route::get('/carousel/update/{id}', [CarouselController::class, 'update'])->name('carousel-update');
    Route::get('/carousel/read/{id}', [CarouselController::class, 'read'])->name('carousel-read');
    Route::post('/carousel/delete', [CarouselController::class, 'delete'])->name('carousel-delete');

    Route::get('/composer', [ComposerController::class, 'index'])->name('composer');
    Route::get('/composer/create', [ComposerController::class, 'create'])->name('composer-create');
    Route::get('/composer/update/{id}', [ComposerController::class, 'update'])->name('composer-update');
    Route::get('/composer/read/{id}', [ComposerController::class, 'read'])->name('composer-read');
    Route::post('/composer/delete', [ComposerController::class, 'delete'])->name('composer-delete');

    Route::get('/artist', [ArtistController::class, 'index'])->name('artist');
    Route::get('/artist/create', [ArtistController::class, 'create'])->name('artist-create');
    Route::get('/artist/update/{id}', [ArtistController::class, 'update'])->name('artist-update');
    Route::get('/artist/read/{id}', [ArtistController::class, 'read'])->name('artist-read');
    Route::post('/artist/delete', [ArtistController::class, 'delete'])->name('artist-delete');

    Route::get('/genre', [GenreController::class, 'index'])->name('genre');
    Route::get('/contact', [ContactController::class, 'index'])->name('contact');
    Route::post('/genre/delete', [GenreController::class, 'delete'])->name('genre-delete');

    Route::get('/label', [LabelController::class, 'index'])->name('label');
    Route::post('/label/delete', [LabelController::class, 'delete'])->name('label-delete');

    Route::get('/headermusic', [HeaderMusicController::class, 'index'])->name('headermusic');
    Route::post('/headermusic/delete', [HeaderMusicController::class, 'delete'])->name('headermusic-delete');

    Route::get('/headercomposer', [HeaderComposerController::class, 'index'])->name('headercomposer');
    Route::post('/headercomposer/delete', [HeaderComposerController::class, 'delete'])->name('headercomposer-delete');

    Route::get('/music', [MusicController::class, 'index'])->name('music');
    Route::get('/music/create', [MusicController::class, 'create'])->name('music-create');
    Route::get('/music/update/{id}', [MusicController::class, 'update'])->name('music-update');
    Route::get('/music/read/{id}', [MusicController::class, 'read'])->name('music-read');
    Route::post('/music/delete', [MusicController::class, 'delete'])->name('music-delete');

    Route::get('/article', [ArticleController::class, 'index'])->name('article');
    Route::get('/article/create', [ArticleController::class, 'create'])->name('article-create');
    Route::get('/article/update/{id}', [ArticleController::class, 'update'])->name('article-update');
    Route::get('/article/read/{id}', [ArticleController::class, 'read'])->name('article-read');

    Route::get('/page/update/{id}', [PagesController::class, 'update'])->name('page-update');

    /* Select2 */

    Route::get('/composer/select2', [ComposerController::class, 'select2'])->name('composer-select2');
    Route::get('/composer/select2/{musicid}', [ComposerController::class, 'select2premusic'])->name('composer-select2-music');

    Route::get('/artist/select2', [ArtistController::class, 'select2'])->name('artist-select2');
    Route::get('/artist/select2/{musicid}', [ArtistController::class, 'select2premusic'])->name('artist-select2-music');

    Route::get('/genre/select2', [GenreController::class, 'select2'])->name('genre-select2');
    Route::get('/genre/select2/{musicid}', [GenreController::class, 'select2premusic'])->name('genre-select2-music');

    Route::get('/label/select2', [LabelController::class, 'select2'])->name('label-select2');
    Route::get('/label/select2/{musicid}', [LabelController::class, 'select2premusic'])->name('label-select2-music');
    Route::get('/label/select2/composer/{labelid}', [LabelController::class, 'select2precomposer'])->name('label-select2-composer');

    Route::get('/headermusic/select2', [MusicController::class, 'select2HeaderMusic'])->name('headermusic-select2');
    Route::get('/headercomposer/select2', [ComposerController::class, 'select2HeaderComposer'])->name('headercomposer-select2');

    Route::get('/article/select2/{articleId}', [ArticleController::class, 'select2precomposer'])->name('article-select2-composer');

    /* Select2 */

    /* Article Image */
    
    Route::post('/article/upload/image', [ArticleController::class, 'uploadImage'])->name('article-upload-image');
    Route::post('/article/delete/image', [ArticleController::class, 'deleteImage'])->name('article-delete-image');

    /* Page Image */

    Route::post('/page/upload/image', [PagesController::class, 'uploadImage'])->name('page-upload-image');
    Route::post('/page/delete/image', [PagesController::class, 'deleteImage'])->name('page-delete-image');

    /* Article Image */

});

// Route::post('/check/email', [UsersController::class, 'checkEmail'])->name('check-email');
