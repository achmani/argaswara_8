const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/app.js", "public/js")
    .postCss("resources/css/app.css", "public/css", [require("postcss-import")])
    .css("resources/css/player_single.css", "public/css")
    .postCss("resources/css/login.css", "public/css", [
        require("postcss-import"),
        require("tailwindcss")
    ])
    .js("resources/js/front_end.js", "public/js")
    .css("resources/css/front_end.css", "public/css")
    .scripts(
        ["node_modules/ckeditor5-build-laravel-image/build/ckeditor.js"],
        "public/js/vendors.js"
    );
