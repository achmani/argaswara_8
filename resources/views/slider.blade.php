<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Slider</title>
    <link rel="stylesheet" href="{{asset('css/my.css')}}">
    <link rel="stylesheet" href="{{asset('css/front_end_2.css')}}">
    <link rel="stylesheet" href="../node_modules/slick-carousel/slick/slick-theme.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="../node_modules/jquery/dist/jquery.js"></script>
    <script src="../node_modules/slick-carousel/slick/slick.min.js"></script>
</head>
<style>
    *:focus {
        outline: none;
    }
    .circle-slider{
        padding-top: 100%;
        width: 100%;
        border-radius:50%; 
        background-color: green;
    }
    .player-slider{
        width: 100%;
        height: 100px;
        background-color: red;
        margin-top: 50px;
    }
    .wrapper-slider{
        position: relative;
        transition: transform 1s;
        width: 55%;
    }
    .item-slider{
        padding-top: 130px;
        display: flex !important;
        justify-content: center;
    }
    .slick-current > .wrapper-slider {
        transform: translateY(-130px);
    }
    .slick-dots{
        bottom: 80px;
    }
    .coba{
        width: 60%;
        height: 40px;
        position: relative;
        top: -100px;
    }
    @media(max-width:992px){
        .wrapper-slider{
            width: 75%;
        }
    }
    @media(max-width:768px){
        .player-slider{
            height: 80px;
        }
    }
</style>
<body>
    <div class="row mx-0" style="overflow: hidden !important">
        <div class="col-12 px-0">
            <div class="slick mt-3">
                <div class="item-slider">
                    <div class="wrapper-slider">
                        <div class="circle-slider"></div>
                        <div class="player-slider"></div>
                    </div>
                </div>       
                <div class="item-slider" >
                    <div class="wrapper-slider">
                        <div class="circle-slider"></div>
                        <div class="player-slider"></div>
                    </div>
                </div>       
                <div class="item-slider" >
                    <div class="wrapper-slider">
                        <div class="circle-slider"></div>
                        <div class="player-slider"></div>
                    </div>
                </div>       
                <div class="item-slider" >
                    <div class="wrapper-slider">
                        <div class="circle-slider"></div>
                        <div class="player-slider"></div>
                    </div>
                </div>    
            </div>
            <div class="coba d-flex align-items-center px-2 justify-content-between mx-auto">
                <div class="previous montserrat">< PREV</div>
                <div class="next montserrat">NEXT ></div>
            </div>   
        </div>
    </div>
   
    {{-- <div class="slick2" style="margin-top: 100px;">
        <div class="coba"></div>
        <div class="coba"></div>
        <div class="coba"></div>
        <div class="coba"></div>
        <div class="coba"></div>
    </div> --}}
    
  

</body>
<script>
    $('.slick').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        centerMode:true,
        centerPadding: '22%',
        autoplay:false,
        dots:false,
        appendArrows:'.coba',
        nextArrow:$('.next'),
        prevArrow:$('.previous'),
        responsive: [
            {
            breakpoint: 768,
            settings: {
                centerMode:false
            }
            }
        ]
    });
</script>
</html>