<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="turbo-cache-control" content="no-cache">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/ckeditor.css') }}" data-turbolinks-track="true">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/front_end.css') }}" data-turbolinks-track="true">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/front_end_2.css') }}" data-turbolinks-track="true">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/my.css') }}" data-turbolinks-track="true">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/my.css') }}" data-turbolinks-track="true">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/player_single.css') }}" data-turbolinks-track="true">
    <link rel="icon" type="image/ico" sizes="16x16" href="{{ asset('image/icon.ico') }}" data-turbolinks-track="true">
    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" data-turbolinks-track="true" data-turbo-track="reload"/> --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css" data-turbolinks-track="true" data-turbo-track="reload"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <script src="{{ asset('js/turbo.js') }}" data-turbolinks-track="true"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/amplitudejs@5.2.0/dist/amplitude.js" data-turbolinks-track="true" data-turbo-track="reload"></script>
    <script src="{{ asset('js/front_end.js') }}" data-turbolinks-track="true" data-turbo-track="reload"></script>
    <script src="{{ asset('js/my.js') }}" data-turbolinks-track="true" data-turbo-track="reload"></script>
    <script src="{{ asset('js/slick.min.js') }}" data-turbolinks-track="true" data-turbo-track="reload"></script>
    <script src = "https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer data-turbolinks-track="true" data-turbo-track="reload"></script>
    <script src = "https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js" defer data-turbolinks-track="true" data-turbo-track="reload"></script>
    {{--
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat%3A100%2C
    200%2C300%2C400%2C500%2C600%2C700%2C800%2C900%2C100italic%2C200italic%2C300italic%2C400italic%2
    C500italic%2C600italic%2C700italic%2C800italic%2C900italic%7CRoboto%3A100%2C300%2C400%2C500%2C700%2C900
    %2C100italic%2C300italic%2C400italic%2C500italic%2C700italic%2C900italic%7CPlayfair+Display%3A400%2C700%2
    C900%2C400italic%2C700italic%2C900italic&#038;subset=latin&#038;ver=1596446003"> --}}

    {{--
    <link rel="stylesheet" href="../node_modules/owl.carousel/dist/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="../node_modules/animate.css/animate.min.css" />
    <link rel="stylesheet" href="../node_modules/slick-carousel/slick/slick.css" />
    <link rel="stylesheet" href="../node_modules/slick-carousel/slick/slick-theme.css" />
    --}}
    {{-- <title>Argaswara - @yield('title')</title> --}}
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
    {!! JsonLd::generate() !!}
</head>

<body>
    <div class="container-fluid px-0" style="overflow: hidden">
        <div class="row mx-0">
            <div class="col-md-12">
                <div class="sidebar fixed-top" style="display:none;">
                    <a href="#" id="cross-sidebar">
                        <div class="fas fa-times"></div>
                    </a>
                    <div class="sidebar-container">
                        <div class="sidebar-item montserrat">
                            <a href="{{ route('index') }}">Home</a>
                            <div class="sidebar-hr"></div>
                        </div>
                        <div class="sidebar-item montserrat">
                            <a href="{{ route('catalogue-table') }}">Songs</a>
                            <div class="sidebar-hr"></div>
                        </div>
                        <div class="sidebar-item montserrat">
                            <a href="{{ route('composer2',[1,1]) }}">Composer</a>
                            <div class="sidebar-hr"></div>
                        </div>
                        <div class="sidebar-item montserrat">
                            <a href="{{ route('hubungi_kami') }}">Contact Us </a>
                            <div class="sidebar-hr"></div>
                        </div>
                        <div class="sidebar-item montserrat">
                            <input type="text" class="search-input-header search-input w-100 py-3" value="" name="name-message"
                                placeholder="Search">
                            <div class="message-border-animate"></div>
                        </div>
                    </div>
                </div>
                <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-white px-sm-2 px-0">
                    <a href="{{ route('index') }}">
                        <img class="logo-navbar" src="{{ asset('image/logo-argaswara-2.jpg') }}">
                    </a>
                    <button class="navbar-toggler" type="button">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <div class="search">
                                <input type="text" class="search-input-header search-input w-100 py-3" value="" name="name-message"
                                    placeholder="Search">
                                <div class="message-border-animate"></div>
                            </div>
                            <li class="nav-item  mx-2">
                                <a  class="nav-link active montserrat" href="{{ route('index') }}">Home</a>
                                <div id="Home" class="nav"></div>
                            </li>
                            <li class="nav-item  mx-2">
                                <a class="nav-link montserrat" href="{{ route('catalogue-table') }}">Songs</a>
                                <div id="listlagu" class="nav"></div>
                            </li>
                            <li class="nav-item  mx-2">
                                <a class="nav-link montserrat" href="{{ route('composer2',[1,1]) }}">Composer</a>
                                <div id="Composer" class="nav"></div>
                            </li>
                            {{-- <li class="nav-item  dropdown mx-2">
                                <a class="nav-link dropdown-toggle montserrat" href="#" id="navbarDropdown"
                                    role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Composer
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item montserrat" href="{{ route('catalogue') }}"
                                        style="font-size:12px;">Daftar Lagu</a>
                                    <a class="dropdown-item montserrat" href="{{ route('composer2',[1,1]) }}"
                                        style="font-size:12px;">Composer</a>
                                </div>
                            </li> --}}

                            {{-- <li class="nav-item  mx-2">
                                <a class="nav-link montserrat" href="#">Services</a>
                            </li>
                            <li class="nav-item  mx-2">
                                <a class="nav-link montserrat" href="#">Profile</a>
                            </li> --}}
                            <li class="nav-item  mx-2">
                                <a class="nav-link  montserrat" href="{{ route('hubungi_kami') }}">
                                    Contact Us
                                </a>
                                <div id="hubungikami" class="nav"></div>
                                {{-- <div class="dropdown-menu"
                                    aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item montserrat" href="#" style="font-size:12px;">Legal</a>
                                </div> --}}
                            </li>
                            <li class="nav-item for-search  mx-2">
                                <a class="nav-link"><i class="fas fa-search"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            @yield('content')
            <div id="footer" class="col-12 px-0 section-footer">
                <div class="row mx-0 cr-footer text-white roboto">
                    <div class="col-md-3 px-0 d-flex flex-column justify-content-center align-self-start">
                        <h5 style="padding: 5px 16px;font-size: 26px;">TENTANG KAMI</h5>
                        <p style="padding: 5px 16px;text-align:justify">
                        Arga Swara Kencana Musik, Perusahaan yang berdiri serak 15 April 1998. Fokus pada administrasi Database Karya Musik, Perizinan Musik, Pengumpulan Royalti, Pendaftaran Katalog dan Publikasi
                        </br>
                        </br>
                        Arga Swara membuktikan lebih dari 100 komposer dengan lebih dari 5.000 lagu yang kami kilola dan terus berkembang setiap hari
                        </p>
                    </div>
                    <div class="col-md-2 px-0 d-flex flex-column justify-content-center align-self-start">
                        <h5 style="padding: 5px 16px;font-size: 26px;">KEANGGOTAAN</h5>
                        <p style="padding: 5px 0px">
                        <a href="{{ route('page-show','terms-of-service') }}" class="text-white px-3 my-anchor">Hukum</a>
                        </br>
                        <a href="{{ route('page-show','privacy-policy') }}" class="text-white px-3 my-anchor">Kebijakan</a>
                        </br>
                        <a href="{{ route('page-show','privacy-policy') }}" class="text-white px-3 my-anchor">Persyaratan Pelayanan</a>
                        </br>
                        <a href="{{ route('page-show','privacy-policy') }}" class="text-white px-3 my-anchor">FAQ</a>
                        </br>
                        </p>
                        {{-- <a href="{{ route('page-show','bantuan') }}" class="text-white px-3 my-anchor">© 2008 - 2020</a> --}}
                    </div>
                    <div class="col-md-3 px-0 d-flex flex-column justify-content-center align-self-start">
                        <h5 style="padding: 5px 16px;font-size: 26px">HUBUNGI KAMI</h5>
                        <p style="padding: 5px 16px"><i class="fas fa-phone"></i>&nbsp (021) 2921 9174</p>
                        <p style="padding: 5px 16px"><i class="fas fa-envelope"></i>&nbsp publisher@argaswara.com</p>
                    </div>
                    <div class="col-md-4 px-0 d-flex flex-column justify-content-center align-self-start">
                        <h5 style="padding: 5px 16px;font-size: 26px">POSTING</h5>
                        <div class="row" style="padding: 5px 16px;">
                            <div class="col-md-4" style="padding: 5px 16px;">
                                <div style="background-color:white;height:150px;">
                                    
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 5px 16px;">
                                <div style="background-color:white;height:150px;">
                                    
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 5px 16px;">
                                <div style="background-color:white;height:150px;">
                                    
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 5px 16px;">
                                <div style="background-color:white;height:150px;">
                                    
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 5px 16px;">
                                <div style="background-color:white;height:150px;">
                                    
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 5px 16px;">
                                <div style="background-color:white;height:150px;">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mx-0 text-white roboto" style="background-color:#cf0100;padding-top:10px;padding-bottom:10px">
                    <div class="col-md-12 px-0 d-flex justify-content-center">
                        <div class="px-3">2021 ARGASWARA | <a class="text-white my-anchor" style="display: inline !important" href="http://www.apikomunika.com/">by Api Komunika </a></div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
{{-- <script id="dsq-count-scr" src="//argawara-com.disqus.com/count.js" async data-turbolinks-track="true"></script> --}}
<script>
    // document.addEventListener("turbo:load", function() {
            $('.search-input-header').keydown(function (e){
                if(e.keyCode == 13){
                    window.location.href = '/search/'+$(this).val();
                    return false;
                }
            })

            var rules = {
                "":"Home",
                "player":"listlagu",
                "catalogue":"listlagu",
                "composer":"Composer",
                "hubungikami":"hubungikami"
            };

            $(function(){
                var urlPath = window.location.pathname.split("/");
                if (urlPath.length > 1) {
                    var first_part = urlPath[1];
                    if( rules.hasOwnProperty(first_part) ){
                        $('#'+rules[first_part]).siblings('a').addClass('active');
                        $('#'+rules[first_part]).addClass('active-nav');
                    }
                }  
            });

            $('.sidebar-item').on('click', function() {
                $(this).children(".sidebar-sub-item").slideToggle('fast');
            });
            $('.for-search').on('click', function() {
                $('.search').animate({
                    width: 'toggle'
                });
            });
            $('.navbar-toggler').click(function() {
                $('.sidebar').animate({
                    width: 'toggle'
                });
            });
            $('#cross-sidebar').on('click', function() {
                $(this).parent().animate({
                    width: 'toggle'
                });
            });
            var prevScrollpos = 0;
            $(window).scroll(function() {
                if ($(document).scrollTop() > prevScrollpos) {
                    $(".navbar").css("height", "100px");
                } else {
                    $(".navbar").css("height", "85px");
                }
                prevScrollpos = window.pageYOffset;
            });
    // })
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5f9578bb2915ea4ba0968655/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();

</script>
<!--End of Tawk.to Script-->

{{-- <script src="../node_modules/jquery/dist/jquery.js/"></script>
<script src="../node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
<script src="../node_modules/slick-carousel/slick/slick.min.js"></script> --}}
@yield('js')

</html>
