<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/ckeditor.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/front_end.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/front_end_2.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/my.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/player_single.css') }}">
    <link rel="icon" type="image/ico" sizes="16x16" href="{{ asset('image/icon.ico') }}">
    {{--
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat%3A100%2C
    200%2C300%2C400%2C500%2C600%2C700%2C800%2C900%2C100italic%2C200italic%2C300italic%2C400italic%2
    C500italic%2C600italic%2C700italic%2C800italic%2C900italic%7CRoboto%3A100%2C300%2C400%2C500%2C700%2C900
    %2C100italic%2C300italic%2C400italic%2C500italic%2C700italic%2C900italic%7CPlayfair+Display%3A400%2C700%2
    C900%2C400italic%2C700italic%2C900italic&#038;subset=latin&#038;ver=1596446003"> --}}

    {{--
    <link rel="stylesheet" href="../node_modules/owl.carousel/dist/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="../node_modules/animate.css/animate.min.css" />
    <link rel="stylesheet" href="../node_modules/slick-carousel/slick/slick.css" />
    <link rel="stylesheet" href="../node_modules/slick-carousel/slick/slick-theme.css" />
    --}}
    {{-- <title>Argaswara - @yield('title')</title> --}}
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
    {!! JsonLd::generate() !!}
</head>

<body>
    <div class="container-fluid px-0" style="overflow: hidden">
        <div class="row mx-0">
            <div class="col-md-12">
                <div class="sidebar-pink fixed-top" style="display:none;">
                    {{-- <img class="logo-navbar" src="{{ asset('image/logo-argaswara.png') }}"> --}}
                    <a href="#" id="cross-sidebar">
                        <div class="fas fa-times" style="height: 28px;font-size: 28px;color: white"></div>
                    </a>
                    <div class="sidebar-container">
                        <div class="sidebar-pink-item montserrat">
                            <a class="my-anchor text-white" href="{{ route('index') }}">BERANDA</a>
                        </div>
                        <div class="sidebar-pink-item montserrat">
                            <a class="my-anchor text-white" href="#">KATALOG <i class="fas fa-caret-down"></i></a>
                            <div class="sidebar-pink-sub-item" style="display:none;">
                                <a href="{{ route('catalogue') }}" class="my-anchor text-white">Daftar Lagu</a>
                            </div>
                            <div class="sidebar-pink-sub-item" style="display:none;">
                                <a href="{{ route('composer2') }}" class="my-anchor text-white">Katalog</a>
                            </div>
                            {{-- <div class="sidebar-sub-item" style="display:none;">
                                <a href="#" class="my-anchor" style="color: white">Arrangers</a>
                            </div> --}}
                        </div>
                        <div class="sidebar-pink-item montserrat">
                            <a class="my-anchor text-white" href="{{ route('hubungi_kami') }}">HUBUNGI KAMI </a>
                            {{-- <div class="sidebar-sub-item" style="display:none;">
                                <a href="#" class="my-anchor" style="color: white">Legal</a>
                            </div> --}}
                        </div>
                        {{-- <div class="sidebar-pink-item montserrat">
                            <a class="my-anchor text-white" href="#">BERITA</a>
                        </div> --}}
                    </div>
                </div>
                <nav class="navbar fixed-top navbar-expand px-sm-2 px-0 navbar-pink" style="box-shadow: none">
                    <a href="{{ route('index') }}">
                    <img class="logo-navbar" src="{{ asset('image/logo-argaswara.png') }}">
                    </a>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <div class="menu__wrapper">
                                    <img class="trigger-navbar" src="{{asset('image/bento-icon.png')}}" alt="">
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            @yield('content')
            <div class="col-12 px-0 section-footer mt-5">
                {{-- <div class="row mx-0 pb-3">
                    <div class="col-lg-3">
                        <img class="logo-footer" src="{{ asset('image/logo-argaswara.png') }}">
                    </div>
                    <div class="col-lg-2 col-md-3 my-3 my-md-5 my-lg-2 d-flex flex-column">
                        <div href="" class="montserrat text-white ">Perusahaan</div>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Tentang</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Pekerjaan</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">For the Record</a>
                    </div>
                    <div class="col-lg-2 col-md-3 my-3 my-md-5 my-lg-2 d-flex flex-column">
                        <div href="" class="montserrat text-white ">Komunitas</div>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Untuk Artis</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Pengembang</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Iklan</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Investor</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Vendor</a>
                    </div>
                    <div class="col-lg-2 col-md-3 my-3 my-md-5 my-lg-2 d-flex flex-column">
                        <div href="" class="montserrat text-white ">Tautan Berguna</div>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Bantuan</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Pemutar Web</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Aplikasi Seluler Gratis</a>
                    </div>
                    <div class="col-lg-2 col-md-3 my-3 my-md-5 my-lg-2 d-flex flex-row">
                        <a href="#"><img src="{{ asset('image/joox.png') }}" class="joox-footer mx-0 mx-md-2"></a>
                        <a href="#"><i class="fas fa-music mx-2" style="color:white"></i></a>
                        <a href="#"><i class="fab fa-spotify mx-2" style="color:white"></i></a>
                        <a href="#"><i class="fab fa-youtube mx-2" style="color:white"></i></a>
                    </div>
                </div> --}}
                <div class="row mx-0 cr-footer py-2 text-white roboto">
                    <div class="col-md-6 px-0 py-2 d-flex justify-content-center justify-content-md-start">
                        <a href="{{ route('page-show','tentang-kami') }}" class="text-white px-3 my-anchor">Tentang Kami</a>
                        <span>|</span>
                        <a href="{{ route('page-show','untuk-composer') }}" class="text-white px-3 my-anchor">Untuk Composer</a>
                        <span>|</span>
                        <a href="{{ route('page-show','bantuan') }}" class="text-white px-3 my-anchor">Bantuan</a>
                        <span>|</span>
                        <a href="{{ route('page-show','hukum') }}" class="text-white px-3 my-anchor">Hukum</a>
                    </div>
                    <div class="col-md-6 px-0 d-flex justify-content-center justify-content-md-end">
                        <div class="px-3 py-2">2020 ARGASWARA | by Api Komunika</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="{{ asset('js/front_end.js') }}"></script>
<script src="{{ asset('js/my.js') }}"></script>
<script id="dsq-count-scr" src="//argawara-com.disqus.com/count.js" async></script>
<script>
    $('.sidebar-pink-item').on('click', function() {
        $(this).children(".sidebar-pink-sub-item").slideToggle('fast');
    });
    $('.for-search').on('click', function() {
        $('.search').animate({
            width: 'toggle'
        });
    });
    $('.trigger-navbar').click(function() {
        $('.sidebar-pink').fadeToggle("slow", "linear");
    });
    $('#cross-sidebar').on('click', function() {
        $(this).parent().fadeToggle("slow", "linear");
    });
    var prevScrollpos = 0;
    $(window).scroll(function() {
        if ($(document).scrollTop() > prevScrollpos) {
            $(".navbar").css("height", "100px");
            $(".navbar").css("box-shadow", "0 1px 6px 0 rgba(49, 53, 59, 0.5)");
        } else {
            $(".navbar").css("height", "85px");
            $(".navbar").css("box-shadow", "none");
        }
        prevScrollpos = window.pageYOffset;
    });

</script>


{{-- <script src="../node_modules/jquery/dist/jquery.js/"></script>
<script src="../node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
<script src="../node_modules/slick-carousel/slick/slick.min.js"></script> --}}
@yield('js')

</html>
