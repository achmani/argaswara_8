@extends('layouts.home')
@section('content')
    <div class="col-12 px-0" style="margin-top: 100px;">
        <div style="width: 100%"><iframe width="100%" height="600" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJoXXlDEIdai4RZGjzf9cStJQ&key=AIzaSyC0PTKvIdJGj04BXe00I3GvxHPr-RTmbmQ" allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="montserrat my-3" style="font-size:20px;">PT Arga Swara Kencana Musik</div>
                <div class="d-flex align-items-lg-center mb-2">
                    <img src="image/location-1.png" style="height: 25px;" alt="">
                    <div class="roboto ml-2" style="font-size: 15px;">Ruko Elang Laut Boulevard Blok D No. 52</br>Jalan Pantai Indah Selatan, Kamal Muara, Jakarta Utara 14470</div>
                </div>
                <div class="d-flex align-items-center mb-2">
                    <img src="image/telpon-1.png" alt="">
                    <div class="roboto ml-1" style="font-size: 15px;">(021) 2921 9174</div>
                </div>
                {{-- <div class="d-flex align-items-center mb-1">
                    <img src="image/wa-1.png" alt="">
                    <div class="roboto ml-1" style="font-size: 15px;">+62 812-xxxx-xxxx</div>
                </div> --}}
            </div>
        </div>
    </div>
    <div class="col-lg-6" style="margin-bottom: 60px">
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="montserrat my-3" style="font-size:25px;">Kontak Kami</div>
                <form id="callus" action="{{ route('contact-create') }}" class="row">
                    {{ csrf_field() }}
                    <div class="col-md-6 my-2">
                        <div class="roboto">Nama Lengkap</div>
                        <input type="text" id="name" name="name" required class="w-100 message-input-2" value="" name="name-message">
                    </div>
                    <div class="col-md-6 my-2">
                        <div class="roboto">Email</div>
                        <input type="email" id="email" name="email" required class="w-100 message-input-2" value="" name="email-message">
                    </div>
                    <div class="col-md-6 my-2">
                        <div class="roboto">Handphone</div>
                        <input type="text" id="phone" name="phone" required class="w-100 message-input-2" value="" name="handphone-message">
                    </div>
                    <div class="col-md-6 my-2">
                        <div class="roboto">Subject</div>
                        <input type="text" id="subject" name="subject" required class="w-100 message-input-2" value="" name="subject-message">
                    </div>
                    <div class="col-12 my-2">
                        <div class="roboto">Pesan</div>
                        <textarea name="message" id="message" required class="message-input-2 w-100" style="height:200px;" name="message"></textarea>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn-submit mt-2">Kirim Pesan</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {

            $("#callus").on("submit", function(event) {
                event.preventDefault();
                var data = $(this).serialize();
                var url = $('#callus').attr('action');
                $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        dataType: 'json'
                    }).done(function(data) {
                        // show the response
                        $("#name").val("");
                        $("#email").val("");
                        $("#phone").val("");
                        $("#subject").val("");
                        $('#message').val('').change();
                        $("#message").html("");
                        alert(data.message);
                    })
                    .fail(function(data) {
                        // just in case posting your form failed
                        alert(data.message);
                    });;
            });
        });

    </script>
@endsection
