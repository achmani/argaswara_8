<x-app-layout>
    @section('content')
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    @if ($type == 'create')
                        <livewire:masterdata.music-form :rules="$rules" :type="$type">
                        @elseif($type == "update")
                            <livewire:masterdata.music-form :rules="$rules" :type="$type" :musicId="$data->music_id"
                                :musicName="$data->music_name" :musicTemp="$data->music_url" :musicRelease="$data->music_release"
                                :musicJoox="$data->music_joox" :musicItunes="$data->music_itunes"
                                :musicSpotify="$data->music_spotify" :musicYoutube="$data->music_youtube"
                                :genreId="$data->genre->pluck('genre_id')->toArray()"
                                :genreValue="$data->genre->pluck('genre_value')->toArray()"
                                :labelId="$data->label->pluck('label_id')->toArray()"
                                :labelValue="$data->label->pluck('label_value')->toArray()"
                                :artistId="$data->artist->pluck('artist_id')->toArray()"
                                :artistName="$data->artist->pluck('artist_name')->toArray()">
                        @elseif($type == "read")
                            <livewire:masterdata.music-form :rules="$rules" :type="$type" :musicId="$data->music_id"
                                :musicName="$data->music_name" :musicTemp="$data->music_url" :musicRelease="$data->music_release" 
                                :musicJoox="$data->music_joox" :musicItunes="$data->music_itunes"
                                :musicSpotify="$data->music_spotify" :musicYoutube="$data->music_youtube"
                                :genreId="$data->genre->pluck('genre_id')->toArray()"
                                :genreValue="$data->genre->pluck('genre_value')->toArray()"
                                :labelId="$data->label->pluck('label_id')->toArray()"
                                :labelValue="$data->label->pluck('label_value')->toArray()"
                                :artistId="$data->artist->pluck('artist_id')->toArray()"
                                :artistName="$data->artist->pluck('artist_name')->toArray()">
                    @endif
                </div>
            </div>
        </div>
    @endsection

    @section('scripts')
        <script src="{{ asset('js/form_livewire.js') }}"></script>
        <script>
            $(document).ready(function() {

                var wireId = $("#livewire-form-music").attr('wire:id');

                $('#genreId').select2({
                    placeholder: "Choose tags...",
                    minimumInputLength: 2,
                    debug: true,
                    ajax: {
                        url: "{{ route('genre-select2') }}",
                        dataType: 'json',
                        data: function(params) {
                            return {
                                term: params.term || '',
                                page: params.page || 1
                            }
                        },
                        processResults: function(data, params) {
                            params.page = params.page || 1;
                            return {
                                results: data.results,
                                pagination: {
                                    more: (params.page * 10) < data.count_filtered
                                }
                            };
                        },
                        cache: true,
                    }
                });

                $('#labelId').select2({
                    placeholder: "Choose tags...",
                    minimumInputLength: 2,
                    debug: true,
                    ajax: {
                        url: "{{ route('label-select2') }}",
                        dataType: 'json',
                        data: function(params) {
                            return {
                                term: params.term || '',
                                page: params.page || 1
                            }
                        },
                        processResults: function(data, params) {
                            params.page = params.page || 1;
                            return {
                                results: data.results,
                                pagination: {
                                    more: (params.page * 10) < data.count_filtered
                                }
                            };
                        },
                        cache: true,
                    }
                });

                $('#artistId').select2({
                    placeholder: "Choose tags...",
                    minimumInputLength: 2,
                    debug: true,
                    ajax: {
                        url: "{{ route('artist-select2') }}",
                        dataType: 'json',
                        data: function(params) {
                            return {
                                term: params.term || '',
                                page: params.page || 1
                            }
                        },
                        processResults: function(data, params) {
                            params.page = params.page || 1;
                            return {
                                results: data.results,
                                pagination: {
                                    more: (params.page * 10) < data.count_filtered
                                }
                            };
                        },
                        cache: true,
                    }
                });

                $('#composerId').select2({
                    placeholder: "Choose tags...",
                    minimumInputLength: 2,
                    debug: true,
                    ajax: {
                        url: "{{ route('composer-select2') }}",
                        dataType: 'json',
                        data: function(params) {
                            return {
                                term: params.term || '',
                                page: params.page || 1
                            }
                        },
                        processResults: function(data, params) {
                            params.page = params.page || 1;
                            return {
                                results: data.results,
                                pagination: {
                                    more: (params.page * 10) < data.count_filtered
                                }
                            };
                        },
                        cache: true,
                    }
                });

                $('#genreId').on('change', function(e) {
                    var data = $(this).val();
                    window.livewire.find(wireId).set('genreId', data);
                });

                $('#labelId').on('change', function(e) {
                    var data = $(this).val();
                    window.livewire.find(wireId).set('labelId', data);
                });

                $('#artistId').on('change', function(e) {
                    var data = $(this).val();
                    window.livewire.find(wireId).set('artistId', data);
                });

                $('#composerId').on('change', function(e) {
                    var data = $(this).val();
                    window.livewire.find(wireId).set('composerId', data);
                });

                @if(isset($data))
                    // Fetch the preselected item, and add to the control
                    var artistSelect = $('#artistId');
                    $.ajax({
                        type: 'GET',
                        url: "{{ route('artist-select2-music', $data->music_id) }}"
                    }).then(function(data) {
                        // create the option and append to Select2
                        data = data.results;
                        data.forEach(element => {
                            var option = new Option(element.text, element.id, true, true);
                            artistSelect.append(option).trigger('change');
                        });
                        // manually trigger the `select2:select` event
                        artistSelect.trigger({
                            type: 'select2:select',
                            params: {
                                data: data
                            }
                        });
                    });

                    // Fetch the preselected item, and add to the control
                    var genreSelect = $('#genreId');
                    $.ajax({
                        type: 'GET',
                        url: "{{ route('genre-select2-music', $data->music_id) }}"
                    }).then(function(data) {
                        // create the option and append to Select2
                        data = data.results;
                        data.forEach(element => {
                            var option = new Option(element.text, element.id, true, true);
                            genreSelect.append(option).trigger('change');
                        });
                        // manually trigger the `select2:select` event
                        genreSelect.trigger({
                            type: 'select2:select',
                            params: {
                                data: data
                            }
                        });
                    });

                    // Fetch the preselected item, and add to the control
                    var labelSelect = $('#labelId');
                    $.ajax({
                        type: 'GET',
                        url: "{{ route('label-select2-music', $data->music_id) }}"
                    }).then(function(data) {
                        // create the option and append to Select2
                        data = data.results;
                        data.forEach(element => {
                            var option = new Option(element.text, element.id, true, true);
                            labelSelect.append(option).trigger('change');
                        });
                        // manually trigger the `select2:select` event
                        labelSelect.trigger({
                            type: 'select2:select',
                            params: {
                                data: data
                            }
                        });
                    });

                    // Fetch the preselected item, and add to the control
                    var composerSelect = $('#composerId');
                    $.ajax({
                        type: 'GET',
                        url: "{{ route('composer-select2-music', $data->music_id) }}"
                    }).then(function(data) {
                        // create the option and append to Select2
                        data = data.results;
                        data.forEach(element => {
                            var option = new Option(element.text, element.id, true, true);
                            composerSelect.append(option).trigger('change');
                        });
                        // manually trigger the `select2:select` event
                        composerSelect.trigger({
                            type: 'select2:select',
                            params: {
                                data: data
                            }
                        });
                    });
                @endif

            });

        </script>
    @endsection
</x-app-layout>
