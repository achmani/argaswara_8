@extends('layouts.home')
@section('content')
    <div class="col-12 px-0">
        <div class="jumbotron-img d-flex justify-content-center" style="background-image: 
        linear-gradient(to right, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url('image/jumbotron-composer.jpg');">
            <div class="text-jumbotron-img">
                <div class="montserrat" style="color: white;font-size:3vw;">OUR COMPOSER</div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="text-center montserrat title-composer-page">
            OUR COMPOSER
            <div class="text-center roboto" style="font-size: 0.5em">
                Find out more about the people behind our music
            </div>
        </div>
        <div class="w-100 d-flex new-song-carousel overflow-scroll">
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/kaka-slank.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Kaka</div>
                        <div class="font-italic mb-3 font-14">Terlalu Manis</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/armand-gigi.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Armand Maulana</div>
                    <div class="font-italic mb-3 font-14">Pemimpin Dari Surga</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/dewa-19.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Dewa 19</div>
                        <div class="font-italic mb-3 font-14">Kangen</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/crishye.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Crishye</div>
                    <div class="font-italic mb-3 font-14"> Seperti Yang Kau Minta</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/ebied.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Ebied G.Ade</div>
                        <div class="font-italic mb-3 font-14">Camelia</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/tomi-j-p.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Tomy J Pisa</div>
                    <div class="font-italic mb-3 font-14">Dibatas Kota Ini</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/pasha.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Pasha</div>
                        <div class="font-italic mb-3 font-14">Para Pencari Tuhan</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/irwansyah.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Irwansyah</div>
                    <div class="font-italic mb-3 font-14">Pencinta Wanita</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/kaka-slank.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Kaka</div>
                        <div class="font-italic mb-3 font-14">Terlalu Manis</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/armand-gigi.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Armand Maulana</div>
                    <div class="font-italic mb-3 font-14">Pemimpin Dari Surga</div>
                </div>
            </div>
        </div>
        <nav aria-label="Page navigation example" class="mt-5">
            <ul class="pagination justify-content-center">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">Previous</a>
              </li>
              <li class="page-item"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">Next</a>
              </li>
            </ul>
          </nav>
    </div>
@endsection
