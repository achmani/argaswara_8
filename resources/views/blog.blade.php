@extends('layouts.home')
@section('content')
    <div class="col-12 px-0">
        <div class="jumbotron-img d-flex justify-content-center" style="background-image: 
        linear-gradient(to right, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url('image/jumbotron-composer.jpg');">
            <div class="text-jumbotron-img">
                <div class="montserrat" style="color: white;font-size:3vw;">OUR BLOG</div>
            </div>
        </div>
    </div>
    <div class="col-12 mt-5">
        <div class="row">
            <div class="col-lg-9">
                <div class="img-blog"></div>
                <div class="desc-img-blog mt-4">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                    nisi ut aliquip ex ea commodo consequat. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                    minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </div>
                <p class="my-4">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                    enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Ut enim ad minim
                    veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                    laboris nisi ut aliquip ex ea commodo consequat. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat.
                </p>
                <p class="my-4">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                    enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Ut enim ad minim
                    veniam, quis nostrud exercitation. <i>Red penulis</i>
                </p>
            </div>
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-12 col-sm-6 col-lg-12">
                        <b class="d-block mb-3">KATEGORI PENCIPTA LAGU</b>
                        <a href="#" class="my-anchor my-1">Bian Bian Gindas</a>
                        <a href="#" class="my-anchor my-1">Bian Bian Gindas</a>
                        <a href="#" class="my-anchor my-1">Bian Bian Gindas</a>
                        <a href="#" class="my-anchor my-1">Bian Bian Gindas</a>
                        <a href="#" class="my-anchor my-1">Bian Bian Gindas</a>
                        <a href="#" class="my-anchor my-1">Bian Bian Gindas</a>
                        <a href="#" class="my-anchor my-1">Bian Bian Gindas</a>
                        <a href="#" class="my-anchor my-1">Bian Bian Gindas</a>
                        <a href="#" class="my-anchor my-1">Bian Bian Gindas</a>
                        <a href="#" class="my-anchor my-1">Bian Bian Gindas</a>
                        <a href="#" class="my-anchor my-1">Bian Bian Gindas</a>
                        <a href="#" class="my-anchor my-1">Bian Bian Gindas</a>
                        <a href="#" class="my-anchor my-1">Bian Bian Gindas</a>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-12">
                        <b class="d-block mt-5 mt-sm-0 mt-lg-5 mb-3">BERITA TERBARU</b>
                        <div class="row my-2">
                            <div class="col-12">
                                <a href="#" class="my-anchor row" style="display: flex !important;">
                                    <div class="col-4">
                                        <div class="img-sub-sm-blog" style="background-image: url('image/republik.jpg')"></div>
                                    </div>
                                    <div class="col-8 pl-0">
                                        <div class="montserrat">
                                            Berita Rhoma Irama  1
                                        </div>
                                        <div class="d-flex">
                                            <img src={{asset('image/icon-time.PNG')}} style="height: 22px;width:22px;">
                                            <div class="mx-1">3/2/2016</div>
                                            <img class="mx-1" src={{asset('image/icon-comment.PNG')}} style="height: 22px;width:22px;">
                                            <div class="mx-1">3</div>
                                            <img class="mx-1" src={{asset('image/icon-love.PNG')}} style="height: 22px;width:22px;">
                                            <div class="mx-1">10</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-12">
                                <a href="#" class="my-anchor row" style="display: flex !important;">
                                    <div class="col-4">
                                        <div class="img-sub-sm-blog" style="background-image: url('image/republik.jpg')"></div>
                                    </div>
                                    <div class="col-8 pl-0">
                                        <div class="montserrat">
                                            Berita Rhoma Irama  1
                                        </div>
                                        <div class="d-flex">
                                            <img src={{asset('image/icon-time.PNG')}} style="height: 22px;width:22px;">
                                            <div class="mx-1">3/2/2016</div>
                                            <img class="mx-1" src={{asset('image/icon-comment.PNG')}} style="height: 22px;width:22px;">
                                            <div class="mx-1">3</div>
                                            <img class="mx-1" src={{asset('image/icon-love.PNG')}} style="height: 22px;width:22px;">
                                            <div class="mx-1">10</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <button class="btn btn-sm btn-secondary px-4" style="color: black"><b>Blog</b></button>
        <button class="btn btn-sm btn-secondary px-4" style="color: black"><b>Rhoma irama</b></button>
        <div class="d-flex mt-4">
           <a href="#"><img src={{asset('image/twitter-blog-icon.PNG')}} style="height: 50px;width: 50px;"></a> 
           <a href="#"><img src={{asset('image/facebook-blog-icon.PNG')}} style="height: 50px;width: 50px;"></a> 
           <a href="#"><img src={{asset('image/whatsapp-blog-icon.PNG')}} style="height: 50px;width: 50px;"></a> 
        </div>
       <button class="btn btn-xl btn-outline-secondary mt-4">
            <img class="mx-1" src={{asset('image/icon-love.PNG')}} style="height: 22px;width:22px;">
            10
       </button>
    </div>
    <div class="col-lg-9">
        <div class="montserrat my-4 title-berita">Berita Terkait</div>
        <div class="row">
            <a class="col-md-4 my-anchor my-3" href="#">
                <div class="overflow-hidden" style="border-radius:10px;">
                    {{-- <div class="img-sub-blog"
                        style="background-image: url('{{ asset('app/articleThumbnail/article-' . $item->article_id . '/' . $item->article_img . '.jpg') }}');padding-top:70%;">
                    </div> --}}
                    <div class="img-sub-blog"
                        style="background-color:grey;padding-top:70%;">
                    </div>
                </div>
                <div class="text-muted mt-3" style="font-size: 11px;"><img src={{asset('image/icon-time.PNG')}} style="height: 20px;width:20px;">3 Februari 2016, in Rhoma Irama</div>
                <div style="font-weight: 900;">Berita Rhoma Irama</div>
                <p class="mt-2 mb-1">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad, consequatur.
                </p>
                <div class="d-flex align-items-center">
                    <div class="img-author-blog"></div>
                    <b class="mx-2" style="font-size: 12px;">Penulis Admin</b>
                </div>
            </a>
            <a class="col-md-4 my-anchor my-3" href="#">
                <div class="overflow-hidden" style="border-radius:10px;">
                    {{-- <div class="img-sub-blog"
                        style="background-image: url('{{ asset('app/articleThumbnail/article-' . $item->article_id . '/' . $item->article_img . '.jpg') }}');padding-top:70%;">
                    </div> --}}
                    <div class="img-sub-blog"
                        style="background-color:grey;padding-top:70%;">
                    </div>
                </div>
                <div class="text-muted mt-3" style="font-size: 11px;"><img src={{asset('image/icon-time.PNG')}} style="height: 20px;width:20px;">3 Februari 2016, in Rhoma Irama</div>
                <div style="font-weight: 900;">Berita Rhoma Irama</div>
                <p class="mt-2 mb-1">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad, consequatur.
                </p>
                <div class="d-flex align-items-center">
                    <div class="img-author-blog"></div>
                    <b class="mx-2" style="font-size: 12px;">Penulis Admin</b>
                </div>
            </a>
            <a class="col-md-4 my-anchor my-3" href="#">
                <div class="overflow-hidden" style="border-radius:10px;">
                    {{-- <div class="img-sub-blog"
                        style="background-image: url('{{ asset('app/articleThumbnail/article-' . $item->article_id . '/' . $item->article_img . '.jpg') }}');padding-top:70%;">
                    </div> --}}
                    <div class="img-sub-blog"
                        style="background-color:grey;padding-top:70%;">
                    </div>
                </div>
                <div class="text-muted mt-3" style="font-size: 11px;"><img src={{asset('image/icon-time.PNG')}} style="height: 20px;width:20px;">3 Februari 2016, in Rhoma Irama</div>
                <div style="font-weight: 900;">Berita Rhoma Irama</div>
                <p class="mt-2 mb-1">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad, consequatur.
                </p>
                <div class="d-flex align-items-center">
                    <div class="img-author-blog"></div>
                    <b class="mx-2" style="font-size: 12px;">Penulis Admin</b>
                </div>
            </a>
        </div>
    </div>
@endsection
