@extends('layouts.home')
@section('title', $title)
@section('content')
    {{-- <div class="col-12 px-0 bg-pink"> --}}
    <div class="col-12 px-0" style=" background-size: cover;background-position: center;background-image: url({{ asset('image/background-carousel.jpg') }})">
        <div class="slick slick-index">
            @foreach ($musics as $music)
                <div class="item-slider" data-id="{{ $song_item }}">
                    <div class="wrapper-slider" style="margin-top: 35px">
                        <div class="circle-slider"
                            style="background-image: url('{{ asset('app/composer/composer-' . $music->composer()->first()->composer_id . '/' . $music->composer()->first()->composer_img . '.jpg') }}');">
                        </div>
                        <div class="bottom-container player-index">
                            <div class="container">
                                <div class="control-container row">
                                    <div class="col-md-9 pr-0 col-7 artist-name">
                                        <span data-amplitude-song-info="name"
                                            data-amplitude-song-index="{{ $song_index }}"></span>
                                    </div>
                                    <div class="col-md-3 pl-0 col-5 d-flex control-song">
                                        <div class="amplitude-prev">
                                        </div>
                                        <div id="play-pause" class="amplitude-play-pause amplitude-paused"
                                            data-amplitude-song-index="{{ $song_index }}">
                                        </div>
                                        <div class="amplitude-next" data-nextid="{{ $song_index + 1 }}">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="time-container">
                                            <progress class="amplitude-song-played-progress" id="song-played-progress-{{ $song_item++ }}"
                                                data-amplitude-song-index="{{ $song_index }}"></progress>
                                            <span data-amplitude-song-info="artist" class="song-name montserrat" style="font-weight: 100;color: #333"
                                                data-amplitude-song-index="{{ $song_index }}"></span>
                                            <span class="duration">
                                                <span class="amplitude-duration-minutes"
                                                    data-amplitude-song-index="{{ $song_index }}"></span>:<span
                                                    class="amplitude-duration-seconds"
                                                    data-amplitude-song-index="{{ $song_index++ }}"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="coba d-flex align-items-center px-2 justify-content-between mx-auto">
            <div class="previous montserrat text-white" style="cursor: pointer">
                < PREV</div>
                    <div class="next montserrat text-white" style="cursor: pointer">NEXT ></div>
            </div>
        </div>
            <div class="px-8p">
                <div class="row mx-0">
                    <div class="col-12">
                        <div class="w-100 d-block text-center barrier selamat-datang" style="margin-top: 70px;margin-bottom: 70px">
                            <div id="welcome-argaswara" class="montserrat-extra-bold gradient-red-black w-100">
                                Welcome to ARGA SWARA
                            </div>
                            <div id="kami-hadirkan" class="d-block mx-auto montserrat">
                                KAMI HADIRKAN BERBAGAI MACAM GENRE MUSIK PILIHAN </br><span class="play-fair-display-i">~ enjoy - let's sing a song ~</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 mx-0">
                <div class="w-100 layers-image" style="padding: 0 5% !important;margin-bottom: 125px;">
                    <img class="layers-image-2" style="border-radius: 20px;" src="{{ asset('image/layers-image-2.jpg') }}">
                </div>
            </div>
                {{-- <div class="col-12 px-md-5" style="position:relative;top:-50px;margin-bottom:30px;">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="w-100 px-3 pt-3">
                                <img src="{{ asset('image/icon-setting.png') }}">
                                <div class="title-services montserrat-extra-bold">Penerbit Musik</div>
                                <div class="detail-services roboto">Manajemen hak musik yang transparan, koleksi royalti di seluruh
                                    dunia, Sinkronisasi kreatif & pemasaran lagu</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="w-100 px-3 pt-3">
                                <img src="{{ asset('image/icon-setting.png') }}">
                                <div class="title-services montserrat-extra-bold">Digital Service</div>
                                <div class="detail-services roboto">Distribusi musik digital, Monetisas video, Koleksi hak cipta
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="w-100 px-3 pt-3">
                                <img src="{{ asset('image/icon-setting.png') }}">
                                <div class="title-services montserrat-extra-bold">Lisensi Musik</div>
                                <div class="detail-services roboto">Lisensi musik untuk digunakn di TV, film, radio, iklan, cetak
                                    dan di
                                    web</div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            <div class="px-8p">
                <div class="row mx-0">
                    <div class="col-12 px-0" style="background-color: white;margin-bottom: 125px">
                        <div class="row mx-0">
                            <div class="col-xl-6 col-sm-12 px-4 px-sm-0">
                                {{-- <div class="image-invite"></div> --}}
                                <video autoplay id="argaswara_video" class="video-js vjs-default-skin vjs-big-play-centered"
                                    controls preload="auto" width="100%">
                                    <source src="{{url($video)}}" type="{{$mime}}" />
                                </video>
                            </div>
                            <div class="col-xl-6">
                                <div class="container-invite">
                                    <div class="title-invite montserrat-extra-bold" style="color: black">
                                        Kami ada untuk melayani. Penggubah, artis, dan mitra penerbitan
                                    </div>
                                    <div class="roboto detail-invite" style="color: black">
                                        Apakah anda seorang produser atau komposer yang mencari penerbit musik terbaik ? Seorang pemilik
                                        label mencari mitra penerbitan ? Pemilik katalog atau penerbit asli yang mencari representasi
                                        terbaik ? Cari tahu apa yang bisa kami lakukan.
                                        <div class="w-100 d-flex" style="margin-top: 30px;">
                                            <a href="{{ route('page-show','tentang-kami') }}" class="btn-invite">info lebih lanjut <i class="fas fa-arrow-right"
                                                    style="position: relative;left: 15px;"></i></a>
                                            {{-- <div class="download-pdf">
                                                <a href="#">Atau downlod contoh pdf gratis</a>
                                            </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>

            <div class="px-8p">
                <div class="row mx-0">
                    @if(count($sponsor) > 0)
                        <div class="w-100">
                            <hr class="hr-text montserrat-extra-bold" style="margin-top: 70px !important;margin-bottom: 70px !important" data-content="Our Partner">
                        </div>
                        <div class="w-100 d-flex flex-row" style="overflow-x: scroll">
                            @foreach ($sponsor as $item)
                                <div class="col-12 col-md-6 col-xl-3 mt-4 mt-sm-0 mb-5">
                                    <a href="{{ $item->header_link }}" class="card-clg-artist w-clg-100"
                                        style="background-image: url('{{ asset('app/carousel_header/'.$item->header_path.".jpg") }}');background-position:center;">
                                        <div class="bg-card-clg-artist" style="height: 300px;">
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="d-flex mx-0 flex-row justify-content-center">
                    {{-- <div class="flex-shrink-1 previous-2 px-2 mt-4" style="font-size: 30px;"><</div> --}}
                    <div id="sponsor-2" class="d-flex flex-row" style="margin-bottom: 60px;width:100%;margin-top:25px;">
                        <div class="col-12 col-md-2 col-xl-2 mt-4 mt-sm-0 mb-5">
                            <a href="#" class="card-clg-artist w-clg-100"
                                style="background-image: url('{{ asset('app/carousel_header/'."joox.png") }}');background-position:center center;background-size:contain">
                                <div class="bg-card-clg-artist" style="height: 100px;">
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-2 col-xl-2 mt-4 mt-sm-0 mb-5">
                            <a href="#" class="card-clg-artist w-clg-100"
                                style="background-image: url('{{ asset('app/carousel_header/'."musixmatch.png") }}');background-position:center center;background-size:contain">
                                <div class="bg-card-clg-artist" style="height: 100px">
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-2 col-xl-2 mt-4 mt-sm-0 mb-5">
                            <a href="#" class="card-clg-artist w-clg-100"
                                style="background-image: url('{{ asset('app/carousel_header/'."resso.png") }}');background-position:center center;background-size:contain">
                                <div class="bg-card-clg-artist" style="height: 100px">
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-2 col-xl-2 mt-4 mt-sm-0 mb-5">
                            <a href="#" class="card-clg-artist w-clg-100"
                                style="background-image: url('{{ asset('app/carousel_header/'."spotify.png") }}');background-position:center center;background-size:contain">
                                <div class="bg-card-clg-artist" style="height: 100px">
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-2 col-xl-2 mt-4 mt-sm-0 mb-5">
                            <a href="#" class="card-clg-artist w-clg-100"
                                style="background-image: url('{{ asset('app/carousel_header/'."youtube.png") }}');background-position:center center;background-size:contain">
                                <div class="bg-card-clg-artist" style="height: 100px">
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-2 col-xl-2 mt-4 mt-sm-0 mb-5">
                            <a href="#" class="card-clg-artist w-clg-100"
                                style="background-image: url('{{ asset('app/carousel_header/'."smule.png") }}');background-position:center center;background-size:contain">
                                <div class="bg-card-clg-artist" style="height: 100px">
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-2 col-xl-2 mt-4 mt-sm-0 mb-5">
                            <a href="#" class="card-clg-artist w-clg-100"
                                style="background-image: url('{{ asset('app/carousel_header/'."starmaker.jpg") }}');background-position:center center;background-size:contain">
                                <div class="bg-card-clg-artist" style="height: 100px">
                                </div>
                            </a>
                        </div>
                    </div>
                    {{-- <div class="flex-shrink-1 next-2 px-2 mt-4" style="font-size: 30px;">></div> --}}
                </div>
            </div>

    @endsection

    @section('js')
        {{-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/amplitudejs@5.2.0/dist/amplitude.js"></script> --}}
        <script>
            var song_array = {!! $music_json !!};
            var song_array_length = song_array.length;

            // document.addEventListener("turbo:load", function() {

                // document.addEventListener("turbo:before-cache", function() {
                //     console.log("asasasas");
                //     var media = $("#argaswara_video").get(0);
                //     console.log(media);
                //     media.pause();
                //     media.currentTime = 0;
                // });

                $("#callus").on("submit", function(event) {
                    event.preventDefault();
                    var data = $(this).serialize();
                    var url = $('#callus').attr('action');
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        dataType: 'json'
                    }).done(function(data) {
                        // show the response
                        $("#name").val("");
                        $("#email").val("");
                        $("#phone").val("");
                        $("#subject").val("");
                        $('#message').val('').change();
                        $("#message").html("");
                        alert(data.message);
                    })
                    .fail(function(data) {
                        // just in case posting your form failed
                        alert(data.message);
                    });;
                });

                $('.slick').slick({
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '22%',
                    autoplay: false,
                    dots: false,
                    appendArrows: '.coba',
                    nextArrow: $('.next'),
                    prevArrow: $('.previous'),
                    responsive: [{
                        breakpoint: 768,
                        settings: {
                            centerMode: false
                        }
                    }]
                });

                $('#sponsor-2').slick({
                    infinite: true,
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    infinite: true,
                    centerMode: false,
                    centerPadding: '0%',
                    autoplay: true,
                    dots: false,
                    arrows: false,
                    // nextArrow: $('.next-2'),
                    // prevArrow: $('.previous-2'),
                    responsive: [
                        {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            centerMode: false
                        }
                    }]
                });

                for (let index = 1; index <= song_array.length; index++) {
                    document.getElementById('song-played-progress-'+index).addEventListener('click', function(e) {
                        if (Amplitude.getActiveIndex() == 0) {
                            var offset = this.getBoundingClientRect();
                            var x = e.pageX - offset.left;

                            Amplitude.setSongPlayedPercentage((parseFloat(x) / parseFloat(this
                                .offsetWidth)) *
                                100);
                        }
                    });
                }
                // document.getElementById('song-played-progress-1').addEventListener('click', function(e) {
                //     if (Amplitude.getActiveIndex() == 0) {
                //         var offset = this.getBoundingClientRect();
                //         var x = e.pageX - offset.left;

                //         Amplitude.setSongPlayedPercentage((parseFloat(x) / parseFloat(this.offsetWidth)) *
                //             100);
                //     }
                // });

                // document.getElementById('song-played-progress-2').addEventListener('click', function(e) {
                //     if (Amplitude.getActiveIndex() == 1) {
                //         var offset = this.getBoundingClientRect();
                //         var x = e.pageX - offset.left;

                //         Amplitude.setSongPlayedPercentage((parseFloat(x) / parseFloat(this.offsetWidth)) *
                //             100);
                //     }
                // });

                // document.getElementById('song-played-progress-3').addEventListener('click', function(e) {
                //     if (Amplitude.getActiveIndex() == 1) {
                //         var offset = this.getBoundingClientRect();
                //         var x = e.pageX - offset.left;

                //         Amplitude.setSongPlayedPercentage((parseFloat(x) / parseFloat(this.offsetWidth)) *
                //             100);
                //     }
                // });

                // document.getElementById('song-played-progress-4').addEventListener('click', function(e) {
                //     if (Amplitude.getActiveIndex() == 1) {
                //         var offset = this.getBoundingClientRect();
                //         var x = e.pageX - offset.left;

                //         Amplitude.setSongPlayedPercentage((parseFloat(x) / parseFloat(this.offsetWidth)) *
                //             100);
                //     }
                // });

                // var song_array = [{
                //         "name": "Judi",
                //         "artist": "Rhoma Irama",
                //         "album": "",
                //         "url": "{{ route('play', 'RHOMA_IRAMA_Rhoma Irama_Judi') }}",
                //         "cover_art_url": "../album-art/we-are-to-answer.jpg"
                //     }, {
                //         "name": "Sandiwara Cinta",
                //         "artist": "Deddy Dores feat Syahrini",
                //         "album": "",
                //         "url": "{{ route('play', 'DEDDY_DORES_Syahrini_Sandiwara Cinta') }}",
                //         "cover_art_url": "../album-art/we-are-to-answer.jpg"
                //     },
                //     {
                //         "name": "Tanya Hati",
                //         "artist": "Rayen Pono",
                //         "album": "",
                //         "url": "{{ route('play', 'RAYEN_PONO_Pasto_Tanya Hati') }}",
                //         "cover_art_url": "../album-art/we-are-to-answer.jpg"
                //     },
                //     {
                //         "name": "Aku Masih Bisa",
                //         "artist": "Herman",
                //         "album": "",
                //         "url": "{{ route('play', 'HERMAN _ M.AWAL_Seventeen_Aku Masih Bisa') }}",
                //         "cover_art_url": "../album-art/we-are-to-answer.jpg"
                //     },
                // ];

                var nextid = 1;

                Amplitude.init({
                    // "bindings": {
                    //     37: 'prev',
                    //     39: 'next',
                    //     32: 'play_pause'
                    // },
                    "bindings": {
                        37: 'prev',
                        39: 'next'
                    },
                    "continue_next": false,
                    "songs": song_array,
                    callbacks: {
                        initialized: function() {
                            Amplitude.getAudio().addEventListener('play', function() {
                                // should log 'play' when audio begins playing; does not work in responsive design mode!
                                // console.log('play');
                            });
                        }
                    }
                });

                // console.log(Amplitude.getConfig());

                Amplitude.pause();


                $(".amplitude-next").click(function() {
                    $(".slick").slick('slickNext');
                    // if (nextid >= song_array_length) {
                    //     Amplitude.playSongAtIndex(0);
                    //     nextid = 1;
                    // } else {
                    //     nextid = nextid + 1;
                    // }
                });

                $(".next").click(function() {
                    $(".slick-active .amplitude-next").click();
                });

                $(".previous").click(function() {
                    $(".slick-active .amplitude-prev").click();
                });

                $(".amplitude-prev").click(function() {
                    $(".slick").slick('slickPrev');
                    // if (nextid <= 1) {
                    //     nextid = 1;
                    // } else {
                    //     nextid = nextid - 1;
                    // }
                });

                document.addEventListener("turbo:click", function() {
                    if(Amplitude){
                        Amplitude.pause();
                    }
                });

                // $(".amplitude-next").click(function() {
                //     // var nextid = $(this).data("nextid");
                //     // if(nextid == 0){
                //     //     Amplitude.playSongAtIndex( nextid );
                //     // }
                // });

                // window.onkeydown = function(e) {
                //     return !(e.keyCode == 32);
                // };

            // });

        </script>
    @endsection
