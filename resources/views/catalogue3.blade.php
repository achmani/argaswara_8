@extends('layouts.home')
@section('content')
    <div class="col-12 big-artist">
        <div class="w-100 d-flex carousel-big-artist">
            <div class="my-col-md-4  mt-4 mt-sm-0">
                <div class="montserrat" style="color:red;">LISTEN NOW</div>
                <div class="roboto mt-1">Yuni Shara</div>
                <div class="font-italic mt-1 mb-3">Desember Kelabu</div>
                <a href="{{route('player')}}" class="card-clg-artist w-clg-100" style="background-image: url('image/yuni-shara.jpg');">
                    <div class="bg-card-clg-artist">
                        <div class="container-play-music-xl">
                            <i class="far fa-play-circle play-music pm-xl"></i>
                        </div>
                    </div>
                </a>
            </div>
            <div class="my-col-md-4  mt-4 mt-sm-0">
                <div class="montserrat" style="color:red;">LISTEN NOW</div>
                <div class="roboto mt-1">Once</div>
                <div class="font-italic mt-1 mb-3">Pangeran Cinta</div>
                <a href="" class="card-clg-artist w-clg-100" style="background-image: url('image/once.jpg');">
                    <div class="bg-card-clg-artist">
                        <div class="container-play-music-xl">
                            <i class="far fa-play-circle play-music pm-xl"></i>
                        </div>
                    </div>
                </a>
            </div>
            <div class="my-col-md-4  mt-4 mt-sm-0">
                <div class="montserrat" style="color:red;">LISTEN NOW</div>
                <div class="roboto mt-1">Marthino Lio</div>
                <div class="font-italic mt-1 mb-3">Ratusan Purnama</div>
                <a href="" class="card-clg-artist w-clg-100" style="background-image: url('image/marthino-lio-2.jpg');">
                    <div class="bg-card-clg-artist">
                        <div class="container-play-music-xl">
                            <i class="far fa-play-circle play-music pm-xl"></i>
                        </div>
                    </div>
                </a>
            </div>
            <div class="my-col-md-4  mt-4 mt-sm-0">
                <div class="montserrat" style="color:red;">LISTEN NOW</div>
                <div class="roboto mt-1">Kaka</div>
                <div class="font-italic mt-1 mb-3">Terlalu Manis</div>
                <a href="" class="card-clg-artist w-clg-100" style="background-image: url('image/kaka-slank.jpg');">
                    <div class="bg-card-clg-artist">
                        <div class="container-play-music-xl">
                            <i class="far fa-play-circle play-music pm-xl"></i>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-9 many-carousel">
        <div class="montserrat mt-4 ml-2">Lagu Baru</div>
        <div class="w-100 d-flex new-song-carousel overflow-scroll">
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/kaka-slank.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Kaka</div>
                        <div class="font-italic mb-3 font-14">Terlalu Manis</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/armand-gigi.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Armand Maulana</div>
                    <div class="font-italic mb-3 font-14">Pemimpin Dari Surga</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/dewa-19.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Dewa 19</div>
                        <div class="font-italic mb-3 font-14">Kangen</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/crishye.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Crishye</div>
                    <div class="font-italic mb-3 font-14"> Seperti Yang Kau Minta</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/ebied.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Ebied G.Ade</div>
                        <div class="font-italic mb-3 font-14">Camelia</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/tomi-j-p.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Tomy J Pisa</div>
                    <div class="font-italic mb-3 font-14">Dibatas Kota Ini</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/pasha.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Pasha</div>
                        <div class="font-italic mb-3 font-14">Para Pencari Tuhan</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/irwansyah.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Irwansyah</div>
                    <div class="font-italic mb-3 font-14">Pencinta Wanita</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/acha.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Acha Septriasa</div>
                        <div class="font-italic mb-3 font-14">Sampai Menutup Mata</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/nirina.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Nirina</div>
                    <div class="font-italic mb-3 font-14">Pandangan Pertama</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/giring.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Giring</div>
                        <div class="font-italic mb-3 font-14">Laskar Pelangi</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/ariel.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Ariel Noah</div>
                    <div class="font-italic mb-3 font-14">Tak Ada Yang Abadi</div>
                </div>
            </div>
        </div>
        <div class="montserrat mt-4 ml-2">Artis</div>
        <div class="w-100 d-flex artist-carousel overflow-scroll">
            @for ($i=0; $i<=5; $i++)
                <div class="container-artist flex-change-clg">
                    <div class="d-flex">
                        <a href="" class="card-clg-artist w-clg-50 my-2"
                            style="background-image: url('{{ asset('image/rano-karno.webp') }}');">
                            <div class="bg-card-clg-artist">
                                <div class="container-play-music-sm">
                                    <i class="far fa-play-circle play-music pm-sm"></i>
                                </div>
                            </div>
                        </a>
                        <div class="roboto mt-2 ml-2 font-14" style="color: black;font-weight: 900;">Rano Karno</div>
                    </div>
                    <div class="d-flex">
                        <a href="" class="card-clg-artist w-clg-50 my-2"
                            style="background-image: url('{{ asset('image/felix.jpg') }}');">
                            <div class="bg-card-clg-artist">
                                <div class="container-play-music-sm">
                                    <i class="far fa-play-circle play-music pm-sm"></i>
                                </div>
                            </div>
                        </a>
                        <div class="roboto mt-2 ml-2 font-14" style="color: black;font-weight: 900;">Felix</div>
                    </div>
                    <div class="d-flex">
                        <a href="" class="card-clg-artist w-clg-50 my-2"
                            style="background-image: url('{{ asset('image/regita.jpg') }}');">
                            <div class="bg-card-clg-artist">
                                <div class="container-play-music-sm">
                                    <i class="far fa-play-circle play-music pm-sm"></i>
                                </div>
                            </div>
                        </a>
                        <div class="roboto mt-2 ml-2 font-14" style="color: black;font-weight: 900;">Regita Echa</div>
                    </div>
                    <div class="d-flex">
                        <a href="" class="card-clg-artist w-clg-50 my-2"
                            style="background-image: url('{{ asset('image/rian.jpg') }}');">
                            <div class="bg-card-clg-artist">
                                <div class="container-play-music-sm">
                                    <i class="far fa-play-circle play-music pm-sm"></i>
                                </div>
                            </div>
                        </a>
                        <div class="roboto mt-2 ml-2 font-14" style="color: black;font-weight: 900;">Rian D'Masiv</div>
                    </div>
                </div>
            @endfor
        </div>
        <div class="montserrat mt-4 ml-2">Top Composer</div>
        <div class="w-100 d-flex new-song-carousel overflow-scroll">
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/kaka-slank.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Kaka</div>
                        <div class="font-italic mb-3 font-14">Terlalu Manis</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/armand-gigi.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Armand Maulana</div>
                    <div class="font-italic mb-3 font-14">Pemimpin Dari Surga</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/dewa-19.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Dewa 19</div>
                        <div class="font-italic mb-3 font-14">Kangen</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/crishye.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Crishye</div>
                    <div class="font-italic mb-3 font-14"> Seperti Yang Kau Minta</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/ebied.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Ebied G.Ade</div>
                        <div class="font-italic mb-3 font-14">Camelia</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/tomi-j-p.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Tomy J Pisa</div>
                    <div class="font-italic mb-3 font-14">Dibatas Kota Ini</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/pasha.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Pasha</div>
                        <div class="font-italic mb-3 font-14">Para Pencari Tuhan</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/irwansyah.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Irwansyah</div>
                    <div class="font-italic mb-3 font-14">Pencinta Wanita</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/acha.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Acha Septriasa</div>
                        <div class="font-italic mb-3 font-14">Sampai Menutup Mata</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/nirina.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Nirina</div>
                    <div class="font-italic mb-3 font-14">Pandangan Pertama</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/giring.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Giring</div>
                        <div class="font-italic mb-3 font-14">Laskar Pelangi</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/ariel.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Ariel Noah</div>
                    <div class="font-italic mb-3 font-14">Tak Ada Yang Abadi</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 top-song-artist">
        <div class="row">
            <div class="col-6 col-lg-12">
                <div class="montserrat mt-4">Top Song</div>
                <div class="w-100 d-flex flex-column">
                    @foreach ($composer_top as $composer)
                        <div class="d-flex">
                            <a href="" class="card-clg-artist flex-20 my-2"
                                style="background-image: url('{{ asset('app/composer/composer-' . $composer->composer_id . '/' . $composer->composer_img . '_300.jpg') }}');">
                                <div class="bg-card-clg-artist">
                                    <div class="container-play-music-sm">
                                        <i class="far fa-play-circle play-music pm-sm"></i>
                                    </div>
                                </div>
                            </a>
                            <div class="d-flex flex-80 flex-column">
                            <div class="roboto mt-2 ml-2 font-14" style="color: black;font-weight: 900;">{{ $composer->composer_name }}</div>
                                {{-- <div class="font-italic ml-2 font-14">Terlalu Manis</div> --}}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-6 col-lg-12">
                <div class="montserrat mt-4">Top Artist</div>
                <div class="w-100 d-flex flex-column">
                    @foreach ($artist_top as $artist)
                        <div class="d-flex">
                            <a href="" class="card-clg-artist flex-20 my-2"
                                style="background-image: url('{{ asset('app/artist/artist-' . $artist->artist_id . '/' . $artist->artist_img . '_300.jpg') }}');">
                                <div class="bg-card-clg-artist">
                                    <div class="container-play-music-sm">
                                        <i class="far fa-play-circle play-music pm-sm"></i>
                                    </div>
                                </div>
                            </a>
                            <div class="d-flex flex-80 flex-column">
                            <div class="roboto mt-2 ml-2 font-14" style="color: black;font-weight: 900;">{{ $artist->artist_name }}</div>
                                {{-- <div class="font-italic ml-2 font-14">Terlalu Manis</div> --}}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
            // $('.new-song-carousel').slick({
            //     infinite: true,
            //     slidesToShow: 5,
            //     slidesToScroll: 1,
            //     dots: false,
            //     arrows: false,
            //     autoplay: false,
            //     autoplaySpeed: 1500,
            //     responsive: [{
            //             breakpoint: 768,
            //             settings: {
            //                 slidesToShow: 3,
            //                 slidesToScroll: 2
            //             }
            //         },
            //         {
            //             breakpoint: 576,
            //             settings: {
            //                 slidesToShow: 2,
            //                 slidesToScroll: 2
            //             }
            //         }
            //     ]
            // });
            $('.carousel-big-artist').slick({
                infinite: true,
                slidesToShow: 3,
                dots: true,
                arrows: true,
                autoplay: true,
                autoplaySpeed: 1500,
                responsive: [
                    {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                    },
                    {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    }
                ]
            });
            // $('.artist-carousel').slick({
            //     infinite: true,
            //     slidesToShow: 4,
            //     dots: false,
            //     arrows: false,
            //     autoplay: false,
            //     autoplaySpeed: 1500,
            //     responsive: [{
            //         breakpoint: 768,
            //         settings: {
            //             slidesToShow: 2,
            //             slidesToScroll: 1,
            //             infinite: true
            //         }
            //     }]
            // });


    </script>
    
@endsection
