<x-app-layout>

@section('content')
    <div class="container-fluid">
        <!-- *************************************************************** -->
        <!-- Start First Cards -->
        <!-- *************************************************************** -->
        <div class="card-group">
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-dark mb-1 font-weight-medium">236</h2>
                            </div>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Artist</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-muted"><i data-feather="user"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium">1279</h2>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Music
                            </h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-muted"><i class="ti-music"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-dark mb-1 font-weight-medium">79</h2>
                            </div>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Composer</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-muted"><i class="ti-music-alt"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <h2 class="text-dark mb-1 font-weight-medium">50</h2>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Arranger</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-muted"><i class="ti-panel"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- *************************************************************** -->
        <!-- End First Cards -->
        <!-- *************************************************************** -->
        <!-- *************************************************************** -->
        <!-- Start Sales Charts Section -->
        <!-- *************************************************************** -->
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="card" style="height:100%;">
                    <div class="card-body">
                        <h4 class="card-title">Genre Music</h4>
                        <canvas id="chartJS" class="mt-2" height="150vh"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="card" style="height:100%;">
                    <div class="card-body">
                        <h4 class="card-title">Visitors</h4>
                        <canvas id="chartJS2" height="150vh" style="position:absolute;margin:0 auto"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    var ctx = document.getElementById('chartJS').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ['Pop', 'Jazz', 'Dangdut'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3],
                backgroundColor: [
                    '#5f76e8',
                    '#ff4f70',
                    '#01caf1'
                ],
                borderWidth: 1
            }]
        },   
        options: {
            cutoutPercentage: 70
        }
    });
    var ctx = document.getElementById('chartJS2').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Sun', 'Mon', 'Tues','Wed','Thurs','Fri','Sat'],
            datasets: [{
                data: [12, 19, 3,8,10,6,7],
                backgroundColor: [
                    '#5f76e8',
                    '#ff4f70',
                    '#01caf1',
                    '#63c732',
                    '#cf4334',
                    '#c29825',
                    '#b01e93'

                ],
                borderWidth: 1
            }]
        },   
        options: {
            legend: {
                display: false
            },
            cutoutPercentage: 70,
            scales: {
                yAxes: [{
                    ticks: {
                        stepSize: 5
                    }
                }]
            }
        }
    });
    var ctx = document.getElementById('chartJS3').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['Jan', 'Feb', 'Maret','Apr','Mei','June','July','Agust','Sep','Okt','Nov','Des'],
            datasets: [{
                data: [12, 19, 3,8,10,6,7,12,16,5,9,12],
                backgroundColor: [
                    '#5f76e8'
                ],
                borderColor: 'black',
                pointBorderColor: '#ff4f70',
                pointBackgroundColor:'#ff4f70',
                borderWidth:3
            }]
        },   
        options: {
            legend: {
                display: false
            },
            cutoutPercentage: 70,
            scales: {
                yAxes: [{
                    ticks: {
                        stepSize: 5
                    }
                }]
            }
        }
    });
</script>
@endsection

</x-app-layout>
