<x-app-layout>
    @section('content')
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    {{-- <livewire:input-check :titleInput="$titleInput"
                        :routeCheck="$routeCheck"> --}}
                        @if($type == "create")
                            <livewire:masterdata.user-form :rules="$rules" :type="$type">
                        @elseif($type == "update")
                            <livewire:masterdata.user-form :rules="$rules" :type="$type" :name="$user->name" :email="$user->email" :profile="$user->profile_photo_path" :userid="$user->id">
                        @elseif($type == "read")
                            <livewire:masterdata.user-form :rules="$rules" :type="$type" :name="$user->name" :email="$user->email" :profile="$user->profile_photo_path" :userid="$user->id">
                        @endif
                </div>
            </div>
        </div>
    @endsection

    @section('scripts')
        <script src="{{ asset('js/form_livewire.js') }}"></script>
    @endsection
</x-app-layout>
