<form wire:submit.prevent="submit">

    <style>
        

    </style>

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    <div class="row">

        @if ($type == 'update' || $type == 'read')
            <div class="col-md-12">
                <label class="form-control-label">ID</label>
                <input type="text" class="form-control" wire:model="headerId" readonly>
                </br>
            </div>
        @endif
        <div class="col-md-12">
            <label class="form-control-label">Title</label>
            <input type="text" class="form-control @error('headerTitle') {{ 'is-invalid' }} @enderror"
                wire:model="headerTitle" {{ $type === 'read' ? 'readonly' : '' }}>
            <div class="invalid-feedback">
                @error('headerTitle') <span class="error">{{ $message }}</span> @enderror
            </div>
            </br>
        </div>

        <div class="col-md-12">
            <label class="form-control-label">Url</label>
            <input type="text" class="form-control @error('headerLink') {{ 'is-invalid' }} @enderror"
                wire:model="headerLink" {{ $type === 'read' ? 'readonly' : '' }}>
            <div class="invalid-feedback">
                @error('headerLink') <span class="error">{{ $message }}</span> @enderror
            </div>
            </br>
        </div>

        <div class="col-md-4">
            <label class="form-control-label">Image</label>
            @if ($type != 'read')
                <div class="d-flex flex-row align-items-center">
                    <div class="custom-file">
                        <input class="custom-file-input" type="file" wire:model="img">
                        <label class="custom-file-label" style="overflow: hidden;white-space: nowrap;">Choose
                            image</label>
                    </div>
                    <div wire:loading wire:target="img">
                        <div class="custom-file-loading">
                            <button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                <span class="sr-only">Loading...</span>
                            </button>
                        </div>
                    </div>
                </div>
                @error('img') <span class="text-error">{{ $message }}</span> @enderror
                </br>
            @endif
            @if ($img)
                <img class="mx-auto d-block" src="{{ $img->temporaryUrl() }}" width="200"
                    style="box-shadow:0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);margin-bottom: 10px">
            @else
                @if ($imgTemp)
                    <img class="mx-auto d-block" src="{{ asset('app/carousel_header/' . $imgTemp . '_300.jpg') }}"
                        width="200"
                        style="box-shadow:0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);margin-bottom: 10px">
                @else

                @endif
            @endif
        </div>
        @if ($type != 'read')
            <div class="col-md-12">
                <button class="btn btn-primary" type="submit" style="float: right">Save</button>
            </div>
        @endif
    </div>
</form>

<script>
    document.addEventListener("livewire:load", () => {
        // Livewire.on('submit', (data) => {
        //     $("#success-submit").html(data.title);
        //     $("#success-notif").removeClass("show");
        //     $("#success-notif").removeClass("hide");
        //     $("#success-notif").addClass("show");
        //     // alert(data.title);
        //     // alert('A post was added with the id of: ');
        // })
    });

</script>
