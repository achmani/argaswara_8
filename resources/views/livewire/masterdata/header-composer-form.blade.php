<form id="livewire-form" wire:submit.prevent="submit">

    <style>
        .show {
            display: block;
        }

        .hide {
            display: none;
        }

        .text-error {
            color: red;
            font-size: 80%;
        }

    </style>

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif


    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    @if ($type == 'update' || $type == 'read')
                        <div class="col-md-12">
                            <label class="form-control-label">ID</label>
                            <input id="headerComposerId" type="text" class="form-control" wire:model="headerComposerId"
                                readonly>
                            </br>
                        </div>
                    @endif
                    @if ($type == 'create')
                        <div class="col-md-12" style="margin-bottom: 10px;">
                            <label class="form-control-label">Composer</label>
                            <div wire:ignore class="w-full">
                                <select wire:ignore id="composerId" class="form-control">
                                </select>
                            </div>
                            @error('composerId') <span class="text-error">{{ $message }} </span> @enderror
                        </div>
                    @endif
                    <div class="col-md-12">
                        <label class="form-control-label">Sequence</label>
                        <input id="composerSequence" type="text"
                            class="form-control @error('composerSequence') {{ 'is-invalid' }} @enderror"
                            wire:model="composerSequence" {{ $type === 'read' ? 'readonly' : '' }}>
                        <div class="invalid-feedback">
                            @error('composerSequence') <span class="error">{{ $message }}</span> @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="saveForm" type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>

</form>

<script>
</script>
