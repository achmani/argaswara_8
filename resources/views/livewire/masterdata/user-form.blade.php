<form wire:submit.prevent="submit">

    <style>
        .show {
            display: block;
        }

        .hide {
            display: none;
        }

        .text-error {
            color: red;
            font-size: 80%;
        }

    </style>

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    <div class="row">

        @if ($type == 'update' || $type == 'read')
            <div class="col-md-12">
                <label class="form-control-label">ID</label>
                <input type="text" class="form-control" wire:model="userid" readonly>
                </br>
            </div>
        @endif
        <div class="col-md-12">
            <label class="form-control-label">Name</label>
            <input type="text" class="form-control @error('name') {{ 'is-invalid' }} @enderror" wire:model="name"
                {{ $type === 'read' ? 'readonly' : '' }}>
            <div class="invalid-feedback">
                @error('name') <span class="error">{{ $message }}</span> @enderror
            </div>
            </br>
        </div>

        <div class="col-md-12">
            <label class="form-control-label">Email</label>
            <input type="email" class="form-control @error('email') {{ 'is-invalid' }} @enderror" wire:model="email"
                {{ $type === 'create' ? '' : 'readonly' }}>
            <div class="invalid-feedback">
                @error('email') <span class="error">{{ $message }}</span> @enderror
            </div>
            </br>
        </div>

        @if ($type == 'create')
            <div class="col-md-12">
                <label class="form-control-label">Password</label>
                <input type="password" class="form-control @error('password') {{ 'is-invalid' }} @enderror"
                    wire:model="password">
                <div class="invalid-feedback">
                    @error('password') <span class="error">{{ $message }}</span> @enderror
                </div>
                </br>
            </div>

            <div class="col-md-12">
                <label class="form-control-label">Confirm Password</label>
                <input type="password" class="form-control @error('password_confirmation') {{ 'is-invalid' }} @enderror"
                    wire:model="password_confirmation">
                <div class="invalid-feedback">
                    @error('password_confirmation') <span class="error">{{ $message }}</span> @enderror
                </div>
                </br>
            </div>
        @endif

        <div class="col-md-4">
            <label class="form-control-label">Photo Profile</label>
            @if ($type != 'read')
                <div class="d-flex flex-row align-items-center">
                    <div class="custom-file">
                        <input class="custom-file-input" type="file" wire:model="photo">
                        <label class="custom-file-label" style="overflow: hidden;white-space: nowrap;">Choose photo</label>
                    </div>
                    <div wire:loading wire:target="photo">
                        <div class="custom-file-loading">
                            <button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                <span class="sr-only">Loading...</span>
                            </button>
                        </div>
                    </div>
                </div>
                @error('photo') <span class="text-error">{{ $message }}</span> @enderror
                </br>
                </br>
            @endif
            @if ($photo)
                <img class="mx-auto d-block" src="{{ $photo->temporaryUrl() }}" width="200"
                    style="box-shadow:0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);margin-bottom: 10px">
            @else
                @if ($profile)
                    <img class="mx-auto d-block"
                        src="{{ asset('app/photo_profile/' . $email . '/' . $profile . '.jpg') }}" width="200"
                        style="box-shadow:0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);margin-bottom: 10px">
                @else

                @endif
            @endif
        </div>
        @if ($type != 'read')
            <div class="col-md-12">
                <button class="btn btn-primary" type="submit" style="float: right">Save</button>
            </div>
        @endif
    </div>
</form>

<script>
    document.addEventListener("livewire:load", () => {
        // Livewire.on('submit', (data) => {
        //     $("#success-submit").html(data.title);
        //     $("#success-notif").removeClass("show");
        //     $("#success-notif").removeClass("hide");
        //     $("#success-notif").addClass("show");
        //     // alert(data.title);
        //     // alert('A post was added with the id of: ');
        // })
    });

</script>
