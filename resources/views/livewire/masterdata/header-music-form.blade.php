<form id="livewire-form" wire:submit.prevent="submit">

    <style>
        .show {
            display: block;
        }

        .hide {
            display: none;
        }

        .text-error {
            color: red;
            font-size: 80%;
        }

    </style>

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif


    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    @if ($type == 'update' || $type == 'read')
                        <div class="col-md-12">
                            <label class="form-control-label">ID</label>
                            <input id="headerMusicId" type="text" class="form-control" wire:model="headerMusicId"
                                readonly>
                            </br>
                        </div>
                    @endif
                    @if ($type == 'create')
                        <div class="col-md-12" style="margin-bottom: 10px;">
                            <label class="form-control-label">Music</label>
                            <div wire:ignore class="w-full">
                                <select wire:ignore id="musicId" class="form-control">
                                </select>
                            </div>
                            @error('musicId') <span class="text-error">{{ $message }} </span> @enderror
                        </div>
                    @endif
                    <div class="col-md-12">
                        <label class="form-control-label">Sequence</label>
                        <input id="musicSequence" type="text"
                            class="form-control @error('musicSequence') {{ 'is-invalid' }} @enderror"
                            wire:model="musicSequence" {{ $type === 'read' ? 'readonly' : '' }}>
                        <div class="invalid-feedback">
                            @error('musicSequence') <span class="error">{{ $message }}</span> @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="saveForm" type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>

</form>

<script>
</script>
