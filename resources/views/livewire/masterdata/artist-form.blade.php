<form wire:submit.prevent="submit">

    <style>
        .show {
            display: block;
        }

        .hide {
            display: none;
        }

        .text-error {
            color: red;
            font-size: 80%;
        }

    </style>

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif


    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if ($type == 'update' || $type == 'read')
                    <div class="col-md-12">
                        <label class="form-control-label">ID</label>
                        <input id="artistId" type="text" class="form-control" wire:model="artistId" readonly>
                        </br>
                    </div>
                @endif
                <div class="col-md-12">
                    <label class="form-control-label">Name</label>
                    <input id="artistName" type="text" class="form-control @error('artistName') {{ 'is-invalid' }} @enderror"
                        wire:model="artistName" {{ $type === 'read' ? 'readonly' : '' }}>
                    <div class="invalid-feedback">
                        @error('artistName') <span class="error">{{ $message }}</span> @enderror
                    </div>
                    </br>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="saveForm" type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>

</form>

<script>
</script>
