<form id="livewire-form" wire:submit.prevent="submit">

    <style>
        .show {
            display: block;
        }

        .hide {
            display: none;
        }

        .text-error {
            color: red;
            font-size: 80%;
        }

    </style>

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    <div class="row">

        @if ($type == 'update' || $type == 'read')
            <div class="col-md-12">
                <div class="form-group">
                    <label class="form-control-label">Page ID</label>
                    <input type="text" class="form-control" wire:model="pageId" readonly>
                </div>
            </div>
        @endif

        <div class="col-md-12">
            <div class="form-group">
                <label class="form-control-label">URL</label>
                <input type="text" class="form-control @error('pageUrl') {{ 'is-invalid' }} @enderror"
                    wire:model="pageUrl" {{ $type !== 'create' ? 'readonly' : '' }}>
                <div class="invalid-feedback">
                    @error('pageUrl') <span class="error">{{ $message }}</span> @enderror
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label class="form-control-label">Title</label>
                <input type="text" class="form-control @error('pageTitle') {{ 'is-invalid' }} @enderror"
                    wire:model="pageTitle" {{ $type === 'read' ? 'readonly' : '' }}>
                <div class="invalid-feedback">
                    @error('pageTitle') <span class="error">{{ $message }}</span> @enderror
                </div>
            </div>
        </div>

        <div class="col-md-12">
            @if ($type != 'read')
                <div class="form-group w-full">
                    <label class="form-control-label">Article</label>
                    <div wire:ignore>
                        <textarea id="pageText" class="form-control" rows="9" wire:model="pageText"
                            {{ $type === 'read' ? 'readonly' : '' }}></textarea>
                    </div>
                    <div class="invalid-feedback">
                        @error('pageText') <span class="error">{{ $message }}</span> @enderror
                    </div>
                </div>
            @endif
        </div>

        <div class="col-md-6">
            <label class="form-control-label">Page Thumbnail</label>
            @if ($type != 'read')
                <div class="d-flex flex-row align-items-center">
                    <div class="custom-file">
                        <input class="custom-file-input" type="file" wire:model="pageImg">
                        <label class="custom-file-label" style="overflow: hidden;white-space: nowrap;">Choose
                            image</label>
                    </div>
                    <div wire:loading wire:target="pageImg">
                        <div class="custom-file-loading">
                            <button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                <span class="sr-only">Loading...</span>
                            </button>
                        </div>
                    </div>
                </div>
                @error('pageImg') <span class="text-error">{{ $message }}</span> @enderror
                </br>
            @endif
            @if ($pageImg)
                <img class="mx-auto d-block" src="{{ $pageImg->temporaryUrl() }}" width="200"
                    style="box-shadow:0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);margin-bottom: 10px">
            @else
                @if ($pageImgTemp)
                    <img class="mx-auto d-block"
                        src="{{ asset('app/pageThumbnail/page-' . $pageId . '/' . $pageImgTemp . '_300.jpg') }}"
                        width="200"
                        style="box-shadow:0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);margin-bottom: 10px">
                @else

                @endif
            @endif
        </div>

        @if ($type != 'read')
            <div class="col-md-12">
                <button class="btn btn-primary" type="submit" style="float: right">Save</button>
            </div>
        @else
            <div class="col-md-12">
                {!! $pageText !!}
            </div>
        @endif
    </div>
</form>
