<form id="livewire-form-music" wire:submit.prevent="submit">

    <style>
        .show {
            display: block;
        }

        .hide {
            display: none;
        }

        .text-error {
            color: red;
            font-size: 80%;
        }

    </style>

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    <div class="row">

        @if ($type == 'update' || $type == 'read')
            <div class="col-md-12">
                <div class="form-group">
                    <label class="form-control-label">Music ID</label>
                    <input type="text" class="form-control" wire:model="musicId" readonly>
                    <div class="invalid-feedback">
                        @error('musicId') <span class="error">{{ $message }}</span> @enderror
                    </div>
                </div>
            </div>
        @endif

        <div class="col-md-12">
            <label class="form-control-label">Music Name</label>
            <div class="form-group">
                <input type="text" class="form-control @error('musicName') {{ 'is-invalid' }} @enderror"
                    wire:model="musicName" {{ $type === 'read' ? 'readonly' : '' }}>
            </div>
            <div class="invalid-feedback">
                @error('musicName') <span class="error">{{ $message }}</span> @enderror
            </div>
        </div>

        <div class="col-md-6">
            <label class="form-control-label">Music Release</label>
            <div class="form-group">
                <input type="date" class="form-control @error('musicRelease') {{ 'is-invalid' }} @enderror"
                    wire:model="musicRelease">
                <div class="invalid-feedback">
                    @error('musicRelease') <span class="error">{{ $message }}</span> @enderror
                </div>
            </div>
        </div>

        @if ($type != 'read')
            <div class="col-md-6">
                <label class="form-control-label">Music</label>
                <div class="d-flex flex-row align-items-center">
                    <div class="custom-file">
                        <input class="custom-file-input @error('music') {{ 'is-invalid' }} @enderror" type="file"
                            wire:model="music" accept=".mp3,audio/*">
                        <label class="custom-file-label" style="overflow: hidden;white-space: nowrap;">
                            @if ($music)
                                {{ $music->getClientOriginalName() }}
                            @else
                                {{ $musicName . '.mp3' }}
                            @endif
                        </label>
                    </div>
                    <div wire:loading wire:target="music">
                        <div class="custom-file-loading">
                            <button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                <span class="sr-only">Loading...</span>
                            </button>
                        </div>
                    </div>
                </div>
                @error('music') <span class="text-error">{{ $message }}</span> @enderror
            </div>
        @else
        @endif

        <div class="col-md-6">
            <div wire:ignore class="w-full">
                <label class="form-control-label">Genre</label>
                <div class="form-group">
                    <select id="genreId" class="form-control @error('genreId') {{ 'is-invalid' }} @enderror"
                        name="state" multiple="multiple">
                    </select>
                </div>
            </div>
            @error('genreId') <span class="text-error">{{ $message }} </span> @enderror
        </div>

        <div class="col-md-6">
            <div wire:ignore class="w-full">
                <label class="form-control-label">Label</label>
                <div class="form-group">
                    <select id="labelId" class="form-control @error('labelId') {{ 'is-invalid' }} @enderror"
                        name="state" multiple="multiple">
                    </select>
                </div>
            </div>
            @error('labelId') <span class="text-error">{{ $message }} </span> @enderror
        </div>

        <div class="col-md-6">
            <div wire:ignore class="w-full">
                <label class="form-control-label">Artist</label>
                <div class="form-group">
                    <select wire:ignore id="artistId" class="form-control" multiple="multiple">
                        {{-- @foreach ($artistId as $key => $item)
                            <option value="{{ $item }}" selected="selected"></option>
                        @endforeach --}}
                    </select>
                </div>
            </div>
            @error('artistId') <span class="text-error">{{ $message }} </span> @enderror
        </div>

        <div class="col-md-6">
            <div wire:ignore class="w-full">
                <label class="form-control-label">Composer</label>
                <div class="form-group">
                    <select id="composerId" class="form-control" multiple="multiple">
                    </select>
                </div>
            </div>
            @error('composerId') <span class="text-error">{{ $message }} </span> @enderror
        </div>

        <div class="col-md-6">
            <label class="form-control-label">JOOX</label>
            <div class="form-group">
                <input type="text" class="form-control @error('musicJoox') {{ 'is-invalid' }} @enderror"
                    wire:model="musicJoox" {{ $type === 'read' ? 'readonly' : '' }}>
                <div class="invalid-feedback">
                    @error('musicJoox') <span class="error">{{ $message }}</span> @enderror
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <label class="form-control-label">Itunes</label>
            <div class="form-group">
                <input type="text" class="form-control @error('musicItunes') {{ 'is-invalid' }} @enderror"
                    wire:model="musicItunes" {{ $type === 'read' ? 'readonly' : '' }}>
                <div class="invalid-feedback">
                    @error('musicItunes') <span class="error">{{ $message }}</span> @enderror
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <label class="form-control-label">Spotify</label>
            <div class="form-group">
                <input type="text" class="form-control @error('musicSpotify') {{ 'is-invalid' }} @enderror"
                    wire:model="musicSpotify" {{ $type === 'read' ? 'readonly' : '' }}>
                <div class="invalid-feedback">
                    @error('musicSpotify') <span class="error">{{ $message }}</span> @enderror
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <label class="form-control-label">Youtube</label>
            <div class="form-group">
                <input type="text" class="form-control @error('musicYoutube') {{ 'is-invalid' }} @enderror"
                    wire:model="musicYoutube" {{ $type === 'read' ? 'readonly' : '' }}>
                <div class="invalid-feedback">
                    @error('musicYoutube') <span class="error">{{ $message }}</span> @enderror
                </div>
            </div>
        </div>

        @if ($type != 'read')
            <div class="col-md-12">
                <button class="btn btn-primary" type="submit" style="float: right">Save</button>
            </div>
        @endif
    </div>
</form>

<script>
    document.addEventListener("livewire:load", () => {});

</script>
