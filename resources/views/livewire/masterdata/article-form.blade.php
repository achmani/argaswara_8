<form id="livewire-form" wire:submit.prevent="submit">

    <style>
        .show {
            display: block;
        }

        .hide {
            display: none;
        }

        .text-error {
            color: red;
            font-size: 80%;
        }

    </style>

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    <div class="row">

        @if ($type == 'update' || $type == 'read')
            <div class="col-md-12">
                <div class="form-group">
                    <label class="form-control-label">Article ID</label>
                    <input type="text" class="form-control" wire:model="articleId" readonly>
                </div>
            </div>
        @endif

        <div class="col-md-12">
            <div class="form-group">
                <label class="form-control-label">URL</label>
                <input type="text" class="form-control @error('articleUrl') {{ 'is-invalid' }} @enderror"
                    wire:model="articleUrl" {{ $type !== 'create' ? 'readonly' : '' }}>
                <div class="invalid-feedback">
                    @error('articleUrl') <span class="error">{{ $message }}</span> @enderror
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label class="form-control-label">Title</label>
                <input type="text" class="form-control @error('articleTitle') {{ 'is-invalid' }} @enderror"
                    wire:model="articleTitle" {{ $type === 'read' ? 'readonly' : '' }}>
                <div class="invalid-feedback">
                    @error('articleTitle') <span class="error">{{ $message }}</span> @enderror
                </div>
            </div>
        </div>

        <div class="col-md-12">
            @if ($type != 'read')
                <div class="form-group w-full">
                    <label class="form-control-label">Article</label>
                    <div wire:ignore>
                        <textarea id="articleText" class="form-control" rows="9" wire:model="articleText"
                            {{ $type === 'read' ? 'readonly' : '' }}></textarea>
                    </div>
                    <div class="invalid-feedback">
                        @error('articleText') <span class="error">{{ $message }}</span> @enderror
                    </div>
                </div>
            @endif
        </div>

        <div class="col-md-6">
            <div class="form-group w-full">
                <div wire:ignore>
                    <label class="form-control-label">Composer</label>
                    <div>
                        <select id="composerId" class="form-control" multiple="multiple">
                        </select>
                    </div>
                </div>
                @error('composerId') <span class="text-error">{{ $message }} </span> @enderror
            </div>
        </div>

        <div class="col-md-6">
            <label class="form-control-label">Article Thumbnail</label>
            @if ($type != 'read')
                <div class="d-flex flex-row align-items-center">
                    <div class="custom-file">
                        <input class="custom-file-input" type="file" wire:model="articleImg">
                        <label class="custom-file-label" style="overflow: hidden;white-space: nowrap;">Choose
                            image</label>
                    </div>
                    <div wire:loading wire:target="articleImg">
                        <div class="custom-file-loading">
                            <button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                <span class="sr-only">Loading...</span>
                            </button>
                        </div>
                    </div>
                </div>
                @error('articleImg') <span class="text-error">{{ $message }}</span> @enderror
                </br>
            @endif
            @if ($articleImg)
                <img class="mx-auto d-block" src="{{ $articleImg->temporaryUrl() }}" width="200"
                    style="box-shadow:0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);margin-bottom: 10px">
            @else
                @if ($articleImgTemp)
                    <img class="mx-auto d-block"
                        src="{{ asset('app/articleThumbnail/article-' . $articleId . '/' . $articleImgTemp . '_300.jpg') }}"
                        width="200"
                        style="box-shadow:0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);margin-bottom: 10px">
                @else

                @endif
            @endif
        </div>

        @if ($type != 'read')
            <div class="col-md-12">
                <button class="btn btn-primary" type="submit" style="float: right">Save</button>
            </div>
        @else
            <div class="col-md-12">
                {!! $articleText !!}
            </div>
        @endif
    </div>
</form>
