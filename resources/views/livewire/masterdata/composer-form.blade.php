<form id="livewire-form-composer" wire:submit.prevent="submit">

    <style>
        .show {
            display: block;
        }

        .hide {
            display: none;
        }

        .text-error {
            color: red;
            font-size: 80%;
        }

    </style>

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    <div class="row">

        @if ($type == 'update' || $type == 'read')
            <div class="col-md-12">
                <label class="form-control-label">ID</label>
                <input type="text" class="form-control" wire:model="composerId" readonly>
                </br>
            </div>
        @endif
        <div class="col-md-12">
            <label class="form-control-label">Composer Name</label>
            <input type="text" class="form-control @error('composerName') {{ 'is-invalid' }} @enderror"
                wire:model="composerName" {{ $type === 'read' ? 'readonly' : '' }}>
            <div class="invalid-feedback">
                @error('composerName') <span class="error">{{ $message }}</span> @enderror
            </div>
            </br>
        </div>

        <div class="col-md-12">
            <label class="form-control-label">Composer Popularity</label>
            <input type="text" class="form-control @error('composerPopularity') {{ 'is-invalid' }} @enderror"
                wire:model="composerPopularity" {{ $type === 'read' ? 'readonly' : '' }}>
            <div class="invalid-feedback">
                @error('composerPopularity') <span class="error">{{ $message }}</span> @enderror
            </div>
            </br>
        </div>
        <div class="col-md-12">
            <div wire:ignore class="w-full">
                <label class="form-control-label">Label</label>
                <div class="form-group">
                    <select id="labelId" class="form-control @error('labelId') {{ 'is-invalid' }} @enderror"
                        name="state">
                    </select>
                </div>
            </div>
            @error('labelId') <span class="text-error">{{ $message }} </span> @enderror
        </div>
        <div class="col-md-4">
            <label class="form-control-label">Composer Image</label>
            @if ($type != 'read')
                <div class="d-flex flex-row align-items-center">
                    <div class="custom-file">
                        <input class="custom-file-input" type="file" wire:model="composerImg">
                        <label class="custom-file-label" style="overflow: hidden;white-space: nowrap;">Choose image</label>
                    </div>
                    <div wire:loading wire:target="composerImg">
                        <div class="custom-file-loading">
                            <button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                <span class="sr-only">Loading...</span>
                            </button>
                        </div>
                    </div>
                </div>
                @error('composerImg') <span class="text-error">{{ $message }}</span> @enderror
                </br>
            @endif
            @if ($composerImg)
                <img class="mx-auto d-block" src="{{ $composerImg->temporaryUrl() }}" width="200"
                    style="box-shadow:0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);margin-bottom: 10px">
            @else
                @if ($composerImgTemp)
                    <img class="mx-auto d-block"
                        src="{{ asset('app/composer/composer-' . $composerId . '/' . $composerImgTemp . '_300.jpg') }}" width="200"
                        style="box-shadow:0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);margin-bottom: 10px">
                @else

                @endif
            @endif
        </div>
        <div class="col-md-8">
            <label class="form-control-label">Composer Description</label>
            <div class="form-group">
                <textarea id="composerDesc" class="form-control @error('composerDesc') {{ 'is-invalid' }} @enderror" rows="3"
                    wire:model="composerDesc" {{ $type === 'read' ? 'readonly' : '' }}></textarea>
                <div class="invalid-feedback">
                    @error('composerDesc') <span class="error">{{ $message }}</span> @enderror
                </div>
                </br>
            </div>
        </div>
        @if ($type != 'read')
            <div class="col-md-12">
                <button class="btn btn-primary" type="submit" style="float: right">Save</button>
            </div>
        @endif
    </div>
</form>

<script>
    document.addEventListener("livewire:load", () => {
        // Livewire.on('submit', (data) => {
        //     $("#success-submit").html(data.title);
        //     $("#success-notif").removeClass("show");
        //     $("#success-notif").removeClass("hide");
        //     $("#success-notif").addClass("show");
        //     // alert(data.title);
        //     // alert('A post was added with the id of: ');
        // })
    });

</script>
