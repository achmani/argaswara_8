<div>
    <label class="form-control-label">{{ $titleInput }}</label>
    <input type="email" class="form-control {{ $statusClass }}" wire:model.lazy="searchEmail">
    <div class="valid-feedback">
        Success! You've done it.
    </div>
    <div class="invalid-feedback">
        Sorry, that email's taken. Try another ?
    </div>
</div>

<script>
    document.addEventListener("livewire:load", () => {
        Livewire.hook('component.initialized', (component) => {
        })
        Livewire.hook('element.initialized', (el, component) => {
            
        })
        Livewire.hook('element.updating', (fromEl, toEl, component) => {
            
        })
        Livewire.hook('element.updated', (el, component) => {
            
        })
        Livewire.hook('element.removed', (el, component) => {

        })
        Livewire.hook('message.sent', (message, component) => {

        })
        Livewire.hook('message.failed', (message, component) => {

        })
        Livewire.hook('message.received', (message, component) => {
            var email = @this.searchEmail;
            $.ajax({
                url: "{{ $routeCheck }}",
                method: 'POST',
                data : {
                    email: email
                },
                success: function(result){
                    if(result){
                        @this.statusClass = "is-valid";
                    }else{
                        @this.statusClass = "is-invalid";
                    }
                }
            });
        })
        Livewire.hook('message.processed', (message, component) => {

        })
    });
</script>