@extends('layouts.home')
@section('title', $title)
@section('content')
    <div class="col-12 px-0">
        <div class="jumbotron-img d-flex justify-content-center" style="background-image: 
            linear-gradient(to right, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url('{{ $img }}');">
            <div class="text-jumbotron-img">
                <div class="montserrat" style="color: white;font-size:3vw;">Search</div>
            </div>
        </div>
    </div>
    <div class="col-12 mt-5 px-8p">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-12">
                    <hr class="hr-text montserrat-extra-bold" data-content="Composer">
                </div>
                <div class="my-4 row">
                    @if (count($composer) > 0)
                        @foreach ($composer as $item)
                            <div class="col-12 col-md-3 col-xl-3">
                                <a href="{{ route('player', str_replace(' ', '-',$item->composer_name)) }}" class="card-clg-artist w-clg-100 my-2"
                                    style="background-image: url('{{ asset('app/composer/composer-' . $item->composer_id . '/' . $item->composer_img . '_300.jpg') }}');">
                                    <div class="bg-card-clg-artist">
                                        <div class="container-play-music" style="position: relative">
                                            <i class="far fa-play-circle play-music pm-xl"></i>
                                            <div
                                                style="background-color:black; width: 100%;position: absolute;bottom: 0;color:white;text-align: center;padding: 5px">
                                                <h5>{{ $item->composer_name }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @else
                        <h3 class="text-center">Composer not found</h3>
                    @endif

                </div>
                <div class="col-12">
                    <hr class="hr-text montserrat-extra-bold" data-content="Music">
                </div>
                <div class="my-4 row">
                    @if (count($music) > 0)
                        @foreach ($music as $item)
                            <div class="col-12 col-md-3 col-xl-3">
                                <a href="{{ route('player', [ str_replace(' ', '-',$item->composer[0]->composer_name), $item->music_id ]) }}" class="card-clg-artist w-clg-100 my-2"
                                    style="background-image: url('{{ asset('app/composer/composer-' . $item->composer[0]->composer_id . '/' . $item->composer[0]->composer_img . '_300.jpg') }}');">
                                    <div class="bg-card-clg-artist">
                                        <div class="container-play-music" style="position: relative">
                                            <i class="far fa-play-circle play-music pm-xl"></i>
                                            <div
                                                style="background-color:black; width: 100%;position: absolute;bottom: 0;color:white;text-align: center;padding: 5px">
                                                <h5>{{ $item->music_name }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @else
                        <h3 class="text-center">Music not found</h3>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="row">
            </div>
        </div>
    </div>
    </div>

@endsection
