@extends('layouts.home')
@section('title', $title)
@section('content')
    <div class="col-12 px-0">
        <div class="jumbotron-img d-flex justify-content-center"
            style="background-image: url('/image/background-songs.jpg');background-size: cover">
            <div class="text-jumbotron-img" style="padding: 18% 0 !important">
            </div>
        </div>
    </div>
    <div class="px-8p">
        <div class="row">
            <div class="col-12 big-artist" style="overflow-x: scroll;margin-top: 0px !important">
                <div class="montserrat-extra-bold my-4 ml-3" style="font-size: 40px;">Browser</div>
                <hr class="px-2">
                <div class="w-100 d-flex flex-row">
                    @foreach ($composerheader as $item)
                        <div class="col-12 col-md-6 col-xl-4 mt-4 mt-sm-0 mb-5">
                            <div class="montserrat" style="color:red;font-size: 16px">Dengarkan Lagu</div>
                            <div class="roboto mt-1"
                                style="color: black;font-weight: 900;font-size: 18px;margin-bottom: 10px;">
                                {{ $item->composer_name }}</div>
                            <a href="{{ route('player', $item->composer_id) }}" class="card-clg-artist w-clg-100"
                                style="background-image: url('{{ asset('app/composer/composer-' . $item->composer_id . '/' . $item->composer_img . '.jpg') }}');">
                                <div class="bg-card-clg-artist">
                                    <div class="container-play-music-xl">
                                        <i class="far fa-play-circle play-music pm-xl"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-12">
                <div class="montserrat-extra-bold mt-4 ml-2" style="font-size: 40px;">Composer</div>
                <div class="row">
                    @foreach ($composer as $item)
                        <div class="col-12 col-md-3 col-xl-3">
                            <a href="{{ route('player', $item->composer_id) }}" class="card-clg-artist w-clg-100 my-2"
                                style="background-image: url('{{ asset('app/composer/composer-' . $item->composer_id . '/' . $item->composer_img . '_300.jpg') }}');">
                                <div class="bg-card-clg-artist">
                                    <div class="container-play-music" style="position: relative">
                                        <i class="far fa-play-circle play-music pm-xl"></i>
                                        <div
                                            style="background-color:black; width: 100%;position: absolute;bottom: 0;color:white;text-align: center;padding: 5px">
                                            <h5>{{ $item->composer_name }}</h5>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            {{-- <div class="h-artist-clg">
                                <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">
                                    {{ $item->composer_name }}</div>
                            </div> --}}
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-12  mt-5">
                <div class="row justify-content-center">
                    <nav aria-label="Page navigation example" class="mt-5">
                        <ul class="pagination justify-content-center">
                            <li class="page-item {{ $page > 1 ? '' : 'disabled' }}">
                                <a class="page-link" href="{{ route('catalogue', $page - 1) }}">
                                    < </a>
                            </li>
                            @for ($i = 0; $i * $length_page < $count_composer; $i++)
                                <li class="page-item {{ $i + 1 == $page ? 'active' : '' }}"><a class="page-link"
                                        href="{{ route('catalogue', $i + 1) }}">{{ $i + 1 }}</a></li>
                            @endfor
                            <li class="page-item {{ $page + 1 <= ceil($count_composer / $length) ? '' : 'disabled' }}">
                                <a class="page-link" href="{{ route('catalogue', $page + 1) }}"> > </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
