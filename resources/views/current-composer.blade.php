@extends('layouts.home')
@section('content')
    <style>
        .play-pause {
            display: inline-block;
            width: 32px;
            height: 32px;
            cursor: pointer;
            vertical-align: middle;
            margin-right: 10px;
        }

        .play-pause.amplitude-paused {
            background: url("../img/play.svg");
            background-size: 32px 32px;
        }

        .play-pause.amplitude-playing {
            background: url("../img/pause.svg");
            background-size: 32px 32px;
        }

        /* #play-pause {
                display: inline-block;
                width: 32px;
                height: 32px;
                cursor: pointer;
                vertical-align: middle;
                margin-right: 10px;
            }

            #play-pause.amplitude-paused {
                background: url("../img/play.svg");
                background-size: 32px 32px;
            }

            #play-pause.amplitude-playing {
                background: url("../img/pause.svg");
                background-size: 32px 32px;
            } */

        /* #next-music {
                display: inline-block;
                height: 32px;
                width: 32px;
                cursor: pointer;
                background: url(../img/next.svg);
                vertical-align: middle;
                background-size: 32px 32px;
            } */

        /* #previous-music {
                display: inline-block;
                height: 32px;
                width: 32px;
                cursor: pointer;
                background: url(../img/previous.svg);
                vertical-align: middle;
                background-size: 32px 32px;
            } */

        .icon-href a {
            color: black;
        }

        .icon-href a .joox {
            width: 40px;
        }

    </style>
    <div class="col-12" style="margin-top: 110px;">
        <div class="fixed-bottom d-none">
            <div class="w-100 d-flex justify-content-center" style="height:70px !important;background-color:red">
                <div class="amplitude-prev" data-amplitude-playlist="emancipator" id="previous-music">
                    <div class="fas fa-backward mt-2" style="margin:0 60px; width: 40px;height:40px;color:white;"></div>
                </div>
                <div class="amplitude-next" data-amplitude-playlist="emancipator" id="next-music">
                    <div class="fas fa-forward mt-2" style="margin:0 60px;width: 40px;height:40px;color:white;"></div>
                </div>
                <div class="player-controller">
                    <div class="amplitude-play-pause player-music" data-amplitude-playlist="emancipator" id="play-pause">
                        <div id="play-pause-icon" class="fas fa-play-circle"
                            style="height: 100px;width: 100px;color:white;"></div>
                    </div>
                </div>
            </div>
            <div class="w-100 d-flex justify-content-center pb-2" style="background-color: red;padding:0 7%;">
                <input type="range" class="amplitude-song-slider" data-amplitude-playlist="emancipator"
                    style="width: 100%" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="overflow-hidden">
                    <div class="img-sub-catalogue" style="background-image: url('image/ariel-2.jpg');"></div>
                </div>
                <p class="text-justify">
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci et voluptates quasi? 
                    Minus earum corrupti voluptate officiis 
                    beatae non vero veritatis mollitia. 
                    Incidunt repudiandae nam velit! Aperiam ullam fugit aspernatur?
                </p>
            </div>
            <div class="col-md-8">
                <table class="table" style="overflow-x: scroll">
                    <thead>
                        <tr>
                            <th scope="col">Lagu</th>
                            <th scope="col">Dengarkan</th>
                            <th scope="col">Artis</th>
                            <th scope="col">Composer</th>
                            <th scope="col">Genre</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>
                                <span class="amplitude-song-container play-pause amplitude-play-pause"
                                    data-amplitude-playlist="emancipator" data-amplitude-song-index="0"></span>
                                Aku Begini Engkau Begitu
                            </th>
                            <td class="icon-href">
                                <a href="#"><img class="joox" src="{{ asset('image/joox.png') }}"></a>
                                <a href="#"><i class="fas fa-music mx-2"></i></a>
                                <a href="#"><i class="fab fa-spotify mx-2"></i></a>
                                <a href="#"><i class="fab fa-youtube mx-2"></i></a>
                            </td>
                            <td>Ariel Noah</td>
                            <td>John Doe</td>
                            <td>Pop</td>
                        </tr>
                        <tr>
                            <th>
                                <span class="amplitude-song-container play-pause amplitude-play-pause"
                                    data-amplitude-playlist="emancipator" data-amplitude-song-index="1"></span>
                                Widuri
                            </th>
                            <td class="icon-href">
                                <a href="#"><img class="joox" src="{{ asset('image/joox.png') }}"></a>
                                <a href="#"><i class="fas fa-music mx-2"></i></a>
                                <a href="#"><i class="fab fa-spotify mx-2"></i></a>
                                <a href="#"><i class="fab fa-youtube mx-2"></i></a>
                            </td>
                            <td>Ariel Noah</td>
                            <td>John Doe</td>
                            <td>Pop</td>
                        </tr>
                        <tr>
                            <th>
                                <span class="amplitude-song-container play-pause amplitude-play-pause"
                                    data-amplitude-playlist="emancipator" data-amplitude-song-index="2"></span>
                                Mawar Berduri
                            </th>
                            <td class="icon-href">
                                <a href="#"><img class="joox" src="{{ asset('image/joox.png') }}"></a>
                                <a href="#"><i class="fas fa-music mx-2"></i></a>
                                <a href="#"><i class="fab fa-spotify mx-2"></i></a>
                                <a href="#"><i class="fab fa-youtube mx-2"></i></a>
                            </td>
                            <td>Ariel Noah</td>
                            <td>John Doe</td>
                            <td>Pop</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-12" style="background-color: rgb(235, 235, 235);">
        <div class="montserrat mt-4 ml-2">More by Ariel Noah</div>
        <div class="w-100 d-flex new-song-carousel overflow-scroll">
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/yuni-shara.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Kaka</div>
                        <div class="font-italic mb-3 font-14">Terlalu Manis</div>
                    </div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/irwansyah.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Irwansyah</div>
                        <div class="font-italic mb-3 font-14">Pencinta Wanita</div>
                    </div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/giring.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Giring Nidji</div>
                        <div class="font-italic mb-3 font-14">Laskar Pelangi</div>
                    </div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/nirina.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Nirina Zubir</div>
                        <div class="font-italic mb-3 font-14">Pandangan Pertama</div>
                    </div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/pasha.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Pasha Ungu</div>
                        <div class="font-italic mb-3 font-14">Cinta Dalam Diam</div>
                    </div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/dewa-19.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Dewa 19</div>
                        <div class="font-italic mb-3 font-14">Kangen</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="montserrat mt-4 ml-2">Related Artist</div>
        <div class="w-100 d-flex new-song-carousel overflow-scroll">
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/kaka-slank.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Kaka</div>
                        <div class="font-italic mb-3 font-14">Terlalu Manis</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/armand-gigi.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Armand Maulana</div>
                    <div class="font-italic mb-3 font-14">Pemimpin Dari Surga</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/dewa-19.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Dewa 19</div>
                        <div class="font-italic mb-3 font-14">Kangen</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/crishye.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Crishye</div>
                    <div class="font-italic mb-3 font-14"> Seperti Yang Kau Minta</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/ebied.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Ebied G.Ade</div>
                        <div class="font-italic mb-3 font-14">Camelia</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/tomi-j-p.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Tomy J Pisa</div>
                    <div class="font-italic mb-3 font-14">Dibatas Kota Ini</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/pasha.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Pasha</div>
                        <div class="font-italic mb-3 font-14">Para Pencari Tuhan</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/irwansyah.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Irwansyah</div>
                    <div class="font-italic mb-3 font-14">Pencinta Wanita</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/acha.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Acha Septriasa</div>
                        <div class="font-italic mb-3 font-14">Sampai Menutup Mata</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/nirina.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Nirina</div>
                    <div class="font-italic mb-3 font-14">Pandangan Pertama</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/giring.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Giring</div>
                        <div class="font-italic mb-3 font-14">Laskar Pelangi</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/ariel.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Ariel Noah</div>
                    <div class="font-italic mb-3 font-14">Tak Ada Yang Abadi</div>
                </div>
            </div>
        </div>
        <div class="w-100" style="background-color: rgb(235, 235, 235);padding:15px;"></div>
    </div>
    <div class="col-12">
        <div class="montserrat text-center my-4">Blog by Ariel Noah</div>
        <div class="row">
            <a class="col-md-4 my-anchor" href="#">
                <div class="overflow-hidden">
                    <div class="img-sub-catalogue" style="background-image: url('image/ariel.jpg');"></div>
                </div>
                <p class="text-justify">
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci et voluptates quasi? 
                    Minus earum corrupti voluptate officiis 
                    beatae non vero veritatis mollitia. 
                    Incidunt repudiandae nam velit! Aperiam ullam fugit aspernatur?
                </p>
            </a>
            <a class="col-md-4 my-anchor" href="#">
                <div class="overflow-hidden">
                    <div class="img-sub-catalogue" style="background-image: url('image/ariel-2.jpg');"></div>
                </div>
                <p class="text-justify">
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci et voluptates quasi? 
                    Minus earum corrupti voluptate officiis 
                    beatae non vero veritatis mollitia. 
                    Incidunt repudiandae nam velit! Aperiam ullam fugit aspernatur?
                </p>
            </a>
            <a class="col-md-4 my-anchor" href="#">
                <div class="overflow-hidden">
                    <div class="img-sub-catalogue" style="background-image: url('image/ariel-3.jpeg');"></div>
                </div>
                <p class="text-justify">
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci et voluptates quasi? 
                    Minus earum corrupti voluptate officiis 
                    beatae non vero veritatis mollitia. 
                    Incidunt repudiandae nam velit! Aperiam ullam fugit aspernatur?
                </p>
            </a>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/amplitudejs@5.2.0/dist/amplitude.js"></script>
    <script>
        $('#play-pause').click(function() {
            $("#play-pause-icon").removeClass(["fas fa-pause-circle", "fas fa-play-circle"]);
            if ($(this).hasClass("amplitude-paused")) {
                $("#play-pause-icon").addClass("fas fa-pause-circle");
            } else {
                $("#play-pause-icon").addClass("fas fa-play-circle");
            }
        });

        $('.play-pause').click(function() {
            $("#play-pause-icon").removeClass(["fas fa-pause-circle", "fas fa-play-circle"]);
            if ($(this).hasClass("amplitude-paused")) {
                console.log("ha");
                $("#play-pause-icon").addClass("fas fa-pause-circle");
            } else {
                console.log("hi");
                $("#play-pause-icon").addClass("fas fa-play-circle");
            }
        });

        Amplitude.init({
            "playlists": {
                "emancipator": {
                    songs: [{
                            "name": "First Snow",
                            "artist": "Emancipator",
                            "album": "Soon It Will Be Cold Enough",
                            "url": "{{ route('play', 'AkuBeginiEngkauBegitu') }}",
                            "cover_art_url": ""
                        },
                        {
                            "name": "Dusk To Dawn",
                            "artist": "Emancipator",
                            "album": "Dusk To Dawn",
                            "url": "{{ route('play', 'Widuri') }}",
                            "cover_art_url": ""
                        },
                        {
                            "name": "Anthem",
                            "artist": "Emancipator",
                            "album": "Soon It Will Be Cold Enough",
                            "url": "{{ route('play', 'MawarBeduri') }}",
                            "cover_art_url": ""
                        }
                    ]
                }
            }
        });

    </script>
@endsection
