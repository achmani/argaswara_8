@extends('layouts.home')
@section('content')
    <style>
        .play-pause {
            display: inline-block;
            width: 32px;
            height: 32px;
            cursor: pointer;
            vertical-align: middle;
            margin-right: 10px;
        }

        .play-pause.amplitude-paused {
            background: url("../img/play.svg");
            background-size: 32px 32px;
        }

        .play-pause.amplitude-playing {
            background: url("../img/pause.svg");
            background-size: 32px 32px;
        }


        .player-music {
            /* width: 85px; */
            /* padding: 2px; */
            /* height: 85px; */
            display: flex;
            justify-content: center;
            /* border-color: aquamarine; */

            /* border: 5px; */
            position: absolute;
            top: -40px;
            border-radius: 60%;
        }

        .player-controller {
            position: absolute;
            height: inherit;

            display: flex;
            justify-content: center;
        }

        /* #play-pause {
                                                            display: inline-block;
                                                            width: 32px;
                                                            height: 32px;
                                                            cursor: pointer;
                                                            vertical-align: middle;
                                                            margin-right: 10px;
                                                        }

                                                        #play-pause.amplitude-paused {
                                                            background: url("../img/play.svg");
                                                            background-size: 32px 32px;
                                                        }

                                                        #play-pause.amplitude-playing {
                                                            background: url("../img/pause.svg");
                                                            background-size: 32px 32px;
                                                        } */

        /* #next-music {
                                                            display: inline-block;
                                                            height: 32px;
                                                            width: 32px;
                                                            cursor: pointer;
                                                            background: url(../img/next.svg);
                                                            vertical-align: middle;
                                                            background-size: 32px 32px;
                                                        } */

        /* #previous-music {
                                                            display: inline-block;
                                                            height: 32px;
                                                            width: 32px;
                                                            cursor: pointer;
                                                            background: url(../img/previous.svg);
                                                            vertical-align: middle;
                                                            background-size: 32px 32px;
                                                        } */

        .icon-href a {
            color: black;
        }

        .icon-href a .joox {
            width: 40px;
        }

        .gradientBackground {
            /* background: #2c3e50;
                                                        background: -webkit-linear-gradient(to right, #3498db, #2c3e50);
                                                        background: linear-gradient(to right, #3498db, #2c3e50); */

            background: #525252;
            /* fallback for old browsers */
            background: -webkit-linear-gradient(to right, #3d72b4, #525252);
            /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to right, #3d72b4, #525252);
            /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

        }

        .gradientBackground2 {
            background: #43cea2;
            /* fallback for old browsers */
            background: -webkit-linear-gradient(to top, #185a9d, #43cea2);
            /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to top, #185a9d, #43cea2);
            /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

        }

        input[type=range] {
  -webkit-appearance: none;
  margin: 10px 0;
  width: 100%;
}
input[type=range]:focus {
  outline: none;
}
input[type=range]::-webkit-slider-runnable-track {
  width: 100%;
  height: 4px;
  cursor: pointer;
  animate: 0.2s;
  box-shadow: 0px 0px 0px #000000;
  background: #ffffff;
  border-radius: 0px;
  border: 0px solid #000000;
}
input[type=range]::-webkit-slider-thumb {
  box-shadow: 1px 1px 1px #000000;
  border: 1px solid #000000;
  height: 13px;
  width: 10px;
  border-radius: 50px;
  background: #FFFFFF;
  cursor: pointer;
  -webkit-appearance: none;
  margin-top: -5px;
}
input[type=range]:focus::-webkit-slider-runnable-track {
  background: #ffffff;
}
input[type=range]::-moz-range-track {
  width: 100%;
  height: 4px;
  cursor: pointer;
  animate: 0.2s;
  box-shadow: 0px 0px 0px #000000;
  background: #ffffff;
  border-radius: 0px;
  border: 0px solid #000000;
}
input[type=range]::-moz-range-thumb {
  box-shadow: 1px 1px 1px #000000;
  border: 1px solid #000000;
  height: 13px;
  width: 10px;
  border-radius: 50px;
  background: #FFFFFF;
  cursor: pointer;
}
input[type=range]::-ms-track {
  width: 100%;
  height: 4px;
  cursor: pointer;
  animate: 0.2s;
  background: transparent;
  border-color: transparent;
  color: transparent;
}
input[type=range]::-ms-fill-lower {
  background: #ffffff;
  border: 0px solid #000000;
  border-radius: 0px;
  box-shadow: 0px 0px 0px #000000;
}
input[type=range]::-ms-fill-upper {
  background: #ffffff;
  border: 0px solid #000000;
  border-radius: 0px;
  box-shadow: 0px 0px 0px #000000;
}
input[type=range]::-ms-thumb {
  box-shadow: 1px 1px 1px #000000;
  border: 1px solid #000000;
  height: 13px;
  width: 10px;
  border-radius: 50px;
  background: #FFFFFF;
  cursor: pointer;
}
input[type=range]:focus::-ms-fill-lower {
  background: #ffffff;
}
input[type=range]:focus::-ms-fill-upper {
  background: #ffffff;
}
    </style>
    <div class="col-12" style="margin-top: 110px;">
        <div class="fixed-bottom">
            <div class="w-100 d-flex justify-content-center" style="height:48px !important;background-color:rgb(40,40,40);padding:0px;">
                <div class="amplitude-prev" data-amplitude-playlist="emancipator" id="previous-music">
                    <div class="fas fa-backward mt-3" style="margin:0 25px; width: 25px;height:25px;color:white;"></div>
                </div>
                <div class="amplitude-play-pause" data-amplitude-playlist="emancipator"
                    id="play-pause">
                    <div id="play-pause-icon" class="fas fa-play-circle"
                        style="height: 50px;width: 50px;color:white;margin-top:5px;"></div>
                </div>
                <div class="amplitude-next" data-amplitude-playlist="emancipator" id="next-music">
                    <div class="fas fa-forward mt-3" style="margin:0 25px; width: 25px;height:25px;color:white;">
                    </div>
                </div>
                {{-- <div class="amplitude-shuffle amplitude-shuffle-off" id="shuffle-right"></div> --}}
            </div>
            <div class="w-100 d-flex justify-content-center pt-1" style="padding:0 20%;background-color:rgb(40,40,40);">
                <input type="range" class="amplitude-song-slider" data-amplitude-playlist="emancipator"
                    style="width: 100%" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="overflow-hidden">
                    <div class="img-sub-catalogue" style="background-image: url('image/yuni-shara.jpg');"></div>
                </div>
                <p class="mt-3">
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci et voluptates quasi?
                    Minus earum corrupti voluptate officiis
                    beatae non vero veritatis mollitia.
                    Incidunt repudiandae nam velit! Aperiam ullam fugit aspernatur? Lorem ipsum dolor sit amet consectetur,
                    adipisicing elit. Mollitia cupiditate quod fugit dolor a id, ex expedita illum unde assumenda
                    reprehenderit.
                    A officia nihil soluta laborum doloribus, dolore nesciunt odio!
                </p>
            </div>
            <div class="col-md-8">
                <table class="table" style="overflow-x: scroll">
                    <thead>
                        <tr>
                            <th scope="col">Lagu</th>
                            <th scope="col">Dengarkan</th>
                            <th scope="col">Artis</th>
                            <th scope="col">Composer</th>
                            <th scope="col">Genre</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>
                                <span class="amplitude-song-container play-pause amplitude-play-pause"
                                    data-amplitude-playlist="emancipator" data-amplitude-song-index="0"></span>
                                Aku Begini Engkau Begitu
                            </th>
                            <td class="icon-href">
                                <a href="#"><img class="joox" src="{{ asset('image/joox.png') }}"></a>
                                <a href="#"><i class="fas fa-music mx-2"></i></a>
                                <a href="#"><i class="fab fa-spotify mx-2"></i></a>
                                <a href="#"><i class="fab fa-youtube mx-2"></i></a>
                            </td>
                            <td>Yuni Shara</td>
                            <td>John Doe</td>
                            <td>Pop</td>
                        </tr>
                        <tr>
                            <th>
                                <span class="amplitude-song-container play-pause amplitude-play-pause"
                                    data-amplitude-playlist="emancipator" data-amplitude-song-index="1"></span>
                                Widuri
                            </th>
                            <td class="icon-href">
                                <a href="#"><img class="joox" src="{{ asset('image/joox.png') }}"></a>
                                <a href="#"><i class="fas fa-music mx-2"></i></a>
                                <a href="#"><i class="fab fa-spotify mx-2"></i></a>
                                <a href="#"><i class="fab fa-youtube mx-2"></i></a>
                            </td>
                            <td>Yuni Shara</td>
                            <td>John Doe</td>
                            <td>Pop</td>
                        </tr>
                        <tr>
                            <th>
                                <span class="amplitude-song-container play-pause amplitude-play-pause"
                                    data-amplitude-playlist="emancipator" data-amplitude-song-index="2"></span>
                                Mawar Berduri
                            </th>
                            <td class="icon-href">
                                <a href="#"><img class="joox" src="{{ asset('image/joox.png') }}"></a>
                                <a href="#"><i class="fas fa-music mx-2"></i></a>
                                <a href="#"><i class="fab fa-spotify mx-2"></i></a>
                                <a href="#"><i class="fab fa-youtube mx-2"></i></a>
                            </td>
                            <td>Yuni Shara</td>
                            <td>John Doe</td>
                            <td>Pop</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-12" style="background-color: rgb(235, 235, 235);">
        <div class="montserrat mt-4 ml-2">More by Yuni Shara</div>
        <div class="w-100 d-flex new-song-carousel overflow-scroll">
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/yuni-shara.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Kaka</div>
                        <div class="font-italic mb-3 font-14">Terlalu Manis</div>
                    </div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/irwansyah.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Irwansyah</div>
                        <div class="font-italic mb-3 font-14">Pencinta Wanita</div>
                    </div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/giring.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Giring Nidji</div>
                        <div class="font-italic mb-3 font-14">Laskar Pelangi</div>
                    </div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/nirina.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Nirina Zubir</div>
                        <div class="font-italic mb-3 font-14">Pandangan Pertama</div>
                    </div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/pasha.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Pasha Ungu</div>
                        <div class="font-italic mb-3 font-14">Cinta Dalam Diam</div>
                    </div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/dewa-19.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Dewa 19</div>
                        <div class="font-italic mb-3 font-14">Kangen</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="montserrat mt-4 ml-2">Related Artist</div>
        <div class="w-100 d-flex new-song-carousel overflow-scroll">
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/kaka-slank.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Kaka</div>
                        <div class="font-italic mb-3 font-14">Terlalu Manis</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2"
                        style="background-image: url('image/armand-gigi.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Armand Maulana</div>
                    <div class="font-italic mb-3 font-14">Pemimpin Dari Surga</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/dewa-19.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Dewa 19</div>
                        <div class="font-italic mb-3 font-14">Kangen</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/crishye.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Crishye</div>
                    <div class="font-italic mb-3 font-14"> Seperti Yang Kau Minta</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/ebied.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Ebied G.Ade</div>
                        <div class="font-italic mb-3 font-14">Camelia</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/tomi-j-p.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Tomy J Pisa</div>
                    <div class="font-italic mb-3 font-14">Dibatas Kota Ini</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/pasha.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Pasha</div>
                        <div class="font-italic mb-3 font-14">Para Pencari Tuhan</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/irwansyah.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Irwansyah</div>
                    <div class="font-italic mb-3 font-14">Pencinta Wanita</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/acha.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Acha Septriasa</div>
                        <div class="font-italic mb-3 font-14">Sampai Menutup Mata</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/nirina.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Nirina</div>
                    <div class="font-italic mb-3 font-14">Pandangan Pertama</div>
                </div>
            </div>
            <div class="container-new-song flex-change-clg">
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/giring.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="h-artist-clg">
                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Giring</div>
                        <div class="font-italic mb-3 font-14">Laskar Pelangi</div>
                    </div>
                </div>
                <div>
                    <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/ariel.jpg');">
                        <div class="bg-card-clg-artist">
                            <div class="container-play-music">
                                <i class="far fa-play-circle play-music pm-xl"></i>
                            </div>
                        </div>
                    </a>
                    <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">Ariel Noah</div>
                    <div class="font-italic mb-3 font-14">Tak Ada Yang Abadi</div>
                </div>
            </div>
        </div>
        <div class="w-100" style="background-color: rgb(235, 235, 235);padding:15px;"></div>
    </div>
    <div class="col-12">
        <div class="montserrat text-center my-4">Blog by Yuni Shara</div>
        <div class="row">
            <a class="col-md-4 my-anchor" href="#">
                <div class="overflow-hidden">
                    <div class="img-sub-catalogue" style="background-image: url('image/yuni-shara.jpg');"></div>
                </div>
                <div class="mt-2" style="font-weight: 900;">
                    Lorem ipsum dolor sit amet.
                </div>
                <p class="mt-2">
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci et voluptates quasi?
                    Minus earum corrupti voluptate officiis
                    beatae non vero veritatis mollitia.
                    Incidunt repudiandae nam velit! Aperiam ullam fugit aspernatur?
                </p>
            </a>
            <a class="col-md-4 my-anchor" href="#">
                <div class="overflow-hidden">
                    <div class="img-sub-catalogue" style="background-image: url('image/yuni-shara-2.jpg');"></div>
                </div>
                <div class="mt-2" style="font-weight: 900;">Lorem ipsum dolor sit amet.</div>
                <p class="mt-2">
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci et voluptates quasi?
                    Minus earum corrupti voluptate officiis
                    beatae non vero veritatis mollitia.
                    Incidunt repudiandae nam velit! Aperiam ullam fugit aspernatur?
                </p>
            </a>
            <a class="col-md-4 my-anchor" href="#">
                <div class="overflow-hidden">
                    <div class="img-sub-catalogue" style="background-image: url('image/yuni-shara-3.jpg');"></div>
                </div>
                <div class="mt-2" style="font-weight: 900;">Lorem ipsum dolor sit amet.</div>
                <p class="mt-2">
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci et voluptates quasi?
                    Minus earum corrupti voluptate officiis
                    beatae non vero veritatis mollitia.
                    Incidunt repudiandae nam velit! Aperiam ullam fugit aspernatur?
                </p>
            </a>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/amplitudejs@5.2.0/dist/amplitude.js"></script>
    <script>
        $(document).ready(function() {
            $('#play-pause').click(function() {
                $("#play-pause-icon").removeClass(["fas fa-pause-circle", "fas fa-play-circle"]);
                if ($(this).hasClass("amplitude-paused")) {
                    $("#play-pause-icon").addClass("fas fa-pause-circle");
                } else {
                    $("#play-pause-icon").addClass("fas fa-play-circle");
                }
            });

            $('.play-pause').click(function() {
                $("#play-pause-icon").removeClass(["fas fa-pause-circle", "fas fa-play-circle"]);
                if ($(this).hasClass("amplitude-paused")) {
                    console.log("ha");
                    $("#play-pause-icon").addClass("fas fa-pause-circle");
                } else {
                    console.log("hi");
                    $("#play-pause-icon").addClass("fas fa-play-circle");
                }
            });
            Amplitude.init({
                "songs": [{
                    "name": "First Snow",
                    "artist": "Emancipator",
                    "album": "Soon It Will Be Cold Enough",
                    "url": "{{ route('play', 'AkuBeginiEngkauBegitu') }}",
                    "cover_art_url": "../album-art/soon-it-will-be-cold-enough.jpg"
                }],
                "playlists": {
                    "emancipator": {
                        songs: [{
                                "name": "First Snow",
                                "artist": "Emancipator",
                                "album": "Soon It Will Be Cold Enough",
                                "url": "{{ route('play', 'AkuBeginiEngkauBegitu') }}",
                                "cover_art_url": ""
                            },
                            {
                                "name": "Dusk To Dawn",
                                "artist": "Emancipator",
                                "album": "Dusk To Dawn",
                                "url": "{{ route('play', 'Widuri') }}",
                                "cover_art_url": ""
                            },
                            {
                                "name": "Anthem",
                                "artist": "Emancipator",
                                "album": "Soon It Will Be Cold Enough",
                                "url": "{{ route('play', 'MawarBeduri') }}",
                                "cover_art_url": ""
                            }
                        ]
                    }
                }
            });
        });

    </script>
@endsection
