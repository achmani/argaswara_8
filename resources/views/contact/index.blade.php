<x-app-layout>
    @section('content')
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="btn-datatables-group">
                    </div>
                    <br>
                    <br>
                    {{ $dataTable->table() }}
                </div>
            </div>
        </div>
        <div class="modal fade" id="formDetail" tabindex="-1" role="dialog" aria-labelledby="formModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="formModalLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="detailModal" class="col-md-12">
                            </br>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @section('scripts')
        {{ $dataTable->scripts() }}

        <script>
            $(document).ready(function() {
                $("#data-table").on("click", ".detail", function() {
                    $("#detailModal").html( $(this).attr('data-text') );
                    $("#formDetail").modal('show');
                });
            });

        </script>

    @endsection
</x-app-layout>
