@extends('layouts.home')
@section('content')
    <div class="col-12 px-0">
        <div class="jumbotron-img d-flex justify-content-center" style="background-image: 
                    linear-gradient(to right, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url('image/background.jpg');padding: 5% 0 !important;">
            <div class="text-jumbotron-img">
                <div class="montserrat" style="color: white;font-size:1vw;text-align: center">HOME - Blog</div>
                <div class="montserrat" style="color: white;font-size:3vw;text-align: center">RHOMA IRAMA</div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="row" style="padding: 30px;">
            <div class="col-9" style="background-color: gray;height: 600px;">
            </div>
            <div class="col-md-3">
                <a href="" class="card-clg-artist w-clg-100"
                    style="background-image: url('image/compressjpeg/RhomaIrama-min.jpg');">
                    <div class="bg-card-clg-artist">
                        <div class="container-play-music">
                            <svg class="svg-inline--fa fa-play-circle fa-w-16 play-music pm-xl" aria-hidden="true"
                                focusable="false" data-prefix="far" data-icon="play-circle" role="img"
                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                <path fill="currentColor"
                                    d="M371.7 238l-176-107c-15.8-8.8-35.7 2.5-35.7 21v208c0 18.4 19.8 29.8 35.7 21l176-101c16.4-9.1 16.4-32.8 0-42zM504 256C504 119 393 8 256 8S8 119 8 256s111 248 248 248 248-111 248-248zm-448 0c0-110.5 89.5-200 200-200s200 89.5 200 200-89.5 200-200 200S56 366.5 56 256z">
                                </path>
                            </svg><!-- <i class="far fa-play-circle play-music pm-xl"></i> Font Awesome fontawesome.com -->
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 pl-0">
                <br>
                <h1>Composer</h1>
                <p style="color: gray">Find out more about the people behind our music</p>
            </div>
        </div>
    </div>
@endsection
