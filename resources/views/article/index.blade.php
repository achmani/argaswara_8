
<x-app-layout>
    @section('content')
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="btn-datatables-group">
                        <a href="{{ route('article-create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                    </div>
                    <br>
                    <br>
                    {{ $dataTable->table() }}
                </div>
            </div>
        </div>
    @endsection

    @section('scripts')
        {{ $dataTable->scripts() }}
        <script>
            $(document).ready(function() {

                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-sweetalert btn-success',
                        cancelButton: 'btn btn-sweetalert btn-danger'
                    },
                    buttonsStyling: false
                })

                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })

                $("#data-table").on("click", ".btn-delete", function() {
                    data = {
                        'id': $(this).attr("data-id")
                    };
                    swalWithBootstrapButtons.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                        reverseButtons: true,
                        onClose: () => {
                            Toast.fire({
                                icon: 'info',
                                title: 'Cancelled delete data'
                            })
                        },
                        preConfirm: (login) => {
                            return fetch("{{ route('composer-delete') }}", {
                                    method: 'post',
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                                            'content')
                                    },
                                    body: JSON.stringify(data)
                                }).then(response => {
                                    if (!response.ok) {
                                        throw new Error(response.statusText)
                                    }
                                    return response.json()
                                })
                                .catch(error => {
                                    Swal.showValidationMessage(
                                        `Request failed: ${error}`
                                    )
                                })
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        // console.log(result);
                        if (result.isConfirmed) {
                            $('#data-table').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Delete data successfully'
                            })
                        } else if (
                            /* Read more about handling dismissals below */
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                            Toast.fire({
                                icon: 'info',
                                title: 'Cancelled delete data'
                            })
                        }
                    })
                });
            });

        </script>
    @endsection
</x-app-layout>
