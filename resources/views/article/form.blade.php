<x-app-layout>
    @section('content')
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    @if ($type == 'create')
                        <livewire:masterdata.article-form :rules="$rules" :type="$type">
                        @elseif($type == "update")
                            <livewire:masterdata.article-form :rules="$rules" :type="$type" :articleId="$data->article_id"
                                :articleImgTemp="$data->article_img" :articleUrl="$data->article_url"
                                :articleTitle="$data->article_title" :articleText="$data->article_text">
                            @elseif($type == "read")
                                <livewire:masterdata.article-form :rules="$rules" :type="$type" :articleId="$data->article_id"
                                    :articleImgTemp="$data->article_img" :articleUrl="$data->article_url"
                                    :articleTitle="$data->article_title" :articleText="$data->article_text">
                    @endif
                </div>
            </div>
        </div>
    @endsection

    @section('scripts')
        <script src="{{ asset('js/form_livewire.js') }}"></script>
        <script>
            $(document).ready(function() {
                var wireId = $("#livewire-form").attr('wire:id');
                var type = "{{ $type }}"

                if (type == "create") {
                    window.livewire.on('submit', (data) => {
                        $('#composerId').val(null).trigger('change');
                        $('.ck-editor__editable').html('');
                        window.editor.setData('');
                    })
                }

                $('#composerId').select2({
                    placeholder: "Choose tags...",
                    minimumInputLength: 2,
                    debug: true,
                    ajax: {
                        url: "{{ route('composer-select2') }}",
                        dataType: 'json',
                        data: function(params) {
                            return {
                                term: params.term || '',
                                page: params.page || 1
                            }
                        },
                        processResults: function(data, params) {
                            params.page = params.page || 1;
                            return {
                                results: data.results,
                                pagination: {
                                    more: (params.page * 10) < data.count_filtered
                                }
                            };
                        },
                        cache: true,
                    }
                });

                $('#composerId').on('change', function(e) {
                    var data = $(this).val();
                    window.livewire.find(wireId).set('composerId', data);
                });


                let img_array = [];

                ClassicEditor
                    .create(document.querySelector('#articleText'), {
                        // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
                        simpleUpload: {
                            uploadUrl: {
                                url: "{{ route('article-upload-image', ['_token' => csrf_token()]) }}"
                            }
                        }
                    })
                    .then(editor => {
                        let editorModel = editor.model;
                        let editorDocument = editorModel.document;

                        editorDocument.on('change:data', (event) => {
                            let root = editorDocument.getRoot();
                            let children = root.getChildren();

                            let img_temp = [];

                            for (let child of children) {
                                if (child.is('image')) {
                                    let img = child._attrs.get('src');
                                    if (img) {
                                        console.log(img);
                                        img_temp.push(img);
                                    }
                                }
                            }

                            img_array.forEach(value => {
                                if (img_temp.indexOf(value) !== -1) {
                                    //Exist
                                } else {
                                    //Doesnt Exist
                                    $.post("{{ route('article-delete-image') }}", {
                                        url: value,
                                        _token: "{{ csrf_token() }}"
                                    });
                                    console.log("DELETE " + value);
                                }
                            });

                            img_array = img_temp;

                            var articleText = editor.getData();
                            window.livewire.find(wireId).set('articleText', articleText);
                        });

                        window.editor = editor;

                    })
                    .catch(err => {
                        console.error(err.stack);
                    });

                    @if(isset($data))
                        // Fetch the preselected item, and add to the control
                        var composerSelect = $('#composerId');
                        $.ajax({
                            type: 'GET',
                            url: "{{ route('article-select2-composer', $data->article_id) }}"
                        }).then(function(data) {
                            // create the option and append to Select2
                            data = data.results;
                            data.forEach(element => {
                                var option = new Option(element.text, element.id, true, true);
                                composerSelect.append(option).trigger('change');
                            });
                            // manually trigger the `select2:select` event
                            composerSelect.trigger({
                                type: 'select2:select',
                                params: {
                                    data: data
                                }
                            });
                        });
                    @endif

            });

        </script>
    @endsection
</x-app-layout>
