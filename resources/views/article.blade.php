@extends('layouts.home')
@section('title', $title)
@section('content')
    <div class="col-12 px-0">
        <div class="jumbotron-img d-flex justify-content-center"
            style="background-image: 
                                                        linear-gradient(to right, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url('/image/jumbotron-composer.jpg');">
            <div class="text-jumbotron-img">
                <div class="montserrat" style="color: white;font-size:1vw;text-align: center">HOME - BLOG </div>
                <div class="montserrat" style="color: white;font-size:3vw;">{{ $article->article_title }} </div>
            </div>
        </div>
    </div>
    <div class="col-12 mt-5 px-8p">
        <div class="row">
            <div class="col-lg-9">
                <div class="img-blog"
                    style="background-image: url('{{ asset('app/articleThumbnail/article-' . $article->article_id . '/' . $article->article_img . '.jpg') }}')">
                </div>
                {{-- <div class="desc-img-blog mt-4">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                    nisi ut aliquip ex ea commodo consequat. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                    laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                    minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </div> --}}
                <div class="my-4 ck-content">
                    {!! $article->article_text !!}
                </div>
                <p class="my-4">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut
                    enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    Ut enim ad minim
                    veniam, quis nostrud exercitation. <i>Red penulis</i>
                </p>
                <div id="disqus_thread"></div>
            </div>
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-12 col-sm-6 col-lg-12">
                        <b class="d-block mb-3">KATEGORI PENCIPTA LAGU</b>
                        @foreach ($composers as $item)
                            <a href="#" class="my-anchor my-1">{{ $item->composer_name }}</a>
                        @endforeach
                    </div>
                    <div class="col-12 col-sm-6 col-lg-12">
                        <b class="d-block mt-5 mt-sm-0 mt-lg-5 mb-3">BERITA TERBARU</b>
                        @foreach ($newest_article as $item)
                            <div class="row my-2">
                                <div class="col-12">
                                    <a href="{{ route('article-show', str_replace(' ', '-', $item->article_url)) }}"
                                        data-disqus-identifier="{{ str_replace(' ', '-', $item->article_url) }}"
                                        class="my-anchor row" style="display: flex !important;">
                                        <div class="col-4">
                                            <div class="img-sub-sm-blog"
                                                style="background-image: url('{{ asset('app/articleThumbnail/article-' . $item->article_id . '/' . $item->article_img . '.jpg') }}')">
                                            </div>
                                        </div>
                                        <div class="col-8 pl-0">
                                            <div class="montserrat">
                                                {{ $item->article_title }}
                                            </div>
                                            <div class="d-flex">
                                                <img src={{ asset('image/icon-time.PNG') }}
                                                    style="height: 22px;width:22px;">
                                                <div class="mx-1">{{ date_format($item->created_at, 'd M Y') }}</div>
                                                <img class="mx-1" src={{ asset('image/icon-comment.PNG') }}
                                                    style="height: 22px;width:22px;">
                                                <div class="mx-1 disqus-comment-count"
                                                    data-disqus-identifier="{{ str_replace(' ', '-', $item->article_url) }}">
                                                </div>
                                                <img class="mx-1" src={{ asset('image/icon-love.PNG') }}
                                                    style="height: 22px;width:22px;">
                                                <div class="mx-1">6</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-9 px-8p">
        <div class="montserrat my-4 title-berita">Berita Terkait</div>
        <div class="row">
            @foreach ($related_article_after as $item)
                <a class="col-md-4 my-anchor my-3"
                    href="{{ route('article-show', str_replace(' ', '-', $item->article_url)) }}"
                    data-disqus-identifier="{{ str_replace(' ', '-', $item->article_url) }}">
                    <div class="overflow-hidden" style="border-radius:10px;">
                        {{-- <div class="img-sub-blog"
                            style="background-image: url('{{ asset('app/articleThumbnail/article-' . $item->article_id . '/' . $item->article_img . '.jpg') }}');padding-top:70%;">
                        </div> --}}
                        <div class="img-sub-blog"
                            style="background-image: url('{{ asset('app/articleThumbnail/article-' . $item->article_id . '/' . $item->article_img . '.jpg') }}');padding-top:70%;">
                        </div>
                    </div>
                    <div class="text-muted mt-3" style="font-size: 11px;"><img src={{ asset('image/icon-time.PNG') }}
                            style="height: 20px;width:20px;">{{ date_format($item->created_at, 'd M Y') }}, in
                        {{ implode(', ', $item->composer->pluck('composer_name')->toArray()) }}
                    </div>
                    <div style="font-weight: 900;">Berita
                        {{ implode(', ', $item->composer->pluck('composer_name')->toArray()) }}
                    </div>
                    <p class="mt-2 mb-1">
                        {!! Str::limit(str_replace('&nbsp;', ' ', strip_tags($item->article_text)), 200) !!}
                    </p>
                    <div class="d-flex align-items-center">
                        <div class="img-author-blog"
                            style="background-image: url('{{ asset('app/photo_profile/' . $item->user->email . '/' . $item->user->profile_photo_path . '_50.jpg') }}')">
                        </div>
                        <b class="mx-2" style="font-size: 12px;">Penulis {{ $item->user->name }}</b>
                    </div>
                </a>
            @endforeach
            @foreach ($related_article_before as $item)
                <a class="col-md-4 my-anchor my-3"
                    href="{{ route('article-show', str_replace(' ', '-', $item->article_url)) }}"
                    ata-disqus-identifier="{{ str_replace(' ', '-', $item->article_url) }}">
                    <div class="overflow-hidden" style="border-radius:10px;">
                        {{-- <div class="img-sub-blog"
                            style="background-image: url('{{ asset('app/articleThumbnail/article-' . $item->article_id . '/' . $item->article_img . '.jpg') }}');padding-top:70%;">
                        </div> --}}
                        <div class="img-sub-blog"
                            style="background-image: url('{{ asset('app/articleThumbnail/article-' . $item->article_id . '/' . $item->article_img . '.jpg') }}');padding-top:70%;">
                        </div>
                    </div>
                    <div class="text-muted mt-3" style="font-size: 11px;"><img src={{ asset('image/icon-time.PNG') }}
                            style="height: 20px;width:20px;">{{ date_format($item->created_at, 'd M Y') }}, in
                        {{ implode(', ', $item->composer->pluck('composer_name')->toArray()) }}
                    </div>
                    <div style="font-weight: 900;">Berita
                        {{ implode(', ', $item->composer->pluck('composer_name')->toArray()) }}
                    </div>
                    <p class="mt-2 mb-1">
                        {!! Str::limit(str_replace('&nbsp;', ' ', strip_tags($item->article_text)), 200) !!}
                    </p>
                    <div class="d-flex align-items-center">
                        <div class="img-author-blog"
                            style="background-image: url('{{ asset('app/photo_profile/' . $item->user->email . '/' . $item->user->profile_photo_path . '_50.jpg') }}')">
                        </div>
                        <b class="mx-2" style="font-size: 12px;">Penulis {{ $item->user->name }}</b>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            /**
             *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
             *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
             */

            var disqus_config = function() {
                this.page.url = window.location.href;
                this.page.identifier = "{{ str_replace(' ', '-', $article->article_url) }}";
                this.page.title = "{{ str_replace(' ', '-', $article->article_url) }}";
            };

            (function() { // REQUIRED CONFIGURATION VARIABLE: EDIT THE SHORTNAME BELOW
                var d = document,
                    s = d.createElement('script');

                s.src =
                    '//argaswara-com.disqus.com/embed.js'; // IMPORTANT: Replace EXAMPLE with your forum shortname!

                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
            })();
        });

    </script>
@endsection
