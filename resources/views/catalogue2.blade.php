<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/front_end.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/front_end_2.css') }}">
    <link rel="stylesheet" href="../node_modules/owl.carousel/dist/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="../node_modules/slick-carousel/slick/slick.css" />
    <link rel="stylesheet" href="../node_modules/slick-carousel/slick/slick-theme.css" />
    <link rel="stylesheet" href="../node_modules/animate.css/animate.min.css" />
    <title>Catalogue</title>
</head>
<body>
    <div class="container-fluid px-0" style="overflow-x: hidden">
        <div class="row mx-0">
            <div class="col-md-12">
                <div class="sidebar fixed-top" style="display:none;">
                    <a href="#" id="cross-sidebar"><div class="fas fa-times"></div></a>
                    <div class="sidebar-container">
                        <div class="sidebar-item montserrat">
                        <a href="{{route('home')}}">BERANDA</a>
                            <div class="sidebar-hr"></div>
                        </div>
                        <div class="sidebar-item montserrat">
                            <a href="#">CATALOGUE <i class="fas fa-caret-down"></i></a>
                            <div class="sidebar-sub-item"  style="display:none;">
                                List Artist
                            </div> 
                            <div class="sidebar-sub-item"  style="display:none;">
                                Composer
                            </div> 
                            <div class="sidebar-sub-item"  style="display:none;">
                                Arrangers
                            </div> 
                            <div class="sidebar-hr"></div>
                        </div>
                        <div class="sidebar-item montserrat">
                            <a href="#">SERVICES</a>
                            <div class="sidebar-hr"></div>
                        </div>
                        <div class="sidebar-item montserrat">
                            <a href="#">PROFILE</a>
                            <div class="sidebar-hr"></div>
                        </div>
                        <div class="sidebar-item montserrat">
                            <a href="#">HUBUNGI KAMI <i class="fas fa-caret-down"></i></a>
                            <div class="sidebar-sub-item"  style="display:none;">
                                Legal
                            </div> 
                            <div class="sidebar-hr"></div>
                        </div>
                        <div class="sidebar-item montserrat">
                            <a href="#">BERITA</a>
                            <div class="sidebar-hr"></div>
                        </div>
                    </div>
                </div>
                <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-white px-sm-2 px-0" style="transition:0.6s">
                    <img class="logo-navbar" src="{{ asset('image/logo-argaswara.png') }}" >
                    <button class="navbar-toggler" type="button">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                        <li class="nav-item active mx-2">
                            <a class="nav-link montserrat" href="{{route('home')}}">Beranda</a>
                        </li>
                        <li class="nav-item active dropdown mx-2">
                            <a class="nav-link dropdown-toggle montserrat" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Catalogue
                            </a>
                            <div class="active-nav"></div>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item montserrat" href="#" style="font-size:12px;">List Artist</a>
                                <a class="dropdown-item montserrat" href="#" style="font-size:12px;">Composer</a>
                                <a class="dropdown-item montserrat" href="#" style="font-size:12px;">Arrangers</a>
                            </div>
                        </li>
                        <li class="nav-item active mx-2">
                            <a class="nav-link montserrat" href="#">Services</a>
                        </li>
                        <li class="nav-item active mx-2">
                            <a class="nav-link montserrat" href="#">Profile</a>
                        </li>
                        <li class="nav-item active dropdown mx-2">
                            <a class="nav-link dropdown-toggle montserrat" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Hubungi Kami
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item montserrat" href="#" style="font-size:12px;">Legal</a>
                            </div>
                        </li>
                        <li class="nav-item active mx-2">
                            <a class="nav-link montserrat" href="#">Berita</a>
                        </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="col-12 big-artist">
                <div class="w-100 d-flex carousel-big-artist">
                    <div class="my-col-md-4 mx-2 mt-4 mt-sm-0">
                        <div class="montserrat" style="color:red;">LISTEN NOW</div>
                        <div class="roboto mt-1">Yuni Shara</div>
                        <div class="font-italic mt-1 mb-3">Desember Kelabu</div>
                        <a href="" class="card-clg-artist w-clg-100" style="background-image: url('image/yuni-shara.jpg');">
                            <div class="bg-card-clg-artist">
                                <div class="container-play-music">
                                    <i class="far fa-play-circle play-music pm-xl"></i>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="my-col-md-4 mx-2 mt-4 mt-sm-0">
                        <div class="montserrat" style="color:red;">LISTEN NOW</div>
                        <div class="roboto mt-1">Once</div>
                        <div class="font-italic mt-1 mb-3">Pangeran Cinta</div>
                        <a href="" class="card-clg-artist w-clg-100" style="background-image: url('image/once.jpg');">
                            <div class="bg-card-clg-artist">
                                <div class="container-play-music">
                                    <i class="far fa-play-circle play-music pm-xl"></i>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="my-col-md-4 mx-2 mt-4 mt-sm-0">
                        <div class="montserrat" style="color:red;">LISTEN NOW</div>
                        <div class="roboto mt-1">Marthino Lio</div>
                        <div class="font-italic mt-1 mb-3">Ratusan Purnama</div>
                        <a href="" class="card-clg-artist w-clg-100" style="background-image: url('image/marthino-lio-2.jpg');">
                            <div class="bg-card-clg-artist">
                                <div class="container-play-music">
                                    <i class="far fa-play-circle play-music pm-xl"></i>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="my-col-md-4 mx-2 mt-4 mt-sm-0">
                        <div class="montserrat" style="color:red;">LISTEN NOW</div>
                        <div class="roboto mt-1">Kaka</div>
                        <div class="font-italic mt-1 mb-3">Terlalu Manis</div>
                        <a href="" class="card-clg-artist w-clg-100" style="background-image: url('image/kaka-slank.jpg');">
                            <div class="bg-card-clg-artist">
                                <div class="container-play-music">
                                    <i class="far fa-play-circle play-music pm-xl"></i>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 many-carousel">
                <div class="montserrat mt-4 ml-2">Lagu Baru</div>
                <div class="w-100 d-flex new-song-carousel">
                    @for ($i = 0; $i < 6; $i++)
                        <div class="container-new-song">
                            <div>
                                <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/kaka-slank.jpg');">
                                    <div class="bg-card-clg-artist">
                                        <div class="container-play-music">
                                            <i class="far fa-play-circle play-music pm-xl"></i>
                                        </div>
                                    </div>
                                </a>
                                <div class="roboto mt-1 font-12" style="color: red;font-weight: 900;">Kaka</div>
                                <div class="font-italic mb-3 font-12">Terlalu Manis</div>
                            </div>
                            <div>
                                <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/kaka-slank.jpg');">
                                    <div class="bg-card-clg-artist">
                                        <div class="container-play-music">
                                            <i class="far fa-play-circle play-music pm-xl"></i>
                                        </div>
                                    </div>
                                </a>
                                <div class="roboto mt-1 font-12" style="color: red;font-weight: 900;">Kaka</div>
                                <div class="font-italic mb-3 font-12">Terlalu Manis</div>
                            </div>
                        </div>
                    @endfor
                </div>
                <div class="montserrat mt-4 ml-2">Artis</div>
                <div class="w-100 d-flex artist-carousel">
                    @for ($j = 0; $j < 5; $j++)
                        <div class="container-artist">
                            @for ($i = 0; $i < 4; $i++)
                                <div class="d-flex">
                                    <a href="" class="card-clg-artist w-clg-50 my-2" style="background-image: url('image/kaka-slank.jpg');">
                                        <div class="bg-card-clg-artist">
                                            <div class="container-play-music">
                                                <i class="far fa-play-circle play-music pm-sm"></i>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="roboto mt-2 ml-2 font-12" style="color: red;font-weight: 900;">Kaka</div>
                                </div>
                            @endfor
                        </div>
                    @endfor
                </div>
                <div class="montserrat mt-4 ml-2">Top Composer</div>
                <div class="w-100 d-flex new-song-carousel">
                    @for ($i = 0; $i < 6; $i++)
                        <div class="container-new-song">
                            <div>
                                <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/kaka-slank.jpg');">
                                    <div class="bg-card-clg-artist">
                                        <div class="container-play-music">
                                            <i class="far fa-play-circle play-music pm-xl"></i>
                                        </div>
                                    </div>
                                </a>
                                <div class="font-italic mb-3 font-12">Terlalu Manis</div>
                            </div>
                            <div>
                                <a href="" class="card-clg-artist w-clg-100 my-2" style="background-image: url('image/kaka-slank.jpg');">
                                    <div class="bg-card-clg-artist">
                                        <div class="container-play-music">
                                            <i class="far fa-play-circle play-music pm-xl"></i>
                                        </div>
                                    </div>
                                </a>
                                <div class="font-italic mb-3 font-12">Terlalu Manis</div>
                            </div>
                        </div>
                    @endfor
                </div>
            </div>
            <div class="col-lg-3 top-song-artist">
                <div class="row">
                    <div class="col-6 col-lg-12">
                        <div class="montserrat mt-4">Top Song</div>
                        <div class="w-100 d-flex flex-column">
                            @for ($i = 0; $i < 6; $i++)
                                <div class="d-flex">
                                    <a href="" class="card-clg-artist flex-20 my-2" style="background-image: url('image/kaka-slank.jpg');">
                                        <div class="bg-card-clg-artist">
                                            <div class="container-play-music">
                                                <i class="far fa-play-circle play-music pm-sm"></i>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="d-flex flex-80 flex-column">
                                        <div class="roboto mt-2 ml-2 font-12" style="color: red;font-weight: 900;">Kaka</div>
                                        <div class="font-italic ml-2 font-12">Terlalu Manis</div>
                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div>
                    <div class="col-6 col-lg-12">
                        <div class="montserrat mt-4">Top Artist</div>
                        <div class="w-100 d-flex flex-column">
                            @for ($i = 0; $i < 6; $i++)
                                <div class="d-flex">
                                    <a href="" class="card-clg-artist flex-20 my-2" style="background-image: url('image/kaka-slank.jpg');">
                                        <div class="bg-card-clg-artist">
                                            <div class="container-play-music">
                                                <i class="far fa-play-circle play-music pm-sm"></i>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="d-flex flex-80 flex-column">
                                        <div class="roboto mt-2 ml-2 font-12" style="color: red;font-weight: 900;">Kaka</div>
                                        <div class="font-italic ml-2 font-12">Terlalu Manis</div>
                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 px-0 section-footer mt-5 pt-3">
                <div class="row mx-0 pb-3">
                    <div class="col-lg-3">
                        <img class="logo-footer" src="{{ asset('image/logo-argaswara.png') }}" >
                    </div>
                    <div class="col-lg-2 col-md-3 my-3 my-md-5 my-lg-2 d-flex flex-column">
                        <div href="" class="montserrat text-white ">Perusahaan</div>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Tentang</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Pekerjaan</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">For the Record</a>
                    </div>
                    <div class="col-lg-2 col-md-3 my-3 my-md-5 my-lg-2 d-flex flex-column">
                        <div href="" class="montserrat text-white ">Komunitas</div>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Untuk Artis</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Pengembang</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Iklan</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Investor</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Vendor</a>
                    </div>
                    <div class="col-lg-2 col-md-3 my-3 my-md-5 my-lg-2 d-flex flex-column">
                        <div href="" class="montserrat text-white ">Tautan Berguna</div>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Bantuan</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Pemutar Web</a>
                        <a href="" class="roboto text-white pt-3 font-14 my-anchor">Aplikasi Seluler Gratis</a>
                    </div>
                    <div class="col-lg-2 col-md-3 my-3 my-md-5 my-lg-2 d-flex flex-row">
                    <a href="#"><img src="{{asset('image/joox.png')}}" class="joox-footer mx-0 mx-md-2"></a>
                        <a href="#"><i class="fas fa-music mx-2" style="color:white"></i></a>
                        <a href="#"><i class="fab fa-spotify mx-2" style="color:white"></i></a>
                        <a href="#"><i class="fab fa-youtube mx-2" style="color:white"></i></a>
                    </div>
                </div>
                <div class="row mx-0 cr-footer py-2 text-white roboto">
                    <div class="d-flex grey-footer-dll-1">
                        <a href="#" class="text-white px-3 my-anchor">Hukum</a>
                        <a href="#" class="text-white px-3 my-anchor">Pusat Privasi</a>
                        <a href="#" class="text-white px-3 my-anchor">Kebijakan Privasi</a>
                    </div>
                    <div class="d-flex grey-footer-dll-2">
                        <a href="#" class="text-white px-3 my-anchor">Cookies</a>
                        <a href="#" class="text-white px-3 my-anchor">Tentang Iklan</a>
                    </div>
                    <div class="d-flex grey-footer-cp px-2">
                        2020 ARGASWARA | Development by Api Komunika
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
    <script src="{{ asset('js/front_end.js') }}"></script>
    <script src="../node_modules/jquery/dist/jquery.js"></script>
    <script src="../node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="../node_modules/slick-carousel/slick/slick.min.js"></script>
    <script>
        var prevScrollpos = 0;
        $( window ).scroll(function() {
            console.log(prevScrollpos);
            console.log($(document).scrollTop());
            if($(document).scrollTop() > prevScrollpos ){
                $( "nav" ).css( "padding", "20px" );
            }
            else{
                $( "nav" ).css( "padding", "10px" );
            }
            prevScrollpos = window.pageYOffset;
        });
        $(document).ready(function(){
                $('.new-song-carousel').slick({
                infinite:true,
                slidesToShow:5,
                slidesToScroll:1,
                dots:true,
                arrows:false,
                autoplay:true,
                autoplaySpeed: 1500,
                responsive: [
                    {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll:2
                        }
                    },
                    {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll:2
                        }
                    }
                ]
            });
            $('.carousel-big-artist').slick({
                infinite:true,
                slidesToShow:3,
                dots:true,
                arrows:true,
                autoplay:true,
                autoplaySpeed: 1500,
                responsive: [
                    {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true
                        }
                    }
                ]
            });
            $('.artist-carousel').slick({
                infinite:true,
                slidesToShow:4,
                dots:true,
                arrows:false,
                autoplay:true,
                autoplaySpeed: 1500,
                responsive: [
                    {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: true
                        }
                    }
                ]
            });
        });
        $('.sidebar-item').on('click',function(){
            $(this).children(".sidebar-sub-item").slideToggle('fast');
        });
        $('.navbar-toggler').click(function(){
            $('.sidebar').animate({width: 'toggle'});
        });
        $('#cross-sidebar').on('click',function(){
            $(this).parent().animate({width: 'toggle'});
        });
    </script>
</html>