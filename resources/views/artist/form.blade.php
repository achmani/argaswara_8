<x-app-layout>
    @section('content')
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    @if ($type == 'create')
                        <livewire:masterdata.artist-form :rules="$rules" :type="$type">
                        @elseif($type == "update")
                            <livewire:masterdata.artist-form :rules="$rules" :type="$type" :artistId="$data->artist_id"
                                :artistName="$data->artist_name" :artistPopularity="$data->artist_popularity"
                                :artistDesc="$data->artist_desc" :artistImgTemp="$data->artist_img">
                            @elseif($type == "read")
                                <livewire:masterdata.artist-form :rules="$rules" :type="$type" :artistId="$data->artist_id"
                                    :artistName="$data->artist_name" :artistPopularity="$data->artist_popularity"
                                    :artistDesc="$data->artist_desc" :artistImgTemp="$data->artist_img">
                    @endif
                </div>
            </div>
        </div>
    @endsection

    @section('scripts')
        <script src="{{ asset('js/form_livewire.js') }}"></script>
    @endsection
</x-app-layout>
