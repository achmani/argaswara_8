<x-app-layout>
    @section('content')
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    @if ($type == 'create')
                        <livewire:masterdata.carousel-header-form :rules="$rules" :type="$type">
                    @elseif($type == "update")
                        <livewire:masterdata.carousel-header-form :rules="$rules" :type="$type" :headerId="$data->header_id" :headerTitle="$data->header_title" :headerLink="$data->header_link" :imgTemp="$data->header_path">
                    @elseif($type == "read")
                        <livewire:masterdata.carousel-header-form :rules="$rules" :type="$type" :headerId="$data->header_id" :headerTitle="$data->header_title" :headerLink="$data->header_link" :imgTemp="$data->header_path">
                    @endif
                </div>
            </div>
        </div>
    @endsection

    @section('scripts')
        <script src="{{ asset('js/form_livewire.js') }}"></script>
    @endsection
</x-app-layout>
