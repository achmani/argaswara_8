@extends('layouts.home')
@section('content')
    <div class="col-12 px-0">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="carousel-content">
                        <div
                            class="carousel-title playfair-display animate__animated animate__fadeInDown animate__delay-1s">
                            Just Relax and Enjoy With Music
                        </div>
                    </div>
                    <img class="d-block w-100 mt-5" src="image/main-home-slide-2a.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <div class="carousel-content">
                        <div
                            class="carousel-title playfair-display animate__animated animate__fadeInDown animate__delay-1s">
                            We've Created to Make Your Business Work Better
                        </div>
                    </div>
                    <img class="d-block w-100 mt-5" src="image/main-home-slide-3a.jpg" alt="Third slide">
                </div>
                <div class="carousel-item">
                    <div class="carousel-content">
                        <div
                            class="carousel-title playfair-display animate__animated animate__fadeInDown animate__delay-1s">
                            We Are Ready to Publish Your Best Music
                        </div>
                    </div>
                    <img class="d-block w-100 mt-5" src="image/main-home-slide-1a.jpg" alt="First slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div class="col-12 px-0">
        <div class="w-100 d-block text-center barrier">
            <div class="montserrat mb-3 w-100 py-1" style="font-size:1vw;background-color:red;color:white;">
                Selamat Datang di Argaswara
            </div>
            <div class="w-50 d-block mx-auto playfair-display my-3" style="font-size:2.3vw;">
                Kami Menghadirkan "Music For Everyone" Untuk Kemudahan Pencinta Music
            </div>
        </div>
        <div class="w-100 layers-image">
            <img class="layers-image-2" src="{{ asset('image/layers-image-2.png') }}">
        </div>
    </div>
    <div class="col-12 px-md-5" style="position:relative;top:-50px;margin-bottom:30px;">
        <div class="row">
            <div class="col-md-4">
                <div class="w-100 px-3 pt-3">
                    <img src="{{ asset('image/icon-thunder.png') }}">
                    <div class="title-services montserrat">One-click installation</div>
                    <div class="detail-services roboto">Vivamus porta augue a massa blandit feugiat.
                        Cras sapien nisl, malesuada ac consequat ac, pharetra porttitor.</div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="w-100 px-3 pt-3">
                    <img src="{{ asset('image/icon-document.png') }}">
                    <div class="title-services montserrat">Digital and label services</div>
                    <div class="detail-services roboto">Curabitur dignissim lacinia nisi eget placerat.
                        In ut massa sed diam ullamcorper hendrerit non sed diam.</div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="w-100 px-3 pt-3">
                    <img src="{{ asset('image/icon-setting.png') }}">
                    <div class="title-services montserrat">Production music</div>
                    <div class="detail-services roboto">In sed hendrerit nulla. Aenean porttitor,
                        neque vitae ultricies tempus, ipsum augue pharetra elementum urna.</div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="w-100 px-3 pt-3">
                    <img src="{{ asset('image/icon-globe.png') }}">
                    <div class="title-services montserrat">Music licensing</div>
                    <div class="detail-services roboto">Quisque accumsan diam et turpis pharetra, sagittis metus suscipit.
                        Vestibulum ac nisi eu vulputate mage</div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="w-100 px-3 pt-3">
                    <img src="{{ asset('image/icon-rocket.png') }}">
                    <div class="title-services montserrat">Neighboring rights</div>
                    <div class="detail-services roboto">Duis blandit libero non gravida ornare. Vestibulum vitae
                        eleifend tellus. In hendrerit arcu et dapibus tempus..</div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="w-100 px-3 pt-3">
                    <img src="{{ asset('image/icon-graphic.png') }}">
                    <div class="title-services montserrat">Synchronization</div>
                    <div class="detail-services roboto">Duis dictum erat ac ipsum blandit interdum. Cras vitae nibh,
                        et aliquam sem. Duis et turpis et dui imperdiet.</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 px-0" style="background-color:#1f1f21">
        <div class="row mx-0">
            <div class="col-xl-6 col-sm-12 px-0">
                <div class="image-invite"></div>
            </div>
            <div class="col-xl-6">
                <div class="container-invite">
                    <div class="title-invite montserrat">
                        We are here to serve. Composers, artist and publishing partners
                    </div>
                    <div class="detail-invite">
                        Are you a producer or composer looking for the best music publisher? A label owner looking for a
                        publishing partner?
                        A catalog owner or original publisher looking for the best representation? Find out what we can do.
                        <div class="w-100 d-flex mt-5">
                            <div class="btn-invite">READ MORE INFO <i class="fas fa-arrow-right"
                                    style="position: relative;left: 15px;"></i></div>
                            <div class="download-pdf">
                                <a href="#">Or download free pdf example</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 my-sm-5 my-3" style="padding: 0 5%">
        <div class="text-center montserrat mb-sm-5 mb-3 title-selection-composer">A Selection of Our Artist</div>
        <div class="w-100 d-flex carousel-artist">
            @for ($i = 0; $i < 6; $i++)
                <div class="container-new-song">
                    <div>
                        <a href="" class="card-clg-artist w-clg-100 my-2"
                            style="background-image: url('image/kaka-slank.jpg');border-radius:0px;">
                            <div class="bg-card-clg-artist" style="border-radius:0px;">
                                <div class="container-play-music">
                                    <i class="far fa-play-circle play-music pm-xl"></i>
                                </div>
                            </div>
                        </a>
                        <div class="roboto mt-1 font-12" style="color: red;font-weight: 900;">Kaka</div>
                        <div class="font-italic mb-3 font-12">Terlalu Manis</div>
                    </div>
                    <div>
                        <a href="" class="card-clg-artist w-clg-100 my-2"
                            style="background-image: url('image/kaka-slank.jpg');border-radius:0px;">
                            <div class="bg-card-clg-artist" style="border-radius:0px;">
                                <div class="container-play-music">
                                    <i class="far fa-play-circle play-music pm-xl"></i>
                                </div>
                            </div>
                        </a>
                        <div class="roboto mt-1 font-12" style="color: red;font-weight: 900;">Kaka</div>
                        <div class="font-italic mb-3 font-12">Terlalu Manis</div>
                    </div>
                </div>
            @endfor
        </div>
    </div>
    <div class="col-12 my-sm-5 my-3 section-artist">
        <div class="text-center montserrat mb-sm-5 mb-3 title-selection-artist">
            A Selection of Our Composers
        </div>
        <div class="w-100 px-2 px-sm-5 d-flex carousel-composer">
            <a href="#" class="container-composer my-anchor my-anchor">
                <div class="mb-2 d-block position-relative mx-auto bg-composer">
                </div>
                <div class="text-center roboto title-composer">Caffein</div>
            </a>
            <a href="#" class="container-composer my-anchor my-anchor">
                <div class="mb-2 d-block position-relative mx-auto bg-composer">
                </div>
                <div class="text-center roboto title-composer">Caffein</div>
            </a>
            <a href="#" class="container-composer my-anchor my-anchor">
                <div class="mb-2 d-block position-relative mx-auto bg-composer">
                </div>
                <div class="text-center roboto title-composer">Caffein</div>
            </a>
            <a href="#" class="container-composer my-anchor my-anchor">
                <div class="mb-2 d-block position-relative mx-auto bg-composer">
                </div>
                <div class="text-center roboto title-composer">Caffein</div>
            </a>
            <a href="#" class="container-composer my-anchor my-anchor">
                <div class="mb-2 d-block position-relative mx-auto bg-composer">
                </div>
                <div class="text-center roboto title-composer">Caffein</div>
            </a>
            <a href="#" class="container-composer my-anchor my-anchor">
                <div class="mb-2 d-block position-relative mx-auto bg-composer">
                </div>
                <div class="text-center roboto title-composer">Caffein</div>
            </a>
            {{-- <a href="#" class="container-composer my-anchor">
                <img class="mb-2 d-block position-relative mx-auto" style="width:85%;" src="{{ asset('image/nano.jpg') }}"
                    alt="">
                <div class="text-center roboto title-composer">Nano</div>
            </a> --}}
        </div>
        {{-- <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-5 col-md-6 mt-md-0 mt-3 d-flex justify-content-center">
                <a href="#" class="card-artist my-anchor d-block">
                    <div class="image-card-artist" style="background-image: url('image/ari-lasso.jpg');"></div>
                    <div class="title-card-artist montserrat">Ari Lasso</div>
                    <div class="detail-card-artist">Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                        Ducimus libero eius impedit sunt consectetur, adipisicing elit. </div>
                </a>
            </div>
            <div class="col-xl-4 col-lg-5 col-md-6 mt-md-0 mt-3 d-flex justify-content-center">
                <a href="#" class="card-artist my-anchor d-block">
                    <div class="image-card-artist" style="background-image: url('image/melly-goeslaw.jpg');"></div>
                    <div class="title-card-artist montserrat">Melly Goeslaw</div>
                    <div class="detail-card-artist">Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                        Ducimus libero eius impedit sunt consectetur, adipisicing elit. </div>
                </a>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-8 col-sm-10 mt-xl-0 mt-3 d-flex flex-column">
                <a href="#" class="card-artist-sm my-anchor">
                    <div class="image-card-sm" style="background-image: url('image/bcl.jpg');"></div>
                    <div class="card-content-sm montserrat">
                        Bunga Citra Lestari
                        <div class="card-line"></div>
                        <div class="card-desc-sm">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                            Ducimus libero eius impedit sunt consectetur, adipisicing elit.
                        </div>
                    </div>
                </a>
                <a href="#" class="card-artist-sm my-anchor">
                    <div class="image-card-sm" style="background-image: url('image/once.jpg');"></div>
                    <div class="card-content-sm montserrat">
                        Once
                        <div class="card-line"></div>
                        <div class="card-desc-sm">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                            Ducimus libero eius impedit sunt consectetur, adipisicing elit.
                        </div>
                    </div>
                </a>
                <a href="#" class="card-artist-sm my-anchor">
                    <div class="image-card-sm" style="background-image: url('image/marthino-lio.jpg');"></div>
                    <div class="card-content-sm montserrat">
                        Marthino Lio
                        <div class="card-line"></div>
                        <div class="card-desc-sm">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                            Ducimus libero eius impedit sunt consectetur, adipisicing elit.
                        </div>
                    </div>
                </a>
            </div>
        </div> --}}
    </div>
    <div class="col-12 my-5 section-form">
        <div class="row">
            <div class="col-md-6">
                <div class="montserrat text-subscribe mb-4">Subscribe and stay updated</div>
                <form action="">
                    <div class="w-100 d-flex">
                        <input placeholder="Enter your email" class="email-subscribe mr-2" type="email"
                            name="email-subscribe" value="">
                        <button type="submit" class="my-anchor submit-subscribe hover-white">SUBSCRIBE</button>
                    </div>
                    <div class="w-100 d-flex">
                        <div class="line-subscribe"></div>
                        <div style="top: -13px;position: relative;">OR</div>
                        <div class="line-subscribe"></div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-8 pr-5">
                        <div class="montserrat" style="font-size:15px">Visit us</div>
                        <div class="roboto" style="font-size: 13px;margin-top: 8px;text-align: justify;">
                            PT Arga Swara Kencana Musik
                            Muara Karang Blok M IX Selatan No. 40-41
                            Jakarta Utara 14450</div>
                        <div class="montserrat mt-3" style="font-size:15px">Call</div>
                        <div class="roboto" style="font-size: 13px;margin-top: 8px;text-align: justify;">+62 21 661-4871
                        </div>
                    </div>
                    <div class="col-sm-4 my-3 my-sm-0">
                        <div class="montserrat" style="font-size:15px">Socialize</div>
                        <div class="d-flex justify-content-start mt-3 w-100 mx-auto">
                            <a href="#"><i class="fab fa-facebook-square mx-2"
                                    style="font-size:25px;color:mediumblue;"></i></a>
                            <a href="#"><i class="fab fa-instagram mx-2" style="font-size:25px;color:red;"></i></a>
                            <a href="#"><i class="fab fa-twitter mx-2" style="font-size:25px;color:deepskyblue;"></i></a>
                            <a href="#"><i class="fab fa-youtube mx-2" style="font-size:25px;color:red;"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 pl-md-5 mt-5 mt-md-0">
                <div class="montserrat text-subscribe mb-4">Feel free to get in touch</div>
                <form action="">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" class="message-input w-100 py-3" value="" name="name-message"
                                placeholder="Name">
                            <div class="message-border-animate"></div>
                        </div>
                        <div class="col-md-6">
                            <input type="email" class="message-input w-100 py-3" name="email-message" value=""
                                placeholder="Email">
                            <div class="message-border-animate"></div>
                        </div>
                        <div class="col-md-12 mt-3">
                            <input type="text" class="message-input w-100" name="subject-message" value=""
                                placeholder="Subject">
                            <div class="message-border-animate"></div>
                        </div>
                        <div class="col-md-12 mt-3">
                            <textarea class="message-input w-100" style="height:100px;" name="message"
                                placeholder="Message"></textarea>
                            <div class="message-border-animate position-relative" style="top:-7px;"></div>
                        </div>
                        <div class="mt-4 col-12 d-flex">
                            <button type="submit" style="background-color:#393cdd;flex:0 0 50%;"
                                class="my-anchor submit-subscribe hover-white">SEND A MESSAGE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        var prevScrollpos = 0;
        $(window).scroll(function() {
            // console.log(prevScrollpos);
            // console.log($(document).scrollTop());
            if ($(document).scrollTop() > prevScrollpos) {
                $("nav").css("padding", "20px");
            } else {
                $("nav").css("padding", "10px");
            }
            prevScrollpos = window.pageYOffset;
        });
        $(document).ready(function() {
            $('.carousel-artist').slick({
                infinite: true,
                slidesToShow: 5,
                slidesToScroll: 1,
                dots: true,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 1500,
                responsive: [{
                        breakpoint: 1100,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 968,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 576,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }
                ]
            });
            $('.carousel-composer').slick({
                infinite: true,
                slidesToShow: 5,
                slidesToScroll: 1,
                dots: false,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 1500,
                responsive: [{
                        breakpoint: 1100,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 968,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 576,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }
                ]
            });
        });
        $('.sidebar-item').on('click', function() {
            $(this).children(".sidebar-sub-item").slideToggle('fast');
        });
        $('.navbar-toggler').click(function() {
            $('.sidebar').animate({
                width: 'toggle'
            });
        });
        $('#cross-sidebar').on('click', function() {
            $(this).parent().animate({
                width: 'toggle'
            });
        });

    </script>
@endsection
