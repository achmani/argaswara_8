@extends('layouts.home')
@section('title', $title)
@section('content')
    <style>
        .play-pause {
            display: inline-block;
            width: 32px;
            height: 32px;
            cursor: pointer;
            vertical-align: middle;
            margin-right: 10px;
        }

        .amplitude-paused .icon-status{
            background: url("/img/play.svg");
            background-size: 20px 20px;
            background-position: center;
            background-repeat: no-repeat;
            display: inline-block;
            width: 32px;
            height: 32px;
            cursor: pointer;
            vertical-align: middle;
            margin-right: 10px;
        }

        .amplitude-playing .icon-status{
            background: url("/img/pause-white.svg");
            background-size: 20px 20px;
            background-position: center;
            background-repeat: no-repeat;
            display: inline-block;
            width: 32px;com
            height: 32px;
            cursor: pointer;
            vertical-align: middle;
            margin-right: 10px;
        }

        .icon-status {
            background-size: 20px 20px;
            background-position: center;
            background-repeat: no-repeat;
            display: inline-block;
            width: 32px;
            height: 32px;
            cursor: pointer;
            vertical-align: middle;
            margin-right: 10px;
        }

        .play-pause.amplitude-paused {
            background: url("../img/play.svg");
            background-size: 20px 20px;
            background-position: center;
            background-repeat: no-repeat;
        }

        .play-pause.amplitude-playing {
            background: url("../img/pause.svg");
            background-size: 20px 20px;
            background-position: center;
            background-repeat: no-repeat;
        }

        .custom-share {
            /* padding-left: 5px;
                                    padding-right: 5px;
                                    padding-top: 2.5px;
                                    padding-right: 2.5px;
                                    border-radius: 70%;
                                    border: 1px solid black; */
        }

        .player-music {
            /* width: 85px; */
            /* padding: 2px; */
            /* height: 85px; */
            display: flex;
            justify-content: center;
            /* border-color: aquamarine; */

            /* border: 5px; */
            position: absolute;
            top: -40px;
            border-radius: 60%;
        }

        .player-controller {
            position: absolute;
            height: inherit;

            display: flex;
            justify-content: center;
        }

        .icon-href a {
            color: black;
        }

        a>.joox {
            width: 22px;
            /* filter: invert(60%); */
        }

        .amplitude-playing .icon-href a {
            background-color: red;
            color: white !important;
        }

        .amplitude-playing .icon-href a img {
            /* -webkit-filter: invert(100%); */
            /* safari 6.0 - 9.0 */
            /* filter: invert(100%) contrast(160%); */
        }

        .amplitude-playing {
            background-color: red;
            color: white !important;
        }

        .gradientBackground {
            /* background: #2c3e50;
                                                background: -webkit-linear-gradient(to right, #3498db, #2c3e50);
                                                background: linear-gradient(to right, #3498db, #2c3e50); */

            background: #525252;
            /* fallback for old browsers */
            background: -webkit-linear-gradient(to right, #3d72b4, #525252);
            /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to right, #3d72b4, #525252);
            /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

        }

        .gradientBackground2 {
            background: #43cea2;
            /* fallback for old browsers */
            background: -webkit-linear-gradient(to top, #185a9d, #43cea2);
            /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to top, #185a9d, #43cea2);
            /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

        }

        input[type=range] {
            -webkit-appearance: none;
            margin: 10px 0;
            width: 100%;
        }

        input[type=range]:focus {
            outline: none;
        }

        input[type=range]::-webkit-slider-runnable-track {
            width: 100%;
            height: 4px;
            cursor: pointer;
            animate: 0.2s;
            box-shadow: 0px 0px 0px #000000;
            background: #ffffff;
            border-radius: 0px;
            border: 0px solid #000000;
        }

        input[type=range]::-webkit-slider-thumb {
            box-shadow: 1px 1px 1px #000000;
            border: 1px solid #000000;
            height: 13px;
            width: 10px;
            border-radius: 50px;
            background: #FFFFFF;
            cursor: pointer;
            -webkit-appearance: none;
            margin-top: -5px;
        }

        input[type=range]:focus::-webkit-slider-runnable-track {
            background: #ffffff;
        }

        input[type=range]::-moz-range-track {
            width: 100%;
            height: 4px;
            cursor: pointer;
            animate: 0.2s;
            box-shadow: 0px 0px 0px #000000;
            background: #ffffff;
            border-radius: 0px;
            border: 0px solid #000000;
        }

        input[type=range]::-moz-range-thumb {
            box-shadow: 1px 1px 1px #000000;
            border: 1px solid #000000;
            height: 13px;
            width: 10px;
            border-radius: 50px;
            background: #FFFFFF;
            cursor: pointer;
        }

        input[type=range]::-ms-track {
            width: 100%;
            height: 4px;
            cursor: pointer;
            animate: 0.2s;
            background: transparent;
            border-color: transparent;
            color: transparent;
        }

        input[type=range]::-ms-fill-lower {
            background: #ffffff;
            border: 0px solid #000000;
            border-radius: 0px;
            box-shadow: 0px 0px 0px #000000;
        }

        input[type=range]::-ms-fill-upper {
            background: #ffffff;
            border: 0px solid #000000;
            border-radius: 0px;
            box-shadow: 0px 0px 0px #000000;
        }

        input[type=range]::-ms-thumb {
            box-shadow: 1px 1px 1px #000000;
            border: 1px solid #000000;
            height: 13px;
            width: 10px;
            border-radius: 50px;
            background: #FFFFFF;
            cursor: pointer;
        }

        input[type=range]:focus::-ms-fill-lower {
            background: #ffffff;
        }

        input[type=range]:focus::-ms-fill-upper {
            background: #ffffff;
        }

    </style>
    <div class="col-12" style="margin-top: 110px;">
        <div class="row">
            <div class="col-lg-4">
                <div class="img-sub-catalogue"
                    style="background-image: url('{{ asset('app/composer/composer-' . $composer->composer_id . '/' . $composer->composer_img . '.jpg') }}');">
                    <div class="player-slider player-sm">
                        <div id="multi-song-player">
                            {{-- <img data-amplitude-song-info="cover_art_url" />
                            --}}
                            {{-- <div class="bottom-container">
                                <progress class="amplitude-song-played-progress" id="song-played-progress"></progress>
                                <div class="time-container">
                                    <span class="current-time">
                                        <span class="amplitude-current-minutes"></span>:<span
                                            class="amplitude-current-seconds"></span>
                                    </span>
                                    <span class="duration">
                                        <span class="amplitude-duration-minutes"></span>:<span
                                            class="amplitude-duration-seconds"></span>
                                    </span>
                                </div>

                                <div class="control-container">
                                    <div class="amplitude-play-pause" id="play-pause"></div>
                                    <div class="meta-container">
                                        <span data-amplitude-song-info="name" class="song-name"></span>
                                        <span data-amplitude-song-info="artist"></span>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="bottom-container">
                                <div class="container">
                                    <div class="control-container row">
                                        <div class="col-md-9 pr-0 col-8 artist-name">
                                            <span data-amplitude-song-info="name"></span>
                                        </div>
                                        <div class="col-md-3 pl-0 col-4 d-flex control-song">
                                            <div class="amplitude-prev">
                                            </div>
                                            <div id="play-pause" class="amplitude-play-pause amplitude-paused">
                                            </div>
                                            <div class="amplitude-next" data-nextid="1">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="time-container">
                                                <progress class="amplitude-song-played-progress"></progress>
                                                {{-- <span class="current-time">
                                                    <span class="amplitude-current-minutes"
                                                        data-amplitude-song-index="0"></span>:<span
                                                        class="amplitude-current-seconds"
                                                        data-amplitude-song-index="0"></span>
                                                </span> --}}
                                                <span data-amplitude-song-info="artist" class="song-name"></span>
                                                <span class="duration">
                                                    <span class="amplitude-duration-minutes"></span>:<span
                                                        class="amplitude-duration-seconds"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <p style="margin-top:60px;">
                    {{ $composer->composer_desc }}
                </p>
            </div>
            <div class="col-lg-8">
                <div class="w-100">
                    <div style="font-size: 30px;" class="montserrat">{{ $composer->composer_name }}</div>
                    {{-- <div class="montserrat">Label : <span class="roboto" data-amplitude-song-info="label"></span></div> --}}
                    {{-- <div class="montserrat">Composer : <span class="roboto" data-amplitude-song-info="artist"></span></div> --}}
                    <div class="montserrat">Genres : <span class="roboto" data-amplitude-song-info="genre"></span></div>
                </div>
                <br>
                <div class="w-100" style="height: 400px;overflow-y: scroll">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col" style="overflow-x: hidden">Lagu</th>
                            <th scope="col" style="overflow-x: hidden">Composer</th>
                            <th scope="col" style="min-width: 170px !important;">Dengarkan</th>
                            {{-- <th scope="col">Share</th> --}}
                            {{-- <th scope="col">Music Release</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($music as $item)
                            <tr id="music-{{$item->music_id}}" class="detail-music amplitude-song-container amplitude-play-pause"
                                data-amplitude-song-index="{{ $song_index++ }}">
                                <th>
                                    <span class="icon-status"></span>
                                    {{ $item->music_name }}
                                </th>
                                <th>
                                    {!! implode(', ', $item->composer->pluck("composer_link")->toArray()) !!}
                                </th>
                                <td class="icon-href">
                                    @if($item->music_joox)
                                    <a href="{{ $item->music_joox ? $item->music_joox : '#' }}" target="_blank"><img
                                            class="joox mr-1" src="{{ asset('image/icon/joox.png') }}"></a>
                                    @endif
                                    @if($item->music_itunes)
                                    <a href="{{ $item->music_itunes ? $item->music_itunes : '#' }}" target="_blank"><img
                                            class="joox mx-1" src="{{ asset('image/icon/itunes.png') }}"></a>
                                    @endif
                                    @if($item->music_spotify)
                                    <a href="{{ $item->music_spotify ? $item->music_spotify : '#' }}" target="_blank"><img
                                            class="joox mx-1" src="{{ asset('image/icon/spotify.png') }}"></a>
                                    @endif
                                    @if($item->music_youtube)
                                    <a href="{{ $item->music_youtube ? $item->music_youtube : '#' }}" target="_blank"><img
                                            class="joox mx-1" src="{{ asset('image//icon/youtube.png') }}"></a>
                                    @endif
                                </td>
                                {{-- <td class="icon-href">
                                    <a href="{{ route('composer_share') }}"><img class="joox mx-1"
                                            src="{{ asset('image/share-min.png') }}"></a>
                                </td> --}}
                                {{-- <td class="icon-href">
                                    {{ $item->music_release }}
                                </td> --}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="px-8p" style="margin-bottom: 60px;">
        <div class="row">
            <div class="col-12 py-3" style="background-color: rgb(235, 235, 235);margin-top:30px">
                <div class="montserrat mt-4 ml-2" style="font-size: 25px;">Composer Terkait</div>
                <div class="w-100 d-flex new-song-carousel overflow-scroll">
                    @foreach ($composerRelated as $item)
                        <div class="container-new-song flex-change-clg">
                            <div>
                                <a href="{{ route('player',str_replace(' ', '-', $item->composer_name)) }}" class="card-clg-artist w-clg-100 my-2"
                                    style="background-image: url('{{ asset('app/composer/composer-' . $item->composer_id . '/' . $item->composer_img . '.jpg') }}');">
                                    <div class="bg-card-clg-artist">
                                        <div class="container-play-music"  style="position: relative">
                                            <i class="far fa-play-circle play-music pm-xl"></i>
                                            <div style="background-color:black; width: 100%;position: absolute;bottom: 0;color:white;text-align: center;padding: 5px">
                                                <h5>{{ $item->composer_name }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div style="display: none" class="col-12">
                <div class="montserrat text-center my-4 title-berita">Berita {{ $composer->composer_name }}
                </div>
                <div class="row">
                    @foreach ($article as $item)
                        <a class="col-md-4 my-anchor my-3" href="{{ route('article-show', str_replace(' ', '-', $item->article_url) ) }}">
                            <div class="overflow-hidden" style="border-radius:10px;">
                                {{-- <div class="img-sub-blog"
                                    style="background-image: url('{{ asset('app/articleThumbnail/article-' . $item->article_id . '/' . $item->article_img . '.jpg') }}');padding-top:70%;">
                                </div> --}}
                                <div class="img-sub-blog" style="background-image: url('{{ asset('app/articleThumbnail/article-' . $item->article_id . '/' . $item->article_img . '.jpg') }}');padding-top:70%;">
                                </div>
                            </div>
                            <div class="text-muted mt-3" style="font-size: 11px;"><img
                                    src={{ asset('image/icon-time.PNG') }} style="height: 20px;width:20px;">3 Februari 2016,
                                in Rhoma Irama</div>
                            <div style="font-weight: 900;">{{ $item->article_title }}</div>
                            <p class="mt-2 mb-1">
                                {!! Str::limit( str_replace("&nbsp;", ' ', strip_tags($item->article_text)), 200) !!}
                            </p>
                            <div class="d-flex align-items-center">
                                <div class="img-author-blog"></div>
                                <b class="mx-2" style="font-size: 12px;">Penulis Admin</b>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            // document.addEventListener("turbo:load", function() {
            // $('#play-pause').click(function() {
            //     $("#play-pause-icon").removeClass(["fas fa-pause-circle", "fas fa-play-circle"]);
            //     if ($(this).hasClass("amplitude-paused")) {
            //         $("#play-pause-icon").addClass("fas fa-pause-circle");
            //     } else {
            //         $("#play-pause-icon").addClass("fas fa-play-circle");
            //     }
            // });

            // $('.play-pause').click(function() {
            //     $("#play-pause-icon").removeClass(["fas fa-pause-circle", "fas fa-play-circle"]);
            //     if ($(this).hasClass("amplitude-paused")) {
            //         console.log("ha");
            //         $("#play-pause-icon").addClass("fas fa-pause-circle");
            //     } else {
            //         console.log("hi");
            //         $("#play-pause-icon").addClass("fas fa-play-circle");
            //     }
            // });

            var song_array = {!! $music_json !!};
            var play = {!! $play !!};
            var song_array_length = song_array.length;
            var nextid = 1;

            Amplitude.init({
                "songs": song_array,
            });

            Amplitude.pause();

            $(".amplitude-next").click(function() {
                // var nextid = $(this).data("nextid");
                if (nextid >= song_array_length) {
                    Amplitude.playSongAtIndex(0);
                    nextid = 1;
                    // $(this).data("nextid",1);
                } else {
                    nextid = nextid + 1;
                    // $(this).data("nextid",nextid);
                }

                var idmusic = $('tbody .amplitude-playing').attr('id');
                var target = document.getElementById(idmusic);
                target.parentNode.parentNode.parentNode.scrollTop = target.offsetTop - 30;    
            });

            $(".amplitude-prev").click(function() {
                // var nextid = $(".amplitude-next").data("nextid");
                if (nextid <= 1) {
                    // $(".amplitude-next").data("nextid",1);
                    nextid = 1;
                } else {
                    // $(".amplitude-next").data("nextid",nextid-1);
                    nextid = nextid - 1;
                }

                var idmusic = $('tbody .amplitude-playing').attr('id');
                var target = document.getElementById(idmusic);
                target.parentNode.parentNode.parentNode.scrollTop = target.offsetTop - 30;    

            });

            $(".list-play-pause").click(function() {
                var nextidlist = $(this).data("amplitude-song-index");
                nextid = nextidlist + 2;
            });

            if(play){
                $('#music-'+play).click();
            }

            document.addEventListener("turbo:click", function() {
                if(Amplitude){
                    Amplitude.pause();
                }
            });

        });


    </script>
@endsection
