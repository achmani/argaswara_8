@extends('layouts.home')
@section('content')
    <div class="col-12 px-0">
        <div class="jumbotron-img d-flex justify-content-center" style="background-image: 
        linear-gradient(to right, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url('/image/jumbotron-composer.jpg');">
            <div class="text-jumbotron-img">
                <div class="montserrat" style="color: white;font-size:3vw;">OUR COMPOSER</div>
            </div>
        </div>
    </div>
    <div class="px-8p">
        <div class="row">
            <div class="col-12">
                <div class="text-center montserrat title-composer-page">
                    COMPOSER
                    <div class="text-center roboto" style="font-size: 0.5em">
                        Find out more about the people behind our music
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>Shorts</label>
                        <select class="form-control" id="shortsSelect">
                            <option value="1">Alphabet</option>
                            <option {{ ($shorts == 2) ? "selected":"" }} value="2">Popularity</option>
                        </select>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            @foreach ($composer as $item)
                                <div class="col-12 col-md-3 col-xl-3">
                                    <a href="{{ route('player',str_replace(' ', '-', $item->composer_name)) }}" class="card-clg-artist w-clg-100 my-2"
                                        style="background-image: url('{{ asset('app/composer/composer-'.$item->composer_id."/".$item->composer_img."_300.jpg") }}');">
                                        <div class="bg-card-clg-artist">
                                            <div class="container-play-music" style="position: relative">
                                                <i class="far fa-play-circle play-music pm-xl"></i>
                                                <div style="background-color:black; width: 100%;position: absolute;bottom: 0;color:white;text-align: center;padding: 5px">
                                                    <h5>{{ $item->composer_name }}</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    {{-- <div class="h-artist-clg">
                                        <div class="roboto mt-1 font-14" style="color: black;font-weight: 900;">{{ $item->composer_name }}</div>
                                    </div> --}}
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-12  mt-5">
                        <div class="row justify-content-center">
                            <nav aria-label="Page navigation example" class="mt-5">
                                <ul class="pagination justify-content-center">
                                    <li class="page-item {{ ($page > 1) ? "" : "disabled" }}"> 
                                        <a class="page-link" href="{{ route('composer2',['page'=> $page-1,'shorts'=> $shorts]) }}"> < </a>
                                    </li>
                                    @if($page-3-1 >= 1)
                                        <li class="page-item"><a class="page-link" href="{{ route('composer2',['page'=> $page-3-1,'shorts'=> $shorts]) }}">...</a></li>
                                    @endif
                                    @for ($i = $page-3; $i < $page; $i++)
                                        @if($i <= ceil($count_composer/$length) && $i >= 1)
                                        <li class="page-item {{ ($i == $page) ? "active" : "" }}"><a class="page-link" href="{{ route('composer2',['page'=> $i,'shorts'=>$shorts]) }}">{{ $i }}</a></li>
                                        @endif
                                    @endfor
                                    @if($count_composer - $page > 3)
                                        @for ($i = $page; $i < $page+3; $i++)
                                            @if($i <= ceil($count_composer/$length))
                                                <li class="page-item {{ ($i == $page) ? "active" : "" }}"><a class="page-link" href="{{ route('composer2',['page'=> $i,'shorts'=>$shorts]) }}">{{ $i }}</a></li>
                                            @endif
                                        @endfor
                                        @if($page+3 < ceil($count_composer/$length))
                                            <li class="page-item"><a class="page-link" href="{{ route('composer2',['page'=> $page+3,'shorts'=>$shorts]) }}">...</a></li>
                                        @endif
                                    @else
                                        @for ($i = 0; $i*$length_page < $count_composer; $i++)
                                            <li class="page-item {{ ($i+1 == $page) ? "active" : "" }}"><a class="page-link" href="{{ route('composer2',['page'=> $i+1,'shorts'=>$shorts]) }}">{{ $i+1 }}</a></li>
                                        @endfor
                                    @endif
                                    <li class="page-item {{ ($page+1 <= ceil($count_composer/$length)) ? "" : "disabled" }}"> 
                                        <a class="page-link" href="{{ route('composer2',['page'=> $page+1,'shorts'=>$shorts]) }}"> > </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>                  
                </div>
            </div> 
        </div>
    </div>
@endsection

@section('js')
<script>
$(document).ready(function(){
    $( "#shortsSelect" ).change(function() {
        var shorts = $( this ).val();
        var newUrl = window.location.href.slice(0, -1);
        window.location.href = newUrl+shorts;
    });
});
</script>
@endsection