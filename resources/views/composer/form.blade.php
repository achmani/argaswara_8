<x-app-layout>
    @section('content')
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    @if ($type == 'create')
                        <livewire:masterdata.composer-form :rules="$rules" :type="$type">
                        @elseif($type == "update")
                            <livewire:masterdata.composer-form :rules="$rules" :type="$type"
                                :labelId="$data->composer_label"
                                :composerId="$data->composer_id" :composerName="$data->composer_name"
                                :composerPopularity="$data->composer_popularity" :composerDesc="$data->composer_desc"
                                :composerImgTemp="$data->composer_img">
                            @elseif($type == "read")
                                <livewire:masterdata.composer-form :rules="$rules" :type="$type"
                                    :labelId="$data->composer_label"
                                    :composerId="$data->composer_id" :composerName="$data->composer_name"
                                    :composerPopularity="$data->composer_popularity" :composerDesc="$data->composer_desc"
                                    :composerImgTemp="$data->composer_img">
                    @endif
                </div>
            </div>
        </div>
    @endsection

    @section('scripts')
        <script src="{{ asset('js/form_livewire.js') }}"></script>
        <script>
            $(document).ready(function() {
                
                var wireId = $("#livewire-form-composer").attr('wire:id');
                
                 $('#labelId').select2({
                    placeholder: "Choose tags...",
                    minimumInputLength: 2,
                    debug: true,
                    ajax: {
                        url: "{{ route('label-select2') }}",
                        dataType: 'json',
                        data: function(params) {
                            return {
                                term: params.term || '',
                                page: params.page || 1
                            }
                        },
                        processResults: function(data, params) {
                            params.page = params.page || 1;
                            return {
                                results: data.results,
                                pagination: {
                                    more: (params.page * 10) < data.count_filtered
                                }
                            };
                        },
                        cache: true,
                    }
                });
                
                $('#labelId').on('change', function(e) {
                    var data = $(this).val();
                    window.livewire.find(wireId).set('labelId', data);
                });
                
                @if(isset($data))
                    // Fetch the preselected item, and add to the control
                    var labelSelect = $('#labelId');
                    $.ajax({
                        type: 'GET',
                        url: "{{ route('label-select2-composer', $data->composer_label) }}"
                    }).then(function(data) {
                        // create the option and append to Select2
                        data = data.results;
                        data.forEach(element => {
                            var option = new Option(element.text, element.id, true, true);
                            labelSelect.append(option).trigger('change');
                        });
                        // manually trigger the `select2:select` event
                        labelSelect.trigger({
                            type: 'select2:select',
                            params: {
                                data: data
                            }
                        });
                    });
                @endif

                
            });

        </script>
    @endsection
</x-app-layout>
