@extends('layouts.home')
@section('title', $title)
@section('content')
    <div class="col-12 px-0">
        <div class="jumbotron-img d-flex justify-content-center" style="background-image: 
        linear-gradient(to right, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url('{{ $img }}');">
            <div class="text-jumbotron-img">
                <div class="montserrat" style="color: white;font-size:3vw;">{{ $data->page_title }}</div>
            </div>
        </div>
    </div>
    <div class="col-12 mt-5 px-8p" style="margin-bottom: 60px">
        <div class="row">
            <div class="col-lg-12">
                <div class="my-4 ck-content">
                    {!! $data->page_text !!}
                </div>
                {{-- <div id="disqus_thread"></div> --}}
            </div>
            <div class="col-lg-3">
                <div class="row">
                </div>
            </div>
        </div>
    </div>
 
@endsection
@section('js')
@endsection