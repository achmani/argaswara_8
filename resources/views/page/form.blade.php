<x-app-layout>
    @section('content')
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    @if ($type == 'create')
                        <livewire:masterdata.page-form :rules="$rules" :type="$type">
                        @elseif($type == "update")
                            <livewire:masterdata.page-form :rules="$rules" :type="$type" :pageId="$data->page_id"
                                :pageImgTemp="$data->page_img" :pageUrl="$data->page_url" :pageTitle="$data->page_title"
                                :pageText="$data->page_text">
                            @elseif($type == "read")
                                <livewire:masterdata.page-form :rules="$rules" :type="$type" :pageId="$data->page_id"
                                    :pageImgTemp="$data->page_img" :pageUrl="$data->page_url" :pageTitle="$data->page_title"
                                    :pageText="$data->page_text">
                    @endif
                </div>
            </div>
        </div>
    @endsection

    @section('scripts')
        <script src="{{ asset('js/form_livewire.js') }}"></script>
        <script>
            $(document).ready(function() {
                var wireId = $("#livewire-form").attr('wire:id');
                var type = "{{ $type }}"

                if (type == "create") {
                    window.livewire.on('submit', (data) => {
                        $('#composerId').val(null).trigger('change');
                        $('.ck-editor__editable').html('');
                        window.editor.setData('');
                    })
                }

                let img_array = [];

                ClassicEditor
                    .create(document.querySelector('#pageText'), {
                        // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
                        simpleUpload: {
                            uploadUrl: {
                                url: "{{ route('page-upload-image', ['_token' => csrf_token()]) }}"
                            }
                        }
                    })
                    .then(editor => {
                        let editorModel = editor.model;
                        let editorDocument = editorModel.document;

                        editorDocument.on('change:data', (event) => {
                            let root = editorDocument.getRoot();
                            let children = root.getChildren();

                            let img_temp = [];

                            for (let child of children) {
                                if (child.is('image')) {
                                    let img = child._attrs.get('src');
                                    if (img) {
                                        console.log(img);
                                        img_temp.push(img);
                                    }
                                }
                            }

                            img_array.forEach(value => {
                                if (img_temp.indexOf(value) !== -1) {
                                    //Exist
                                } else {
                                    //Doesnt Exist
                                    $.post("{{ route('page-delete-image') }}", {
                                        url: value,
                                        _token: "{{ csrf_token() }}"
                                    });
                                    console.log("DELETE " + value);
                                }
                            });

                            img_array = img_temp;

                            var pageText = editor.getData();
                            window.livewire.find(wireId).set('pageText', pageText);
                        });

                        window.editor = editor;

                    })
                    .catch(err => {
                        console.error(err.stack);
                    });

            });

        </script>
    @endsection
</x-app-layout>
