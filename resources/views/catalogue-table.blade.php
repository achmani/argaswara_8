@extends('layouts.home')
@section('title', $title)
@section('content')
    <div class="col-12 px-0">
        <div class="jumbotron-img d-flex justify-content-center"
            style="background-image: url('/image/background-songs.jpg');background-size: cover">
            <div class="text-jumbotron-img" style="padding: 18% 0 !important">
            </div>
        </div>
    </div>
    <div class="px-8p">
        <div class="row">
            <div class="col-12 big-artist">
                <div class="montserrat-extra-bold my-4 ml-3" style="font-size: 40px;">Song Catalogue</div>
                <hr class="px-2">
            </div>
            <div class="col-12">
                {{-- <div class="montserrat-extra-bold mt-4 ml-2" style="font-size: 40px;">Composer</div> --}}
                <div class="row" style="margin-bottom: 60px;margin-top: 60px">
                    {{ $dataTable->table() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script src="{{ asset('js/app.js') }}" data-turbolinks-track="true" data-turbo-track="reload"></script>
<script>
    
</script>
{{-- $dataTable->scripts() --}}
<script src="{{ asset('js/music_datatable.js') }}" data-turbolinks-track="false"></script>
<script>
</script>
@endsection
