(function (window, $) {
    window.LaravelDataTables = window.LaravelDataTables || {}; window.LaravelDataTables["data-table"] = $("#data-table").DataTable({
        "serverSide": true, "processing": true, "ajax": {
            "url": "https:\/\/argaswara.com\/catalogue", "type": "GET", "data": function (data) {
                for (var i = 0, len = data.columns.length; i < len; i++) {
                    if (!data.columns[i].search.value) delete data.columns[i].search;
                    if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                    if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                    if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
                }
                delete data.search.regex;
            }
        }, "columns": [{ "data": "song_title", "name": "music_name", "title": "Song Title", "orderable": true, "searchable": true }, { "data": "performer", "name": "artist.artist_name", "title": "Performer", "orderable": false, "searchable": true }, { "data": "composer", "name": "composer.composer_name", "title": "Composer", "orderable": false, "searchable": true }, { "data": "genre", "name": "genre.genre_value", "title": "Genre", "orderable": false, "searchable": true }, { "data": "release", "name": "music_release", "title": "Release", "orderable": true, "searchable": true }], "responsive": true, "autoWidth": true, "order": [[0, "asc"]]
    });
})(window, jQuery);
