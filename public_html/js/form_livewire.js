$(document).ready(function() {
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-sweetalert btn-success",
            cancelButton: "btn btn-sweetalert btn-danger"
        },
        buttonsStyling: false
    });

    const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: toast => {
            toast.addEventListener("mouseenter", Swal.stopTimer);
            toast.addEventListener("mouseleave", Swal.resumeTimer);
        }
    });

    Livewire.on("submit", data => {
        Toast.fire({
            icon: data.type,
            title: data.message
        });
    });
});
