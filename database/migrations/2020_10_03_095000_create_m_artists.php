<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMArtists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_artists', function (Blueprint $table) {
            $table->bigIncrements('artist_id');
            $table->string('artist_img');
            $table->string('artist_desc');
            $table->bigInteger('artist_count');
            $table->integer('artist_popularity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_artists');
    }
}
