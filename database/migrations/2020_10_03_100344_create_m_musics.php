<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMMusics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_musics', function (Blueprint $table) {
            $table->bigIncrements('music_id');
            $table->boolean('music_is_file');
            $table->string('music_path');
            $table->string('music_url');
            $table->date('music_release');
            $table->bigInteger('music_count');
            $table->integer('music_popularity');
            $table->unsignedBigInteger('artist_id');
            $table->unsignedBigInteger('composer_id');
            $table->unsignedBigInteger('genre_id');
            $table->foreign('artist_id')->references('artist_id')->on('m_artists');
            $table->foreign('composer_id')->references('composer_id')->on('m_composers');
            $table->foreign('genre_id')->references('genre_id')->on('m_genres');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_songs');
    }
}
