<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUrlExternalMusic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_musics', function (Blueprint $table) {
            $table->string('music_joox')->nullable()->after('music_popularity');
            $table->string('music_itunes')->nullable()->after('music_joox');
            $table->string('music_spotify')->nullable()->after('music_itunes');
            $table->string('music_youtube')->nullable()->after('music_spotify');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_musics', function (Blueprint $table) {
            $table->dropColumn('music_joox');
            $table->dropColumn('music_itunes');
            $table->dropColumn('music_spotify');
            $table->dropColumn('music_youtube');
        });
    }
}
