<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_pages', function (Blueprint $table) {
            $table->bigIncrements('page_id');
            $table->string('page_url')->unique();
            $table->string('page_title');
            $table->longText('page_text');
            $table->unsignedBigInteger('page_user_id');
            $table->foreign('page_user_id')->references('id')->on('users');
            $table->integer('page_count');
            $table->string('page_img');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_pages');
    }
}
