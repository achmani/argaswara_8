<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMComposers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_composers', function (Blueprint $table) {
            $table->bigIncrements('composer_id');
            $table->string('composer_img');
            $table->string('composer_desc');
            $table->bigInteger('composer_count');
            $table->integer('composer_popularity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_composers');
    }
}
