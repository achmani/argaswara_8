<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeaderMusicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('header_music', function (Blueprint $table) {
            $table->id('header_music_id');
            $table->unsignedBigInteger('music_id');
            $table->foreign('music_id')->references('music_id')->on('m_musics');
            $table->bigInteger('music_sequence');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('header_music');
    }
}
