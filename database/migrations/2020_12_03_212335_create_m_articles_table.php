<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_articles', function (Blueprint $table) {
            $table->bigIncrements('article_id');
            $table->string('article_url')->unique();
            $table->string('article_title');
            $table->longText('article_text');
            $table->unsignedBigInteger('article_user_id');
            $table->foreign('article_user_id')->references('id')->on('users');
            $table->integer('article_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}
