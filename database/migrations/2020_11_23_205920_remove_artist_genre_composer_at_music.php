<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveArtistGenreComposerAtMusic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_musics', function($table) {
            $table->dropForeign('m_musics_artist_id_foreign');
            $table->dropColumn('artist_id');
            
            $table->dropForeign('m_musics_composer_id_foreign');
            $table->dropColumn('composer_id');

            $table->dropForeign('m_musics_genre_id_foreign');
            $table->dropColumn('genre_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
