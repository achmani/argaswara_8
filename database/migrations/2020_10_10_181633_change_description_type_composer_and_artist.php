<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDescriptionTypeComposerAndArtist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_artists', function(Blueprint $table) {
            $table->text('artist_desc')->change();
        });

        Schema::table('m_composers', function(Blueprint $table) {
            $table->text('composer_desc')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_artists', function(Blueprint $table) {
            $table->string('artist_desc')->change();
        });

        Schema::table('m_composers', function(Blueprint $table) {
            $table->string('composer_desc')->change();
        });
    }
}
