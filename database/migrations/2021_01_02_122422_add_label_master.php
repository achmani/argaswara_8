<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLabelMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('m_labels', function (Blueprint $table) {
            $table->bigIncrements('label_id');
            $table->string('label_value');
            $table->timestamps();
        });

        Schema::create('m_musics_labels', function (Blueprint $table) {
            $table->unsignedBigInteger('music_id');
            $table->unsignedBigInteger('label_id');
            $table->foreign('music_id')->references('music_id')->on('m_musics');
            $table->foreign('label_id')->references('label_id')->on('m_labels');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_labels');
        Schema::dropIfExists('m_musics_labels');
    }
}
