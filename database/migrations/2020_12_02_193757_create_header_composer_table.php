<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeaderComposerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('header_composer', function (Blueprint $table) {
            $table->id('header_composer_id');
            $table->unsignedBigInteger('composer_id');
            $table->foreign('composer_id')->references('composer_id')->on('m_composers');
            $table->bigInteger('composer_sequence');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('header_composer');
    }
}
