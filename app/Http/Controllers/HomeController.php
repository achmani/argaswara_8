<?php

namespace App\Http\Controllers;

use App\Models\Music;
use App\Models\HeaderMusic;
use App\Models\CarouselHeader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;


class HomeController extends Controller
{
    public function index(Request $request)
    {

        $description = "Selamat Datang di Argaswara Kami Menghadirkan Music For Everyone Untuk Kemudahan Pencinta Music. Kami ada untuk melayani. Penggubah, artis, dan mitra penerbitan. Apakah anda seorang produser atau komposer yang mencari penerbit musik terbaik ? Seorang pemilik label mencari mitra penerbitan ? Pemilik katalog atau penerbit asli yang mencari representasi terbaik ? Cari tahu apa yang bisa kami lakukan. Berlangganan & terima update. Jangan ragu untuk menghubungi kami";
        $title = "Home";
        $stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
        $stemmer = $stemmerFactory->createStemmer();

        // stem
        $sentence = str_replace("&nbsp;", ' ', strip_tags($description));
        $keyword = explode(" ",$stemmer->stem($sentence));

        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::addMeta('data:published_time', "2020-12-12", 'property');
        SEOMeta::addMeta('data:section', "music", 'property');
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($request->url());
        OpenGraph::addProperty('type', 'data');
        OpenGraph::addProperty('locale', 'id-id');

        OpenGraph::addImage(asset('image/icon-min.png'));

        JsonLd::setTitle($title);
        JsonLd::setDescription($description);
        JsonLd::setType('Article');
        JsonLd::addImage(asset('image/icon-min.png'));

        $sponsor = CarouselHeader::get();

        $listmusic = HeaderMusic::orderBy("music_sequence","desc")->pluck("music_id")->toArray();
        //$music_json = json_encode(Music::with('composer')->whereIn('music_id',$listmusic)->select("music_name as name", 'composer as composerConcat', "music_url as url")->get(), JSON_UNESCAPED_SLASHES);
        $musics = Music::join("header_music","header_music.music_id","m_musics.music_id")->orderBy("music_sequence","desc")->get();
        $music_json = Music::join("header_music","header_music.music_id","m_musics.music_id")->orderBy("music_sequence","desc")->select("m_musics.music_id","m_musics.music_name as name", "m_musics.music_url as url", "header_music.music_sequence as music_sequence")->whereIn('m_musics.music_id',$listmusic)->orderBy("music_sequence")->get()->map(function ($data) {
            $data['artist'] = implode(', ', $data->composer()->get()->pluck('composer_name')->all());
            return $data;
         })->toArray();
        foreach ($music_json as $key => $value) {
            unset($value['music_id']);
            $temp = array();
            $temp ['name'] = $value['name'];
            $temp ['artist'] = $value['artist'];
            $temp ['album'] = "";
            $temp ['url'] =  $value['url'];
            $temp ['cover_art_url'] = "";
            $music_json[$key] = $temp;
        }
        
        $video = "video/argaswara.m4v";
        $mime = "video/mp4";

        return view('index')
            ->with('sponsor',$sponsor)
            ->with('title',$title)
            ->with('musics',$musics)
            ->with('listmusic',$listmusic)
            ->with('video',$video)
            ->with('mime',$mime)
            ->with('song_index',0)
            ->with('song_item',1)
            ->with('music_json',json_encode($music_json));
    }
}
