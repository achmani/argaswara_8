<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\ArtistDataTable;
use App\Models\Artist;
use App\Models\MusicArtist;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ArtistController extends Controller
{
    public function index(ArtistDataTable $dataTable)
    {
        $rules_created = [
            'artistName' => 'required',
        ];
        return $dataTable->render('artist.index', ["rules_created" => $rules_created]);
    }

    public function create(Request $request)
    {
        $rules = [
            'artistName' => 'required|min:1',
            'artistDesc' => 'required|min:30',
            'artistPopularity' => 'required|integer|min:1',
            'artistImg' => 'image|max:1024', // 1MB Max
        ];
        return view('artist.form')
            ->with("rules", $rules)
            ->with("type", "create");
    }

    public function update(Request $request)
    {
        $rules = [
            'artistName' => 'required|min:1',
            'artistDesc' => 'required|min:30',
            'artistPopularity' => 'required|integer|min:1',
            'artistImg' => 'nullable|max:1024', // 1MB Max
        ];
        $data = Artist::find($request->id);
        return view('artist.form')
            ->with("data", $data)
            ->with("rules", $rules)
            ->with("type", "update");
    }

    public function read(Request $request)
    {
        $rules = [];
        $data = Artist::find($request->id);
        return view('artist.form')
            ->with("data", $data)
            ->with("rules", $rules)
            ->with("type", "read");
    }

    public function delete(Request $request)
    {
        $id = $request->json('id');
        DB::beginTransaction();
        try {
            $data = Artist::where('artist_id', $id)->first();
            if ($data) {
                Storage::disk('public')->deleteDirectory("artist/artist-" . $id);
                Artist::find($id)->delete();
                DB::commit();
                return json_encode(array("ok" => ""));
            } else {
                return json_encode(array("error" => ""));
            }
        } catch (\Throwable $th) {
            Log::debug("Delete Artist Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function select2(Request $request)
    {

        $page = $request->page;
        $resultCount = 10;
        $offset = ($page - 1) * $resultCount;

        $term = trim($request->term);

        if (empty($term)) {
            return response()->json([]);
        }

        $artists = Artist::search($term)->skip($offset)->take($resultCount)->get();

        $formatted_tags = array();

        foreach ($artists as $artist) {
            $formatted_tags[] = ['id' => $artist->artist_id, 'text' => $artist->artist_name];
        }

        $count = Artist::search($term)->count();
        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;

        $results = array(
            "results" => $formatted_tags,
            "count_filtered" => $count,
            "pagination" => array(
                "more" => $morePages
            )
        );

        return response()->json($results);
    }

    public function select2premusic(Request $request)
    {
        $musicid = $request->musicid;
        $listartist = MusicArtist::where('music_id',$musicid)->pluck('artist_id')->toArray();
        $artists = Artist::whereIn('artist_id', $listartist)->get();

        $formatted_tags = array();

        foreach ($artists as $artist) {
            $formatted_tags[] = ['id' => $artist->artist_id, 'text' => $artist->artist_name];
        }

        $results = array(
            "results" => $formatted_tags,
        );

        return response()->json($results);
    }

}
