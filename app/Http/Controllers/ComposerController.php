<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\ComposersDataTable;

use App\Models\Composers;
use App\Models\MusicComposer;
use App\Models\HeaderComposer;
use Illuminate\Support\Composer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ComposerController extends Controller
{
    public function index(ComposersDataTable $dataTable)
    {
        return $dataTable->render('composer.index');
    }

    public function create(Request $request)
    {
        $rules = [
            'composerName' => 'required|min:1',
            'composerDesc' => '',
            'composerPopularity' => 'required|integer|min:1',
            'composerImg' => 'image|max:1024', // 1MB Max
        ];
        return view('composer.form')
            ->with("rules", $rules)
            ->with("type", "create");
    }

    public function update(Request $request)
    {
        $rules = [
            'composerName' => 'required|min:1',
            'composerDesc' => '',
            'composerPopularity' => 'required|integer|min:1',
            'composerImg' => 'nullable|max:1024', // 1MB Max
        ];
        $data = Composers::find($request->id);
        return view('composer.form')
            ->with("data", $data)
            ->with("rules", $rules)
            ->with("type", "update");
    }

    public function read(Request $request)
    {
        $rules = [];
        $data = Composers::find($request->id);
        return view('composer.form')
            ->with("data", $data)
            ->with("rules", $rules)
            ->with("type", "read");
    }

    public function delete(Request $request)
    {
        $id = $request->json('id');
        DB::beginTransaction();
        try {
            $data = Composers::where('composer_id', $id)->first();
            if ($data) {
                Storage::disk('public')->deleteDirectory("composer/composer-" . $id);
                Composers::find($id)->delete();
                DB::commit();
                return json_encode(array("ok" => ""));
            } else {
                return json_encode(array("error" => ""));
            }
        } catch (\Throwable $th) {
            Log::debug("Delete Composer Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function select2(Request $request)
    {

        $page = $request->page;
        $resultCount = 10;
        $offset = ($page - 1) * $resultCount;

        $term = trim($request->term);

        if (empty($term)) {
            return response()->json([]);
        }

        $composers = Composers::search($term)->skip($offset)->take($resultCount)->get();

        $formatted_tags = array();

        foreach ($composers as $composer) {
            $formatted_tags[] = ['id' => $composer->composer_id, 'text' => $composer->composer_name];
        }

        $count = Composers::search($term)->count();
        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;

        $results = array(
            "results" => $formatted_tags,
            "count_filtered" => $count,
            "pagination" => array(
                "more" => $morePages
            )
        );

        return response()->json($results);

    }
    
    public function select2premusic(Request $request){
        $musicid = $request->musicid;
        $listcomposers = MusicComposer::where('music_id',$musicid)->pluck('composer_id')->toArray();
        $composers = Composers::whereIn('composer_id', $listcomposers)->get();

        $formatted_tags = array();

        foreach ($composers as $composer) {
            $formatted_tags[] = ['id' => $composer->composer_id, 'text' => $composer->composer_name];
        }

        $results = array(
            "results" => $formatted_tags,
        );

        return response()->json($results);
    }

    public function select2HeaderComposer(Request $request)
    {

        $page = $request->page;
        $resultCount = 10;
        $offset = ($page - 1) * $resultCount;

        $term = trim($request->term);

        if (empty($term)) {
            return response()->json([]);
        }

        $headercomposer = HeaderComposer::pluck('composer_id')->toArray();
        $composers = Composers::whereNotIn('composer_id',$headercomposer)->search($term)->skip($offset)->take($resultCount)->get();

        $formatted_tags = array();

        foreach ($composers as $composer) {
            $formatted_tags[] = ['id' => $composer->composer_id, 'text' => $composer->composer_name];
        }

        $count = Composers::whereNotIn('composer_id',$headercomposer)->search($term)->count();
        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;

        $results = array(
            "results" => $formatted_tags,
            "count_filtered" => $count,
            "pagination" => array(
                "more" => $morePages
            )
        );

        return response()->json($results);
    }

}
