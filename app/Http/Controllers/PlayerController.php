<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\Music;
use App\Models\Article;
use App\Models\Composers;
use App\Models\MusicComposer;
use App\Models\ArticleComposer;
use Illuminate\Http\Request;

use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;

class PlayerController extends Controller
{
    public function index(Request $request)
    {
        $id = $request->id;
        $play = $request->play  ? $request->play : 0;
        $id = str_replace('-', ' ', $id);
        $composer = Composers::where("composer_label",6)->where('composer_name',$id)->first();
        if (!$composer) {
            abort(404);
        }
        $id = $composer->composer_id;
        $listmusic = MusicComposer::where("composer_id",$id)->pluck("music_id")->toArray();
        $music = Music::whereIn('music_id',$listmusic)->orderBy('music_name','ASC')->get();
        $music_json = Music::whereIn('music_id',$listmusic)->select("m_musics.music_id","m_musics.music_name as name", "m_musics.music_url as url")->orderBy('music_name','ASC')->get()->map(function ($data) {
            $data['artist'] = implode(', ', $data->composer()->get()->pluck('composer_name')->all());
            $data['genre'] = implode(', ', $data->genre()->get()->pluck('genre_value')->all());
            $data['label'] = implode(', ', $data->label()->get()->pluck('label_value')->all());
            return $data;
         })->toArray();
        foreach ($music_json as $key => $value) {
            unset($value['music_id']);
            $temp = array();
            $temp ['name'] = $value['name'];
            $temp ['artist'] = $value['artist'];
            $temp ['genre'] = $value['genre'];
            $temp ['label'] = $value['label'];
            $temp ['album'] = "";
            $temp ['url'] =  $value['url'];
            $temp ['cover_art_url'] = "";
            $music_json[$key] = $temp;
        }
        $listarticle = ArticleComposer::where("composer_id",$id)->pluck("article_id")->toArray();
        $article = Article::whereIn("article_id",$listarticle)->orderBy('updated_at',"DESC")->take(3)->get();
        $composerRelated = Composers::orderBy('composer_count','desc')->skip(0)->take(9)->get(); //get first 16 rows

        $description = $composer->composer_desc;
        $title = $composer->composer_name;
        $stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
        $stemmer = $stemmerFactory->createStemmer();

        // stem
        $sentence = str_replace("&nbsp;", ' ', strip_tags($description));
        $keyword = explode(" ",$stemmer->stem($sentence));

        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::addMeta('data:published_time', $composer->created_at->toW3CString(), 'property');
        SEOMeta::addMeta('data:section', "music", 'property');
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($request->url());
        OpenGraph::addProperty('type', 'data');
        OpenGraph::addProperty('locale', 'id-id');

        OpenGraph::addImage(asset('app/composer/composer-' . $composer->composer_id . '/' . $composer->composer_img . '_300.jpg'));

        JsonLd::setTitle($title);
        JsonLd::setDescription($description);
        JsonLd::setType('Article');
        JsonLd::addImage(asset('app/composer/composer-' . $composer->composer_id . '/' . $composer->composer_img . '_300.jpg'));

        return view('player')
            ->with('title',$title)
            ->with('play',$play)
            ->with('music',$music)
            ->with('article',$article)
            ->with('music_json',json_encode($music_json))
            ->with('song_index',0)
            ->with('composerRelated',$composerRelated)
            ->with('composer',$composer);
    }
}
