<?php

namespace App\Http\Controllers;


use App\DataTables\ArticleDataTable;
use App\Models\Article;
use App\Models\Composers;
use App\Models\ArticleComposer;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Sastrawi\Stemmer\StemmerFactory;

use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
// OR with multi
use Artesaos\SEOTools\Facades\JsonLdMulti;

// OR
use Artesaos\SEOTools\Facades\SEOTools;

class ArticleController extends Controller
{

    public function index(ArticleDataTable $dataTable)
    {
        return $dataTable->render('article.index');
    }

    public function create(Request $request)
    {
        $rules = [
            'articleUrl' => 'required|string|unique:m_articles,article_url',
            'articleTitle' => 'required|string',
            'articleText' => 'required|string',
            'composerId' => 'required',
        ];
        return view('article.form')
            ->with("rules", $rules)
            ->with("type", "create");
    }

    public function update(Request $request)
    {
        $rules = [
            'articleUrl' => 'required|string',
            'articleTitle' => 'required|string',
            'articleText' => 'required|string',
            'composerId' => 'required',
        ];
        $data = Article::find($request->id);
        return view('article.form')
            ->with("data", $data)
            ->with("rules", $rules)
            ->with("type", "update");
    }

    public function read(Request $request)
    {
        $rules = [];
        $data = Article::find($request->id);
        return view('article.form')
            ->with("data", $data)
            ->with("rules", $rules)
            ->with("type", "read");
    }

    public function select2precomposer(Request $request)
    {

        $articleId = $request->articleId;
        $listcomposers = ArticleComposer::where('article_id', $articleId)->pluck('composer_id')->toArray();
        $composers = Composers::whereIn('composer_id', $listcomposers)->get();

        $formatted_tags = array();

        foreach ($composers as $composer) {
            $formatted_tags[] = ['id' => $composer->composer_id, 'text' => $composer->composer_name];
        }

        $results = array(
            "results" => $formatted_tags,
        );

        return response()->json($results);
    }

    public function show(Request $request)
    {

        $id = str_replace('-', ' ', $request->id);
        $article = Article::with("composer")->where("article_url", $id)->first();

        if ($article) {

            $stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
            $stemmer = $stemmerFactory->createStemmer();

            // stem
            $sentence = str_replace("&nbsp;", ' ', strip_tags($article->article_text));
            $keyword = explode(" ",$stemmer->stem($sentence));

            $description = Str::limit(str_replace("&nbsp;", ' ', strip_tags($article->article_text)), 200);

            SEOMeta::setTitle($article->article_title);
            SEOMeta::setDescription($article->resume);
            SEOMeta::addMeta('article:published_time', $article->created_at->toW3CString(), 'property');
            SEOMeta::addMeta('article:section', "music", 'property');
            SEOMeta::addKeyword($keyword);

            OpenGraph::setDescription($description);
            OpenGraph::setTitle($article->article_title);
            OpenGraph::setUrl($request->url());
            OpenGraph::addProperty('type', 'article');
            OpenGraph::addProperty('locale', 'id-id');

            OpenGraph::addImage(asset('app/articleThumbnail/article-' . $article->article_id . '/' . $article->article_img . '_300.jpg'));

            JsonLd::setTitle($article->article_title);
            JsonLd::setDescription($description);
            JsonLd::setType('Article');
            JsonLd::addImage(asset('app/articleThumbnail/article-' . $article->article_id . '/' . $article->article_img . '_300.jpg'));

            $composers = Composers::get();
            $newest_article = Article::where("article_url", "!=", $id)->orderBy("created_at", "desc")->get();

            $listcomposer = $article->composer->pluck('composer_id')->toArray();
            $listarticle = ArticleComposer::where('composer_id', $listcomposer)->pluck('article_id')->toArray();

            $related_article_after = Article::whereIn("article_id", $listarticle)->where("article_url", "!=", $id)->where("created_at", ">", $article->created_at)->orderBy("created_at", "asc")->take(2)->get();
            $related_article_before = Article::whereIn("article_id", $listarticle)->where("article_url", "!=", $id)->where("created_at", "<", $article->created_at)->orderBy("created_at", "asc")->take(2)->get();

            return view('article')
                ->with('title', $article->article_title)
                ->with('newest_article', $newest_article)
                ->with('composers', $composers)
                ->with('related_article_after', $related_article_after)
                ->with('related_article_before', $related_article_before)
                ->with('article', $article);
        } else {
            abort(404);
        }
        
    }

    public function uploadImage(Request $request)
    {
        if ($request->hasFile('upload')) {
            $img_title = Str::random(100);
            $img = Image::make($request->upload);
            $img->resize(null, 600, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg', 50);
            $img->stream(); // <-- Key point
            Storage::disk('public')->put("article/" . $img_title . '.jpg', $img, 'public');
        }

        $response = [
            'uploaded' => true,
            "url" => url("") . "/app/article/" . $img_title . ".jpg"
        ];

        return response()->json($response);
    }

    public function deleteImage(Request $request)
    {
        $url = explode('/', $request->url);
        $file = end($url);
        Storage::disk('public')->delete("article/" . $file);
        $response = [
            'deleted' => true,
            "url" => url("") . "/app/article/" . $file . ".jpg"
        ];
        return response()->json($response);
    }
}
