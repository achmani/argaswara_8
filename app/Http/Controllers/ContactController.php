<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\DataTables\ContactDataTable;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index(ContactDataTable $dataTable)
    {
        return $dataTable->render('contact.index');
    }

    public function create(Request $request){
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'phone' => 'nullable|string|max:255',
            'subject' => 'required|string|max:255',
            'message' => 'required|string',
        ]);
        
        Contact::create([
            'contact_name' => $request->name,
            'contact_email' => $request->email,
            'contact_handphone' => (isset($request->phone) ? $request->phone : "-"),
            'contact_subject' => $request->subject,
            'contact_text' => $request->message,
            'contact_read' => 0,
        ]);

        return response()->json(['message' => 'Terima kasih sudah mengirimkan pesan kepada kami, kami akan membalas pesan anda secepatnya'], 200);

    }
}
