<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\GenreDataTable;

use App\Models\Genre;
use App\Models\MusicGenre;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Storage;

class GenreController extends Controller
{
    public function index(GenreDataTable $dataTable)
    {
        $rules_created = [
            'genreValue' => 'required',
        ];
        return $dataTable->render('genre.index', ["rules_created" => $rules_created]);
        // ->with("rules_created", $rules_created);
    }


    public function delete(Request $request)
    {
        $id = $request->json('id');
        DB::beginTransaction();
        try {
            $data = Genre::where('genre_id', $id)->first();
            if ($data) {
                Genre::find($id)->delete();
                DB::commit();
                return json_encode(array("ok" => ""));
            } else {
                return json_encode(array("error" => ""));
            }
        } catch (\Throwable $th) {
            Log::debug("Delete Genre Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function select2(Request $request)
    {

        $page = $request->page;
        $resultCount = 10;
        $offset = ($page - 1) * $resultCount;

        $term = trim($request->term);

        if (empty($term)) {
            return response()->json([]);
        }

        $genres = Genre::search($term)->skip($offset)->take($resultCount)->get();

        $formatted_tags = array();

        foreach ($genres as $genre) {
            $formatted_tags[] = ['id' => $genre->genre_id, 'text' => $genre->genre_value];
        }

        $count = Genre::search($term)->count();
        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;

        $results = array(
            "results" => $formatted_tags,
            "count_filtered" => $count,
            "pagination" => array(
                "more" => $morePages
            )
        );

        return response()->json($results);
    }

    public function select2premusic(Request $request){
        $musicid = $request->musicid;
        $listgenre = MusicGenre::where('music_id',$musicid)->pluck('genre_id')->toArray();
        $genres = Genre::whereIn('genre_id', $listgenre)->get();

        $formatted_tags = array();

        foreach ($genres as $genre) {
            $formatted_tags[] = ['id' => $genre->genre_id, 'text' => $genre->genre_value];
        }

        $results = array(
            "results" => $formatted_tags,
        );

        return response()->json($results);
    }
}
