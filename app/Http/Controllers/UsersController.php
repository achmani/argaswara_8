<?php

namespace App\Http\Controllers;

use App\Models\User;

use Illuminate\Http\Request;
use App\DataTables\UsersDataTable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    public function index(UsersDataTable $dataTable)
    {
        return $dataTable->render('users.index');
    }

    public function create(Request $request)
    {
        $rules = [
            'name' => 'required|min:1',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:8|same:password_confirmation',
            'password_confirmation' => 'required|min:8|same:password',
            'photo' => 'image|max:1024', // 1MB Max
        ];
        return view('users.form')
            ->with("rules", $rules)
            ->with("type", "create");
    }

    public function update(Request $request)
    {
        $rules = [
            'name' => 'required|min:1',
            'email' => 'required',
            'photo' => 'nullable|max:1024', // 1MB Max
        ];
        $user = User::find($request->id);
        return view('users.form')
            ->with("user", $user)
            ->with("rules", $rules)
            ->with("type", "update");
    }

    public function read(Request $request)
    {
        $rules = [];
        $user = User::find($request->id);
        return view('users.form')
            ->with("user", $user)
            ->with("rules", $rules)
            ->with("type", "read");
    }

    public function delete(Request $request)
    {
        $id = $request->json('id');
        DB::beginTransaction();
        try {
            $user = User::where('id', $id)->first();
            if ($user) {
                Storage::disk('public')->deleteDirectory("photo_profile/" . $user->email);
                User::find($id)->delete();
                DB::commit();
                return json_encode(array("ok" => ""));
            } else {
                return json_encode(array("error" => ""));
            }
        } catch (\Throwable $th) {
            Log::debug("Delete User Error");
            Log::debug($th);
            DB::rollBack();
        }
    }
}
