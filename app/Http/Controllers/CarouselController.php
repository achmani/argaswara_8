<?php

namespace App\Http\Controllers;

use App\Models\CarouselHeader;

use Illuminate\Http\Request;
use App\DataTables\CarouselHeaderDataTable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class CarouselController extends Controller
{
    public function index(CarouselHeaderDataTable $dataTable)
    {
        return $dataTable->render('carousel.index');
    }

    public function create(Request $request)
    {
        $rules = [
            'headerTitle' => 'min:1',
            'headerLink' => 'min:1',
            'img' => 'image|max:1024', // 1MB Max
        ];
        return view('carousel.form')
            ->with("rules", $rules)
            ->with("type", "create");
    }

    public function update(Request $request)
    {
        $rules = [
            'headerTitle' => 'min:1',
            'headerLink' => 'min:1',
            'img' => 'nullable|max:1024', // 1MB Max
        ];
        $data = CarouselHeader::find($request->id);
        return view('carousel.form')
            ->with("data", $data)
            ->with("rules", $rules)
            ->with("type", "update");
    }

    public function read(Request $request)
    {
        $rules = [];
        $data = CarouselHeader::find($request->id);
        return view('carousel.form')
            ->with("data", $data)
            ->with("rules", $rules)
            ->with("type", "read");
    }

    public function delete(Request $request)
    {
        $id = $request->json('id');
        DB::beginTransaction();
        try {
            $data = CarouselHeader::where('header_id', $id)->first();
            if ($data) {
                Storage::disk('public')->delete("carousel_header/" . $data->header_path.".jpg");
                Storage::disk('public')->delete("carousel_header/" . $data->header_path."_300.jpg");
                CarouselHeader::find($id)->delete();
                DB::commit();
                return json_encode(array("ok" => ""));
            } else {
                return json_encode(array("error" => ""));
            }
        } catch (\Throwable $th) {
            Log::debug("Delete Carousel Header Error");
            Log::debug($th);
            DB::rollBack();
        }
    }
}
