<?php

namespace App\Http\Controllers;

use App\Models\Pages;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;

class PagesController extends Controller
{

    public function update(Request $request)
    {
        $rules = [
            'pageUrl' => 'required|string',
            'pageTitle' => 'required|string',
            'pageText' => 'required|string'
        ];
        $data = Pages::where('page_url',$request->id)->first();
        return view('page.form')
            ->with("data", $data)
            ->with("rules", $rules)
            ->with("type", "update");
    }

    public function uploadImage(Request $request)
    {
        if ($request->hasFile('upload')) {
            $img_title = Str::random(100);
            $img = Image::make($request->upload);
            $img->resize(null, 600, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg', 50);
            $img->stream(); // <-- Key point
            Storage::disk('public')->put("page/" . $img_title . '.jpg', $img, 'public');
        }

        $response = [
            'uploaded' => true,
            "url" => url("") . "/app/page/" . $img_title . ".jpg"
        ];

        return response()->json($response);
    }

    public function deleteImage(Request $request)
    {
        $url = explode('/', $request->url);
        $file = end($url);
        Storage::disk('public')->delete("page/" . $file);
        $response = [
            'deleted' => true,
            "url" => url("") . "/app/page/" . $file . ".jpg"
        ];
        return response()->json($response);
    }

    public function show(Request $request)
    {
        $data = Pages::where('page_url',$request->id)->first();
        $title = ucwords(str_replace('-', ' ', $request->id));
        
        if ($data) {
            $img = $data->page_img ? asset('app/pageThumbnail/page-' . $data->page_id . '/' . $data->page_img . '.jpg') : '/image/jumbotron-composer.jpg';
            $stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
            $stemmer = $stemmerFactory->createStemmer();

            // stem
            $sentence = str_replace("&nbsp;", ' ', strip_tags($data->data_text));
            $keyword = explode(" ",$stemmer->stem($sentence));

            $description = Str::limit(str_replace("&nbsp;", ' ', strip_tags($data->data_text)), 200);

            SEOMeta::setTitle($data->page_title);
            SEOMeta::setDescription($description);
            SEOMeta::addMeta('data:published_time', $data->created_at->toW3CString(), 'property');
            SEOMeta::addMeta('data:section', "music", 'property');
            SEOMeta::addKeyword($keyword);

            OpenGraph::setDescription($description);
            OpenGraph::setTitle($data->data_title);
            OpenGraph::setUrl($request->url());
            OpenGraph::addProperty('type', 'data');
            OpenGraph::addProperty('locale', 'id-id');

            OpenGraph::addImage(asset('app/dataThumbnail/data-' . $data->data_id . '/' . $data->data_img . '_300.jpg'));

            JsonLd::setTitle($data->page_title);
            JsonLd::setDescription($description);
            JsonLd::setType('Article');
            JsonLd::addImage(asset('app/dataThumbnail/data-' . $data->data_id . '/' . $data->data_img . '_300.jpg'));

            return view('page')
                ->with('title', $data->page_title)
                ->with('data', $data)
                ->with('img', $img);
        } else {
            abort(404);
        }
    }
}
