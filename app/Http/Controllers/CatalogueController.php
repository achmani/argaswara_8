<?php

namespace App\Http\Controllers;

use App\Models\Composers;
use App\Models\HeaderComposer;
use Illuminate\Http\Request;

use Illuminate\Http\JsonResponse;
use App\DataTables\FrontendMusicDataTable;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;

class CatalogueController extends Controller
{
    public function index(Request $request)
    {
        $length = 12;
        $page = $request->page;

        $count_composer = Composers::count(); //get first 8 rows

        if($page > ceil($count_composer/$length) || $page < 1){
            abort(404);
        }

        $description = "Catalogue music from Argaswara";
        $title = "Catalogue";
        $stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
        $stemmer = $stemmerFactory->createStemmer();

        // stem
        $sentence = str_replace("&nbsp;", ' ', strip_tags($description));
        $keyword = explode(" ",$stemmer->stem($sentence));

        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::addMeta('data:published_time', "2020-12-12", 'property');
        SEOMeta::addMeta('data:section', "music", 'property');
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($request->url());
        OpenGraph::addProperty('type', 'data');
        OpenGraph::addProperty('locale', 'id-id');

        OpenGraph::addImage(asset('image/icon-min.jpg'));

        JsonLd::setTitle($title);
        JsonLd::setDescription($description);
        JsonLd::setType('Article');
        JsonLd::addImage(asset('image/icon-min.jpg'));

        $composerheader = Composers::join("header_composer","header_composer.composer_id","m_composers.composer_id")->orderBy("composer_sequence","desc")->get();
        $composer = Composers::orderBy('composer_count','desc')->skip(($page-1)*$length)->take($length)->get(); //get first 8 rows
    
        return view('catalogue')
            ->with('title',$title)
            ->with('length',$length)
            ->with('page',$page)
            ->with('length_page',$length)
            ->with('composerheader',$composerheader)
            ->with('count_composer',$count_composer)
            ->with('composer',$composer);
    }

    public function indexTable(Request $request)
    {
        $description = "Catalogue music from Argaswara";
        $title = "Catalogue";
        $stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
        $stemmer = $stemmerFactory->createStemmer();

        // stem
        $sentence = str_replace("&nbsp;", ' ', strip_tags($description));
        $keyword = explode(" ",$stemmer->stem($sentence));

        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::addMeta('data:published_time', "2020-12-12", 'property');
        SEOMeta::addMeta('data:section', "music", 'property');
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($request->url());
        OpenGraph::addProperty('type', 'data');
        OpenGraph::addProperty('locale', 'id-id');

        OpenGraph::addImage(asset('image/icon-min.jpg'));

        JsonLd::setTitle($title);
        JsonLd::setDescription($description);
        JsonLd::setType('Article');
        JsonLd::addImage(asset('image/icon-min.jpg'));

        $composerheader = Composers::join("header_composer","header_composer.composer_id","m_composers.composer_id")->orderBy("composer_sequence","desc")->get();

        $dataTable = new FrontendMusicDataTable();
    
        return $dataTable->render('catalogue-table', ["title" => $title, "composerheader" => $composerheader]);
    }

    public function composer(Request $request)
    {
        $length = 12;
        $page = $request->page;
        $shorts = ($request->shorts) ? $request->shorts : 1;

        $count_composer = Composers::count(); //get first 8 rows

        if($page > ceil($count_composer/$length) || $page < 1){
            abort(404);
        }

        $description = "Composer from Argaswara";
        $title = "Composer";
        $stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
        $stemmer = $stemmerFactory->createStemmer();

        // stem
        $sentence = str_replace("&nbsp;", ' ', strip_tags($description));
        $keyword = explode(" ",$stemmer->stem($sentence));

        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::addMeta('data:published_time', "2020-12-12", 'property');
        SEOMeta::addMeta('data:section', "music", 'property');
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($request->url());
        OpenGraph::addProperty('type', 'data');
        OpenGraph::addProperty('locale', 'id-id');

        OpenGraph::addImage(asset('image/icon-min.jpg'));

        JsonLd::setTitle($title);
        JsonLd::setDescription($description);
        JsonLd::setType('Article');
        JsonLd::addImage(asset('image/icon-min.jpg'));
        
        $composerheader = Composers::join("header_composer","header_composer.composer_id","m_composers.composer_id")->orderBy("composer_name","asc")->get();

        if($shorts == 1){
            $composer = Composers::where('composer_label',6)->orderBy('composer_name','asc')->skip(($page-1)*$length)->take($length)->get(); //get first 8 rows
        }else if($shorts == 2){
            $composer = Composers::where('composer_label',6)->orderBy('composer_popularity','desc')->skip(($page-1)*$length)->take($length)->get(); //get first 8 rows
        }else{
            $composer = Composers::where('composer_label',6)->orderBy('composer_name','asc')->skip(($page-1)*$length)->take($length)->get(); //get first 8 rows
        }
        return view('composer')
            ->with('title',$title)
            ->with('length',$length)
            ->with('page',$page)
            ->with('shorts',$shorts)
            ->with('length_page',$length)
            ->with('composerheader',$composerheader)
            ->with('count_composer',$count_composer)
            ->with('composer',$composer);
    }

}
