<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use App\Models\Artist;
use App\Models\HeaderMusic;
use App\Models\MusicComposer;
use App\Models\MusicArtist;
use App\Models\MusicGenre;
use App\Models\Composers;

use Illuminate\Http\Request;
use App\DataTables\MusicDataTable;
use App\Models\Music;
use App\Models\MusicLabel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class MusicController extends Controller
{
    public function index(MusicDataTable $dataTable)
    {
        return $dataTable->render('music.index');
    }

    public function create(Request $request)
    {
        // $rules = [
        //     'musicName' => 'required|min:6',
        //     'musicRelease' => 'required|date',
        //     'artistId' => function ($attribute, $value, $fail) {
        //         $data = explode(",", $value);
        //         foreach ($data as $key => $value) {
        //             try {
        //                 Artist::findOrFail($value);
        //             } catch (\Exception $e) {
        //                 $fail($attribute . ' is not exist');
        //             }
        //         }
        //     },
        //     'composerId' => function ($attribute, $value, $fail) {
        //         $data = explode(",", $value);
        //         foreach ($data as $key => $value) {
        //             try {
        //                 Composers::findOrFail($value);
        //             } catch (\Exception $e) {
        //                 $fail($attribute . ' is not exist');
        //             }
        //         }
        //     },
        //     'genreId' => function ($attribute, $value, $fail) {
        //         $data = explode(",", $value);
        //         foreach ($data as $key => $value) {
        //             try {
        //                 Genre::findOrFail($value);
        //             } catch (\Exception $e) {
        //                 $fail($attribute . ' is not exist');
        //             }
        //         }
        //     }
        // ];

        $rules = [
            'musicName' => 'required|min:1',
            'musicRelease' => 'required|date',
            // 'music' => 'required|mimes:mpga,wav,mp3',
            'composerId' => 'required',
            'genreId' => 'required',
            'labelId' => 'required',
            // 'musicJoox' => 'url',
            // 'musicItunes' => 'url',
            // 'musicSpotify' => 'url',
            // 'musicYoutube' => 'url',
        ];

        return view('music.form')
            ->with("rules", $rules)
            ->with("type", "create");
    }

    public function update(Request $request)
    {
        $rules = [
            'musicName' => 'required|min:1',
            'musicRelease' => 'required|date',
            'music' => 'nullable|mimes:mpga,wav,mp3',
            'composerId' => 'required',
            'genreId' => 'required',
            'musicJoox' => 'nullable|url',
            'musicItunes' => 'nullable|url',
            'musicSpotify' => 'nullable|url',
            'musicYoutube' => 'nullable|url',
        ];
        $data = Music::find($request->id);
        return view('music.form')
            ->with("data", $data)
            ->with("rules", $rules)
            ->with("type", "update");
    }

    public function read(Request $request)
    {
        $rules = [];
        $data = Music::find($request->id);
        return view('music.form')
            ->with("data", $data)
            ->with("rules", $rules)
            ->with("type", "read");
    }

    public function delete(Request $request)
    {
        $id = $request->json('id');
        DB::beginTransaction();
        try {
            MusicGenre::where('music_id', $id)->delete();
            MusicArtist::where('music_id', $id)->delete();
            MusicComposer::where('music_id', $id)->delete();
            MusicLabel::where('music_id', $id)->delete();
            $data = Music::findOrFail($id);
            if ($data) {
                Storage::disk('public')->delete("musics/" . $data->music_url . ".mp3");
                Music::find($id)->delete();
                DB::commit();
                return json_encode(array("ok" => ""));
            } else {
                return json_encode(array("error" => ""));
            }
        } catch (\Throwable $th) {
            Log::debug("Delete Artist Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function play(Request $request)
    {
        //$path = storage_path().DIRECTORY_SEPARATOR."songs".DIRECTORY_SEPARATOR.$song->path.".mp3");
        $music = $request->music;
        $path = Storage::path('public/music/' . $music . '.mp3');
        $response = new BinaryFileResponse($path);
        BinaryFileResponse::trustXSendfileTypeHeader();
        return $response;
    }

    public function plays(Request $request)
    {
        //$path = storage_path().DIRECTORY_SEPARATOR."songs".DIRECTORY_SEPARATOR.$song->path.".mp3");
        $music = $request->music;
        $path = Storage::path('public/musics/' . $music . '.mp3');
        $response = new BinaryFileResponse($path);
        BinaryFileResponse::trustXSendfileTypeHeader();
        return $response;
    }

    public function select2HeaderMusic(Request $request)
    {

        $page = $request->page;
        $resultCount = 10;
        $offset = ($page - 1) * $resultCount;

        $term = trim($request->term);

        if (empty($term)) {
            return response()->json([]);
        }

        $headermusic = HeaderMusic::pluck('music_id')->toArray();
        $musics = Music::whereNotIn('music_id',$headermusic)->search($term)->skip($offset)->take($resultCount)->get();

        $formatted_tags = array();

        foreach ($musics as $music) {
            $formatted_tags[] = ['id' => $music->music_id, 'text' => $music->music_name];
        }

        $count = Music::whereNotIn('music_id',$headermusic)->search($term)->count();
        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;

        $results = array(
            "results" => $formatted_tags,
            "count_filtered" => $count,
            "pagination" => array(
                "more" => $morePages
            )
        );

        return response()->json($results);
    }
}
