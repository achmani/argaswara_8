<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\HeaderComposerDataTable;

use App\Models\HeaderComposer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Storage;

class HeaderComposerController extends Controller
{
    public function index(HeaderComposerDataTable $dataTable)
    {
        $rules_created = [
            'composerSequence' => 'required',
        ];

        $rules_updated = [
            'composerSequence' => 'required',
        ];
        return $dataTable->render('headercomposer.index', ["rules_created" => $rules_created, "rules_updated" => $rules_updated]);
        // ->with("rules_created", $rules_created);
    }


    public function delete(Request $request)
    {
        $id = $request->json('id');
        DB::beginTransaction();
        try {
            $data = HeaderComposer::where('header_composer_id', $id)->first();
            if ($data) {
                HeaderComposer::find($id)->delete();
                DB::commit();
                return json_encode(array("ok" => ""));
            } else {
                return json_encode(array("error" => ""));
            }
        } catch (\Throwable $th) {
            Log::debug("Delete Header Composer Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

}
