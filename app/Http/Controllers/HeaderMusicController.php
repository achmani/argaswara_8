<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\HeaderMusicDataTable;

use App\Models\HeaderMusic;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Storage;

class HeaderMusicController extends Controller
{
    public function index(HeaderMusicDataTable $dataTable)
    {
        $rules_created = [
            'musicSequence' => 'required',
        ];

        $rules_updated = [
            'musicSequence' => 'required',
        ];
        return $dataTable->render('headermusic.index', ["rules_created" => $rules_created, "rules_updated" => $rules_updated]);
        // ->with("rules_created", $rules_created);
    }


    public function delete(Request $request)
    {
        $id = $request->json('id');
        DB::beginTransaction();
        try {
            $data = HeaderMusic::where('header_music_id', $id)->first();
            if ($data) {
                HeaderMusic::find($id)->delete();
                DB::commit();
                return json_encode(array("ok" => ""));
            } else {
                return json_encode(array("error" => ""));
            }
        } catch (\Throwable $th) {
            Log::debug("Delete Header Music Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

}
