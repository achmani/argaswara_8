<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\LabelDataTable;

use App\Models\Label;
use App\Models\MusicLabel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Storage;

class LabelController extends Controller
{
    public function index(LabelDataTable $dataTable)
    {
        $rules_created = [
            'labelValue' => 'required',
        ];
        return $dataTable->render('label.index', ["rules_created" => $rules_created]);
    }


    public function delete(Request $request)
    {
        $id = $request->json('id');
        DB::beginTransaction();
        try {
            $data = Label::where('label_id', $id)->first();
            if ($data) {
                Label::find($id)->delete();
                DB::commit();
                return json_encode(array("ok" => ""));
            } else {
                return json_encode(array("error" => ""));
            }
        } catch (\Throwable $th) {
            Log::debug("Delete Label Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function select2(Request $request)
    {

        $page = $request->page;
        $resultCount = 10;
        $offset = ($page - 1) * $resultCount;

        $term = trim($request->term);

        if (empty($term)) {
            return response()->json([]);
        }

        $labels = Label::search($term)->skip($offset)->take($resultCount)->get();

        $formatted_tags = array();

        foreach ($labels as $label) {
            $formatted_tags[] = ['id' => $label->label_id, 'text' => $label->label_value];
        }

        $count = Label::search($term)->count();
        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;

        $results = array(
            "results" => $formatted_tags,
            "count_filtered" => $count,
            "pagination" => array(
                "more" => $morePages
            )
        );

        return response()->json($results);
    }

    public function select2premusic(Request $request){
        $musicid = $request->musicid;
        $listlabel = MusicLabel::where('music_id',$musicid)->pluck('label_id')->toArray();
        $labels = Label::whereIn('label_id', $listlabel)->get();

        $formatted_tags = array();

        foreach ($labels as $label) {
            $formatted_tags[] = ['id' => $label->label_id, 'text' => $label->label_value];
        }

        $results = array(
            "results" => $formatted_tags,
        );

        return response()->json($results);
    }
    
    public function select2precomposer(Request $request){
        $labelid = $request->labelid;
        $labels = Label::where('label_id', $labelid)->get();

        $formatted_tags = array();

        foreach ($labels as $label) {
            $formatted_tags[] = ['id' => $label->label_id, 'text' => $label->label_value];
        }

        $results = array(
            "results" => $formatted_tags,
        );

        return response()->json($results);
    }
    
}
