<?php

namespace App\Http\Controllers;

use App\Models\Composers;
use App\Models\Music;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $composer = Composers::where("composer_label",6)->search($search)->get();
        $music = Music::search($search)->get();
        $img = '/image/jumbotron-composer.jpg';
        return view('search')
        ->with('search',$search)
        ->with('title','Search - '.$search)
        ->with('composer',$composer)
        ->with('img',$img)
        ->with('music',$music);
    }
}
