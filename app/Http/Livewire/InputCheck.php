<?php

namespace App\Http\Livewire;

use Livewire\Component;

class InputCheck extends Component
{
    public $titleInput;
    public $searchEmail;
    public $routeCheck;
    public $statusClass;

    public function render()
    {
        return view('livewire.input-check');
    }
}
