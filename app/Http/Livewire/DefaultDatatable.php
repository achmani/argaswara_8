<?php

namespace App\Http\Livewire;

use Livewire\Component;

class DefaultDatatable extends Component
{
    public function render()
    {
        return view('livewire.default-datatable');
    }
}
