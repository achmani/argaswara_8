<?php

namespace App\Http\Livewire;

use App\Models\User;

use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ExampleForm extends Component
{
    use WithFileUploads;

    public $name;
    public $email;
    public $photo;
    public $password;
    public $password_confirmation;

    protected $rules = [
        'name' => 'required|min:6',
        'email' => 'required|unique:users|email',
        'password' => 'required|min:8|same:password_confirmation',
        'password_confirmation' => 'required|min:8|same:password',
        'password_confirmation' => 'required|min:8|same:password',
        'photo' => 'image|max:1024|dimensions:ratio=4/3', // 1MB Max
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
        if ($propertyName == "password") {
            $this->validateOnly("password_confirmation");
        }
        if ($propertyName == "password_confirmation") {
            $this->validateOnly("password");
        }
    }

    public function updatedPhoto()
    {
        $this->validate([
            'photo' => 'image|max:1024|dimensions:ratio=4/3', // 1MB Max
        ]);
    }

    public function submit()
    {
        $this->validate();

        $photo_title = Str::random(40);

        // Execution doesn't reach here if validation fails.

        DB::beginTransaction();
        try {
            
            User::create([
                'name' => $this->name,
                'email' => $this->email,
                'password' => bcrypt($this->password),
                'profile_photo_path' => $photo_title
            ]);

            // Store Original Image width 300px
            //$this->photo->storePubliclyAs("photo_profile/".$this->email, $photo_title.'.png');
            $img_default = Image::make($this->photo);
            $img_default->resize(null, 300, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img_default->stream(); // <-- Key point
            Storage::disk('public')->put("photo_profile/" . $this->email . '/' . $photo_title . '.png', $img_default, 'public');
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //Store Image with width 50px
            $img = Image::make($this->photo);
            $img->resize(null, 50, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->stream(); // <-- Key point
            Storage::disk('public')->put("photo_profile/" . $this->email . '/' . $photo_title . '_50.png', $img, 'public');
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            $this->reset();
            session()->flash('message', 'Post successfully updated.');
            // $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Success Insert User']);
            
            DB::commit();

        } catch (\Throwable $th) {
            Log::debug("Add User Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function render()
    {
        return view('livewire.example-form');
    }
}
