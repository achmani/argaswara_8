<?php

namespace App\Http\Livewire\Masterdata;

use App\Models\CarouselHeader;

use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class CarouselHeaderForm extends Component
{
    use WithFileUploads;
    
    public $type;
    public $rules;
    
    public $headerId;
    public $headerTitle;
    public $headerLink;
    public $img;
    public $imgTemp;

    // public function updated($propertyName)
    // {
    //     $this->validateOnly($propertyName);
    // }

    // public function updatedImg()
    // {
    //     $this->validateOnly("img");
    // }

    public function submit()
    {
        $this->validate();

        $img_title = Str::random(100);

        // Execution doesn't reach here if validation fails.

        DB::beginTransaction();
        try {

            if ($this->type == "create") {
                CarouselHeader::create([
                    'header_link' => $this->headerLink,
                    'header_path' => $img_title,
                    'header_title' => $this->headerTitle
                ]);
            } else {
                if (!$this->img) {
                    $img_title = $this->imgTemp;
                }
                $data = CarouselHeader::find($this->headerId);
                $backup_img_temp = $data->header_path;
                $data->update([
                    'header_link' => $this->headerLink,
                    'header_path' => $img_title,
                    'header_title' => $this->headerTitle
                ]);
            }

            if ($this->img && $this->type == "update") {
                Storage::disk('public')->delete("carousel_header/" . $backup_img_temp.".jpg");
                Storage::disk('public')->delete("carousel_header/" . $backup_img_temp."_300.jpg");
            }

            if ($this->img) {
                // Store Original Image width 1200px
                //$this->photo->storePubliclyAs("carousel_header/".$this->email, $photo_title.'.jpg');
                $img_default = Image::make($this->img);
                $img_default->resize(null, 1200, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg', 50);
                $img_default->stream(); // <-- Key point
                Storage::disk('public')->put("carousel_header/"  . $img_title . '.jpg', $img_default, 'public');
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                //Store Image with width 300px
                $img_resize = Image::make($this->img);
                $img_resize->resize(null, 300, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg', 50);
                $img_resize->stream(); // <-- Key point
                Storage::disk('public')->put("carousel_header/" . $img_title . '_300.jpg', $img_resize, 'public');
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Success Insert User']);
            }
            if($this->type == "create"){
                // $this->reset();
                $this->headerLink = "";
                $this->headerTitle = "";
                $this->img = null;
            }
            // session()->flash('message', 'Data carousel successfully updated');
            $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Data Updated Successfully']);
            DB::commit();
        } catch (\Throwable $th) {
            Log::debug("Add Carousel Header Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function render()
    {
        return view('livewire.masterdata.carousel-header-form');
    }
}
