<?php

namespace App\Http\Livewire\Masterdata;

use App\Models\Genre;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GenreForm extends Component
{

    public $type;
    public $rules;

    public $genreId;
    public $genreValue;

    public function submit()
    {
        $this->validate();

        DB::beginTransaction();
        try {

            if ($this->type == "create") {
                Genre::create([
                    'genre_value' => $this->genreValue
                ]);
            } else {
                $data = Genre::find($this->genreId);
                $data->update([
                    'genre_value' => $this->genreValue
                ]);
            }

            if ($this->type == "create") {
                $this->genreValue = "";
            }
            // session()->flash('message', 'Data carousel successfully updated');
            $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Data Updated Successfully']);
            DB::commit();
        } catch (\Throwable $th) {
            Log::debug("Add/Updated Genre Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function render()
    {
        return view('livewire.masterdata.genre-form');
    }
}
