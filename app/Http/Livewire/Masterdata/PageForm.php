<?php

namespace App\Http\Livewire\Masterdata;

use App\Models\Pages;
use App\Models\PagesComposer;

use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class PageForm extends Component
{
    use WithFileUploads;

    public $type;
    public $rules;

    public $pageId;
    public $pageImg;
    public $pageImgTemp;
    public $pageUrl;
    public $pageTitle;
    public $pageText;
    public $composerId;

    public function submit()
    {
        $this->validate();

        $img_title = Str::random(100);

        // Execution doesn't reach here if validation fails.

        DB::beginTransaction();
        try {

            if ($this->type == "create") {
                $this->pageId = Pages::create([
                    'page_url' => $this->pageUrl,
                    'page_title' => $this->pageTitle,
                    'page_text' => $this->pageText,
                    'page_count' => 0,
                    'page_img' => $img_title,
                    'page_user_id' => Auth::user()->id,
                ])->page_id;
            } else {
                if (!$this->pageImg) {
                    $img_title = $this->pageImgTemp;
                }
                $data = Pages::find($this->pageId);
                $data->update([
                    'page_url' => $this->pageUrl,
                    'page_title' => $this->pageTitle,
                    'page_text' => $this->pageText,
                    'page_img' => $img_title
                ]);
            }

            if ($this->pageImg && $this->type == "update") {
                // Storage::disk('public')->delete("artist/artist-" . $this->headerId ."/". $backup_img_temp . ".jpg");
                // Storage::disk('public')->delete("artist/artist-" . $this->headerId ."/". $backup_img_temp . "_300.jpg");
                Storage::disk('public')->deleteDirectory("pageThumbnail/page-" . $this->pageId);
            }

            if ($this->pageImg) {
                // Store Original Image width 1200px
                //$this->photo->storePubliclyAs("carousel_header/".$this->email, $photo_title.'.jpg');
                $img_default = Image::make($this->pageImg);
                $img_default->resize(null, 1200, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg', 50);
                $img_default->stream(); // <-- Key point
                Storage::disk('public')->put("pageThumbnail/page-"  . $this->pageId . "/" . $img_title . '.jpg', $img_default, 'public');
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                //Store Image with width 300px
                $img_resize = Image::make($this->pageImg);
                $img_resize->resize(null, 300, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg', 50);
                $img_resize->stream(); // <-- Key point
                Storage::disk('public')->put("pageThumbnail/page-" . $this->pageId . "/" . $img_title . '_300.jpg', $img_resize, 'public');
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Success Insert User']);
            }

            if ($this->type == "create") {
                // $this->reset();
                $this->pageImg = null;
                $this->pageUrl = "";
                $this->pageTitle = "";
                $this->pageText = "";
            }

            // session()->flash('message', 'Data carousel successfully updated');
            $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Data Updated Successfully']);
            DB::commit();
        } catch (\Throwable $th) {
            Log::debug("Add/Update Pages Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function render()
    {
        return view('livewire.masterdata.page-form');
    }
}
