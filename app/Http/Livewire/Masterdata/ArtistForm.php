<?php

namespace App\Http\Livewire\Masterdata;

use App\Models\Artist;

use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ArtistForm extends Component
{
    use WithFileUploads;

    public $type;
    public $rules;

    public $artistId;
    public $artistName;

    // public function updated($propertyName)
    // {
    //     $this->validateOnly($propertyName);
    // }

    // public function updatedImg()
    // {
    //     $this->validateOnly("img");
    // }

    public function submit()
    {
        $this->validate();

        DB::beginTransaction();
        try {

            if ($this->type == "create") {
                Artist::create([
                    'artist_name' => $this->artistName,
                    'artist_img' => '-',
                    'artist_count' => 0,
                    'artist_desc' => '-',
                    'artist_popularity' => 0
                ]);
            } else {
                $data = Artist::find($this->artistId);
                $data->update([
                    'artist_name' => $this->artistName
                ]);
            }

            if ($this->type == "create") {
                $this->artistName = "";
            }
            // session()->flash('message', 'Data carousel successfully updated');
            $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Data Updated Successfully']);
            DB::commit();
        } catch (\Throwable $th) {
            Log::debug("Add/Updated Artist Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function render()
    {
        return view('livewire.masterdata.artist-form');
    }
}
