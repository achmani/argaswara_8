<?php

namespace App\Http\Livewire\Masterdata;

use App\Models\Music;
use App\Models\MusicGenre;
use App\Models\MusicLabel;
use App\Models\MusicArtist;
use App\Models\MusicComposer;

use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class MusicForm extends Component
{
    use WithFileUploads;

    public $type;

    public $music;
    public $musicTemp;
    public $musicId;
    public $musicName;
    public $musicRelease;
    public $musicJoox;
    public $musicItunes;
    public $musicSpotify;
    public $musicYoutube;

    public $artistId;
    public $artistName;

    public $composerId;
    public $composerName;

    public $genreId;
    public $genreValue;

    public $labelId;
    public $labelValue;

    public $rules;

    public function submit()
    {
        $this->validate();

        $music_title = Str::random(100);

        // Execution doesn't reach here if validation fails.

        DB::beginTransaction();
        try {
            Log::debug($this->musicName);
            if ($this->type == "create") {
                $this->musicId = Music::create([
                    'music_name' => $this->musicName,
                    'music_release' => $this->musicRelease,
                    'music_path'  => "-",
                    'music_url'  => $music_title,
                    'music_is_file' => 1,
                    'music_count' => 0,
                    'music_popularity' => 0,
                    'music_joox' => $this->musicJoox,
                    'music_itunes' => $this->musicItunes,
                    'music_spotify' => $this->musicSpotify,
                    'music_youtube' => $this->musicYoutube,
                ])->music_id;
            } else {

                if (!$this->music) {
                    $music_title = $this->musicTemp;
                }

                $data_update = Music::find($this->musicId);
                $data_update->update([
                    'music_name' => $this->musicName,
                    'music_release' => $this->musicRelease,
                    'music_path' => "-",
                    'music_url' => $music_title,
                    'music_joox' => $this->musicJoox,
                    'music_itunes' => $this->musicItunes,
                    'music_spotify' => $this->musicSpotify,
                    'music_youtube' => $this->musicYoutube,
                ]);

                if ($this->music) {
                    Storage::disk('public')->delete("musics/" . $this->musicTemp . ".mp3");
                }
            }

            MusicGenre::where('music_id', $this->musicId)->delete();
            // $data = explode(",", $this->genreId);
            foreach ($this->genreId as $key => $value) {
                Log::debug($value);
                MusicGenre::create([
                    'music_id' => $this->musicId,
                    'genre_id' => $value
                ]);
            }

            MusicLabel::where('music_id', $this->musicId)->delete();
            // $data = explode(",", $this->genreId);
            foreach ($this->labelId as $key => $value) {
                Log::debug($value);
                MusicLabel::create([
                    'music_id' => $this->musicId,
                    'label_id' => $value
                ]);
            }

            MusicArtist::where('music_id', $this->musicId)->delete();
            // $data = explode(",", $this->artistId);
            if ($this->artistId) {
                foreach ($this->artistId as $key => $value) {
                    MusicArtist::create([
                        'music_id' => $this->musicId,
                        'artist_id' => $value
                    ]);
                }
            }

            MusicComposer::where('music_id', $this->musicId)->delete();
            // $data = explode(",", $this->composerId);
            foreach ($this->composerId as $key => $value) {
                MusicComposer::create([
                    'music_id' => $this->musicId,
                    'composer_id' => $value
                ]);
            }
            // MusicComposer::create([
            //     'music_id' => $this->musicId,
            //     'composer_id' => $this->composerId
            // ]);

            if ($this->music) {
                // Storage::disk('public')->put("musics/" . $music_title . '.mp3', $this->music, 'public');
                $this->music->storeAs('public/musics', $music_title. '.mp3');
            }

            $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Data Updated Successfully']);
            DB::commit();
        } catch (\Throwable $th) {
            Log::debug("Add Music Error");
            Log::debug($th);
            DB::rollBack();
            $this->emit('submit', ['type' => 'error', 'title' => 'Error', 'message' => 'Error operate data']);
        }
    }

    public function render()
    {
        return view('livewire.masterdata.music-form');
    }
}
