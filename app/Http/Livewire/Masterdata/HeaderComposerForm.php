<?php

namespace App\Http\Livewire\Masterdata;

use App\Models\HeaderComposer;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class HeaderComposerForm extends Component
{

    public $type;
    public $rules;

    public $headerComposerId;
    public $composerId;
    public $composerSequence;

    public function submit()
    {
        $this->validate();

        DB::beginTransaction();
        try {

            if ($this->type == "create") {
                HeaderComposer::create([
                    'composer_id' => $this->composerId,
                    'composer_sequence' => $this->composerSequence
                ]);
            } else {
                $data = HeaderComposer::find($this->headerComposerId);
                $data->update([
                    'composer_sequence' => $this->composerSequence
                ]);
            }

            if ($this->type == "create") {
                $this->composerId = "";
                $this->composerSequence = "";
            }else if($this->type == "update"){
                $this->composerSequence = "";
            }
            // session()->flash('message', 'Data carousel successfully updated');
            $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Data Updated Successfully']);
            DB::commit();
        } catch (\Throwable $th) {
            Log::debug("Add/Updated Header Composer Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function render()
    {
        return view('livewire.masterdata.header-composer-form');
    }
}
