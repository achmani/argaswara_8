<?php

namespace App\Http\Livewire\Masterdata;

use App\Models\Composers;

use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ComposerForm extends Component
{
    use WithFileUploads;

    public $type;
    public $rules;

    public $composerId;
    public $composerName;
    public $composerPopularity;
    public $composerDesc;
    public $composerImg;
    public $composerImgTemp;
    
    public $labelId;
    public $labelValue;

    // public function updated($propertyName)
    // {
    //     $this->validateOnly($propertyName);
    // }

    // public function updatedImg()
    // {
    //     $this->validateOnly("img");
    // }

    public function submit()
    {
        $this->validate();

        $img_title = Str::random(100);

        // Execution doesn't reach here if validation fails.

        DB::beginTransaction();
        try {

            if ($this->type == "create") {
                $this->composerId = Composers::create([
                    'composer_name' => $this->composerName,
                    'composer_img' => $img_title,
                    'composer_count' => 0,
                    'composer_desc' => $this->composerDesc,
                    'composer_popularity' => $this->composerPopularity,
                    'composer_label' => $this->labelId
                ])->composer_id;
            } else {
                if (!$this->composerImg) {
                    $img_title = $this->composerImgTemp;
                }
                $data = Composers::find($this->composerId);
                $backup_img_temp = $data->header_path;
                $data->update([
                    'composer_name' => $this->composerName,
                    'composer_img' => $img_title,
                    'composer_desc' => $this->composerDesc,
                    'composer_popularity' => $this->composerPopularity,
                    'composer_label' => $this->labelId
                ]);
            }

            if ($this->composerImg && $this->type == "update") {
                // Storage::disk('public')->delete("composer/composer-" . $this->headerId ."/". $backup_img_temp . ".jpg");
                // Storage::disk('public')->delete("composer/composer-" . $this->headerId ."/". $backup_img_temp . "_300.jpg");
                Storage::disk('public')->deleteDirectory("composer/composer-" . $this->composerId);
            }

            if ($this->composerImg) {
                // Store Original Image width 1200px
                //$this->photo->storePubliclyAs("carousel_header/".$this->email, $photo_title.'.jpg');
                $img_default = Image::make($this->composerImg);
                $img_default->resize(null, 1200, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg', 50);
                $img_default->stream(); // <-- Key point
                Storage::disk('public')->put("composer/composer-"  . $this->composerId ."/". $img_title . '.jpg', $img_default, 'public');
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                //Store Image with width 300px
                $img_resize = Image::make($this->composerImg);
                $img_resize->resize(null, 300, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg', 50);
                $img_resize->stream(); // <-- Key point
                Storage::disk('public')->put("composer/composer-" . $this->composerId ."/". $img_title . '_300.jpg', $img_resize, 'public');
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Success Insert User']);
            }
            if ($this->type == "create") {
                // $this->reset();
                $this->composerImg = null;
                $this->composerDesc = "";
                $this->composerName = "";
                $this->composerPopularity = "";
                $this->labelId = "";
            }
            // session()->flash('message', 'Data carousel successfully updated');
            $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Data Updated Successfully']);
            DB::commit();
        } catch (\Throwable $th) {
            Log::debug("Add Composer Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function render()
    {
        return view('livewire.masterdata.composer-form');
    }
}
