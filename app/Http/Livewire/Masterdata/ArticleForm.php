<?php

namespace App\Http\Livewire\Masterdata;

use App\Models\Article;
use App\Models\ArticleComposer;

use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ArticleForm extends Component
{
    use WithFileUploads;

    public $type;
    public $rules;

    public $articleId;
    public $articleImg;
    public $articleImgTemp;
    public $articleUrl;
    public $articleTitle;
    public $articleText;
    public $composerId;

    public function submit()
    {
        $this->validate();

        $img_title = Str::random(100);

        // Execution doesn't reach here if validation fails.

        DB::beginTransaction();
        try {

            if ($this->type == "create") {
                $this->articleId = Article::create([
                    'article_url' => $this->articleUrl,
                    'article_title' => $this->articleTitle,
                    'article_text' => $this->articleText,
                    'article_count' => 0,
                    'article_img' => $img_title,
                    'article_user_id' => Auth::user()->id,
                ])->article_id;
            } else {
                if (!$this->articleImg) {
                    $img_title = $this->articleImgTemp;
                }
                $data = Article::find($this->articleId);
                $data->update([
                    'article_url' => $this->articleUrl,
                    'article_title' => $this->articleTitle,
                    'article_text' => $this->articleText,
                    'article_img' => $img_title
                ]);
            }

            if ($this->articleImg && $this->type == "update") {
                // Storage::disk('public')->delete("artist/artist-" . $this->headerId ."/". $backup_img_temp . ".jpg");
                // Storage::disk('public')->delete("artist/artist-" . $this->headerId ."/". $backup_img_temp . "_300.jpg");
                Storage::disk('public')->deleteDirectory("articleThumbnail/article-" . $this->articleId);
            }

            ArticleComposer::where('article_id', $this->articleId)->delete();
            // $data = explode(",", $this->genreId);
            foreach ($this->composerId as $key => $value) {
                Log::debug($value);
                ArticleComposer::create([
                    'article_id' => $this->articleId,
                    'composer_id' => $value
                ]);
            }

            if ($this->articleImg) {
                // Store Original Image width 1200px
                //$this->photo->storePubliclyAs("carousel_header/".$this->email, $photo_title.'.jpg');
                $img_default = Image::make($this->articleImg);
                $img_default->resize(null, 1200, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg', 50);
                $img_default->stream(); // <-- Key point
                Storage::disk('public')->put("articleThumbnail/article-"  . $this->articleId . "/" . $img_title . '.jpg', $img_default, 'public');
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                //Store Image with width 300px
                $img_resize = Image::make($this->articleImg);
                $img_resize->resize(null, 300, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg', 50);
                $img_resize->stream(); // <-- Key point
                Storage::disk('public')->put("articleThumbnail/article-" . $this->articleId . "/" . $img_title . '_300.jpg', $img_resize, 'public');
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Success Insert User']);
            }

            if ($this->type == "create") {
                // $this->reset();
                $this->articleImg = null;
                $this->articleUrl = "";
                $this->articleTitle = "";
                $this->articleText = "";
            }

            // session()->flash('message', 'Data carousel successfully updated');
            $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Data Updated Successfully']);
            DB::commit();
        } catch (\Throwable $th) {
            Log::debug("Add/Update Article Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function render()
    {
        return view('livewire.masterdata.article-form');
    }
}
