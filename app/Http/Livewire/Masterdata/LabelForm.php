<?php

namespace App\Http\Livewire\Masterdata;

use App\Models\Label;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LabelForm extends Component
{

    public $type;
    public $rules;

    public $labelId;
    public $labelValue;

    public function submit()
    {
        $this->validate();

        DB::beginTransaction();
        try {

            if ($this->type == "create") {
                Label::create([
                    'label_value' => $this->labelValue
                ]);
            } else {
                $data = Label::find($this->labelId);
                $data->update([
                    'label_value' => $this->labelValue
                ]);
            }

            if ($this->type == "create") {
                $this->labelValue = "";
            }
            // session()->flash('message', 'Data carousel successfully updated');
            $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Data Updated Successfully']);
            DB::commit();
        } catch (\Throwable $th) {
            Log::debug("Add/Updated Label Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function render()
    {
        return view('livewire.masterdata.label-form');
    }
}
