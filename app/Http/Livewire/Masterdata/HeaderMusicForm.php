<?php

namespace App\Http\Livewire\Masterdata;

use App\Models\HeaderMusic;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class HeaderMusicForm extends Component
{

    public $type;
    public $rules;

    public $headerMusicId;
    public $musicId;
    public $musicSequence;

    public function submit()
    {
        $this->validate();

        DB::beginTransaction();
        try {

            if ($this->type == "create") {
                HeaderMusic::create([
                    'music_id' => $this->musicId,
                    'music_sequence' => $this->musicSequence
                ]);
            } else {
                $data = HeaderMusic::find($this->headerMusicId);
                $data->update([
                    'music_sequence' => $this->musicSequence
                ]);
            }

            if ($this->type == "create") {
                $this->musicId = "";
                $this->musicSequence = "";
            }else if($this->type == "update"){
                $this->musicSequence = "";
            }
            // session()->flash('message', 'Data carousel successfully updated');
            $this->emit('submit', ['type' => 'success', 'title' => 'Success', 'message' => 'Data Updated Successfully']);
            DB::commit();
        } catch (\Throwable $th) {
            Log::debug("Add/Updated Header Music Error");
            Log::debug($th);
            DB::rollBack();
        }
    }

    public function render()
    {
        return view('livewire.masterdata.header-music-form');
    }
}
