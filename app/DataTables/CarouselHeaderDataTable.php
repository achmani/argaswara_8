<?php

namespace App\DataTables;

use App\Models\CarouselHeader;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class CarouselHeaderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        // $button = "";
        // $button = $button.'<a href="#" class="btn-datatables-icon btn btn-sm btn-primary"><i class="fas fa-pencil-alt"></i></a>';
        // $button = $button.'<a href="#" class="btn-datatables-icon btn btn-sm btn-danger"><i class="fa fa-times"></i></a>';
        // $button = $button.'<a href="#" class="btn-datatables-icon btn btn-sm btn-success"><i class="fa fa-list"></i></a>';
        return datatables()
            ->eloquent($query)
            ->editColumn('updated_at', function ($data) {
                return $data->updated_at->format('Y-m-d H:i:s');
            })
            ->editColumn('created_at', function ($data) {
                return $data->updated_at->format('Y-m-d H:i:s');
            })
            //->addColumn('action', $button)
            ->addColumn('action', function ($data) {
                $button = "";
                $button = $button . '<a href="' . route('carousel-update', [$data->sponsor_id]) . '" class="btn-datatables-icon btn btn-sm btn-primary"><i class="fas fa-pencil-alt"></i></a>';
                $button = $button . '<a href="#" data-id="' . $data->sponsor_id . '" class="btn-datatables-icon btn-delete btn btn-sm btn-danger"><i class="fa fa-times"></i></a>';
                $button = $button . '<a href="' . route('carousel-read', [$data->sponsor_id]) . '" class="btn-datatables-icon btn btn-sm btn-success"><i class="fa fa-list"></i></a>';
                return $button;
            }); //->rawColumns(['action', 'created_at']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\CarouselHeader $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(CarouselHeader $model)
    {
        return $model->newQuery()->select('header_id as sponsor_id','header_title as sponsor_title', 'header_link as sponsor_link', 'created_at', 'updated_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('data-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            //->dom('Bfrtip')
            ->responsive(true)
            ->autoWidth(false)
            ->orderBy(1);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
            Column::make('sponsor_id'),
            Column::make('sponsor_title'),
            Column::make('sponsor_link'),
            Column::make('created_at'),
            Column::make('updated_at'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'CarouselHeader_' . date('YmdHis');
    }
}
