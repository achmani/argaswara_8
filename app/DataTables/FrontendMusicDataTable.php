<?php
namespace App\DataTables;

use Illuminate\Support\Carbon;
use App\Models\Music;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class FrontendMusicDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        // $button = "";
        // $button = $button.'<a href="#" class="btn-datatables-icon btn btn-sm btn-primary"><i class="fas fa-pencil-alt"></i></a>';
        // $button = $button.'<a href="#" class="btn-datatables-icon btn btn-sm btn-danger"><i class="fa fa-times"></i></a>';
        // $button = $button.'<a href="#" class="btn-datatables-icon btn btn-sm btn-success"><i class="fa fa-list"></i></a>';
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($data) {
                $button = "";
                $button = $button.'<a href="'.route('music-update',[$data->music_id]).'" class="btn-datatables-icon btn btn-sm btn-primary"><i class="fas fa-pencil-alt"></i></a>';
                $button = $button.'<a href="#" data-id="'.$data->music_id.'" class="btn-datatables-icon btn-delete btn btn-sm btn-danger"><i class="fa fa-times"></i></a>';
                $button = $button.'<a href="'.route('music-read',[$data->music_id]).'" class="btn-datatables-icon btn btn-sm btn-success"><i class="fa fa-list"></i></a>';
                return $button;
            })
            ->editColumn('composer', function ($data) {
                if(count($data->composer) > 0){
                    $link="";
                    foreach ($data->composer as $key => $value) {
                        if($value->composer_label == 6){
                            $link = $link.'<a class="text-dark" href="'.route('player', str_replace(' ', '-',$value->composer_name)).'">'.$value->composer_name.'</a>, ';   
                        }else{
                            $link = $link.'<span class="text-dark">'.$value->composer_name.'</span>, ';   
                        }
                    }
                    $link = rtrim($link, ', ');
                }else{
                    $link = "";
                }
                return $link;
            })
            ->addColumn('song_title', function ($data) {
                if(count($data->composer) > 0){
                    $link = '<a class="text-dark" href="'.route('player', [ str_replace(' ', '-',$data->composer[0]->composer_name), $data->music_id] ).'">'.$data->music_name.'</a>';
                }else{
                    $link = "";
                }
                return $link;
            })
            // ->editColumn('composer', function ($data) {
            //     $temp="";
            //     $composer =  $data->composer;
            //     foreach ($composer as $key => $value) {
            //         $temp = $value->composer_name.", ".$temp;
            //     }
            //     return rtrim($temp, ', ');
            // })
            ->editColumn('release', function ($data) {
               return date('Y', strtotime($data->music_release));
            })
            ->editColumn('performer', function ($data) {
                $temp="";
                $artist =  $data->artist;
                foreach ($artist as $key => $value) {
                    $temp = $value->artist_name.", ".$temp;
                }
                return rtrim($temp, ', ');
            })
            ->editColumn('genre', function ($data) {
                $temp="";
                $genre =  $data->genre;
                foreach ($genre as $key => $value) {
                    $temp = $value->genre_value.", ".$temp;
                }
                return rtrim($temp, ', ');
            })
            ->rawColumns(['music_name','composer','song_title']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Music $model)
    {
        return $model->newQuery()->with('composer')->with('artist')->with('genre');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('data-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->responsive(true)
                    ->autoWidth(true)
                    ->orderBy(0,'asc');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('song_title')->name("music_name"),
            Column::make('performer')->name('artist.artist_name')->orderable(false),
            Column::make('composer')->name('composer.composer_name')->orderable(false),
            Column::make('genre')->name('genre.genre_value')->orderable(false),
            Column::make('release')->name('music_release'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
