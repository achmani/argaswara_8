<?php
namespace App\DataTables;

use Illuminate\Support\Carbon;
use App\Models\Music;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MusicDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        // $button = "";
        // $button = $button.'<a href="#" class="btn-datatables-icon btn btn-sm btn-primary"><i class="fas fa-pencil-alt"></i></a>';
        // $button = $button.'<a href="#" class="btn-datatables-icon btn btn-sm btn-danger"><i class="fa fa-times"></i></a>';
        // $button = $button.'<a href="#" class="btn-datatables-icon btn btn-sm btn-success"><i class="fa fa-list"></i></a>';
        return datatables()
            ->eloquent($query)
            ->editColumn('updated_at', function ($data) {
                return $data->updated_at->format('Y-m-d H:i:s');
            })
            ->editColumn('created_at', function ($data) {
                return $data->updated_at->format('Y-m-d H:i:s');
            })
            //->addColumn('action', $button)
            ->addColumn('action', function ($data) {
                $button = "";
                $button = $button.'<a href="'.route('music-update',[$data->music_id]).'" class="btn-datatables-icon btn btn-sm btn-primary"><i class="fas fa-pencil-alt"></i></a>';
                $button = $button.'<a href="#" data-id="'.$data->music_id.'" class="btn-datatables-icon btn-delete btn btn-sm btn-danger"><i class="fa fa-times"></i></a>';
                $button = $button.'<a href="'.route('music-read',[$data->music_id]).'" class="btn-datatables-icon btn btn-sm btn-success"><i class="fa fa-list"></i></a>';
                return $button;
            })
            ->editColumn('composer', function ($data) {
                $temp="";
                $composer =  $data->composer;
                foreach ($composer as $key => $value) {
                    $temp = $value->composer_name.", ".$temp;
                }
                return rtrim($temp, ', ');
            })
            ->editColumn('artist', function ($data) {
                $temp="";
                $artist =  $data->artist;
                foreach ($artist as $key => $value) {
                    $temp = $value->artist_name.", ".$temp;
                }
                return rtrim($temp, ', ');
            })
            ->editColumn('genre', function ($data) {
                $temp="";
                $genre =  $data->genre;
                foreach ($genre as $key => $value) {
                    $temp = $value->genre_value.", ".$temp;
                }
                return rtrim($temp, ', ');
            })
            ;//->rawColumns(['action', 'created_at']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Music $model)
    {
        return $model->newQuery()->with('composer')->with('artist')->with('genre');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('data-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->responsive(true)
                    ->autoWidth(false)
                    ->orderBy(1);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(50)
                  ->addClass('text-center'),
            Column::make('music_id'),
            Column::make('music_name'),
            Column::make('music_release'),
            Column::make('artist')->name('artist.artist_name'),
            Column::make('composer')->name('composer.composer_name'),
            Column::make('genre')->name('genre.genre_value'),
            Column::make('created_at'),
            Column::make('updated_at'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
