<?php

namespace App\DataTables;

use App\Models\Composers;
use Illuminate\Support\Composer;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ComposersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        // $button = "";
        // $button = $button.'<a href="#" class="btn-datatables-icon btn btn-sm btn-primary"><i class="fas fa-pencil-alt"></i></a>';
        // $button = $button.'<a href="#" class="btn-datatables-icon btn btn-sm btn-danger"><i class="fa fa-times"></i></a>';
        // $button = $button.'<a href="#" class="btn-datatables-icon btn btn-sm btn-success"><i class="fa fa-list"></i></a>';
        return datatables()
            ->eloquent($query)
            ->editColumn('updated_at', function ($data) {
                return $data->updated_at->format('Y-m-d H:i:s');
            })
            ->editColumn('created_at', function ($data) {
                return $data->updated_at->format('Y-m-d H:i:s');
            })
            //->addColumn('action', $button)
            ->addColumn('action', function ($data) {
                $button = "";
                $button = $button . '<a href="' . route('composer-update', [$data->composer_id]) . '" class="btn-datatables-icon btn btn-sm btn-primary"><i class="fas fa-pencil-alt"></i></a>';
                $button = $button . '<a href="#" data-id="' . $data->composer_id . '" class="btn-datatables-icon btn-delete btn btn-sm btn-danger"><i class="fa fa-times"></i></a>';
                $button = $button . '<a href="' . route('composer-read', [$data->composer_id]) . '" class="btn-datatables-icon btn btn-sm btn-success"><i class="fa fa-list"></i></a>';
                return $button;
            }); //->rawColumns(['action', 'created_at']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\ComposersDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Composers $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('data-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            //->dom('Bfrtip')
            ->responsive(true)
            ->autoWidth(false)
            ->orderBy(1);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
            Column::make('composer_id'),
            Column::make('composer_name'),
            Column::make('composer_count'),
            Column::make('composer_popularity'),
            Column::make('created_at'),
            Column::make('updated_at'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Composers_' . date('YmdHis');
    }
}
