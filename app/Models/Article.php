<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Searchable;

class Article extends Model
{
    use HasFactory;
    use Searchable;

    public $searchable = ['article_url', 'article_title', 'article_text'];

    protected $table = 'm_articles';

    protected $primaryKey = 'article_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_url',
        'article_img',
        'article_title',
        'article_text',
        'article_user_id',
        'article_count'
    ];

    public function composer()
    {
        return $this->belongsToMany(
            'App\Models\Composers',
            'App\Models\ArticleComposer',
            'article_id',
            'composer_id'
        );
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'article_user_id');
    }
}
