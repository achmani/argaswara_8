<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HeaderComposer extends Model
{
    use HasFactory;

    protected $table = 'header_composer';

    protected $primaryKey = 'header_composer_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'header_composer_id',
        'composer_id',
        'composer_sequence'
    ];

    public function composer()
    {
        return $this->belongsTo('App\Models\Composers','composer_id');
    }

}
