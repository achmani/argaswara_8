<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Searchable;

class Artist extends Model
{
    use HasFactory;
    use Searchable;

    public $searchable = ['artist_id', 'artist_name', 'artist_desc'];

    protected $table = 'm_artists';

    protected $primaryKey = 'artist_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'artist_id',
        'artist_name',
        'artist_img',
        'artist_id',
        'artist_count',
        'artist_desc',
        'artist_popularity'
    ];
}
