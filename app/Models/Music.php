<?php

namespace App\Models;

use App\Casts\UrlPlay;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Searchable;

class Music extends Model
{
    use HasFactory;
    use Searchable;

    public $searchable = ['music_name'];

    protected $table = 'm_musics';

    protected $primaryKey = 'music_id';
    
    protected $casts = [
        'url' => UrlPlay::class,
        'composerConcat' => StringConcat::class
    ];

    public function composer()
    {
        return $this->belongsToMany(
            'App\Models\Composers',
            'App\Models\MusicComposer',
            'music_id',
            'composer_id'
        );
    }

    public function artist()
    {
        return $this->belongsToMany(
            'App\Models\Artist',
            'App\Models\MusicArtist',
            'music_id',
            'artist_id'
        );
    }

    public function genre()
    {
        return $this->belongsToMany(
            'App\Models\Genre',
            'App\Models\MusicGenre',
            'music_id',
            'genre_id'
        );
    }

    public function label()
    {
        return $this->belongsToMany(
            'App\Models\Label',
            'App\Models\MusicLabel',
            'music_id',
            'label_id'
        );
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'music_id',
        'music_name',
        'music_is_file',
        'music_path',
        'music_url',
        'music_release',
        'music_count',
        'music_popularity',
        'music_joox',
        'music_itunes',
        'music_spotify',
        'music_youtube',
    ];
}
