<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Searchable;

class Genre extends Model
{
    use HasFactory;
    use Searchable;

    public $searchable = ['genre_id', 'genre_value'];

    protected $table = 'm_genres';

    protected $primaryKey = 'genre_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'genre_id',
        'genre_value'
    ];
}
