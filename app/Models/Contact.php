<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Searchable;

class Contact extends Model
{
    use HasFactory;
    use Searchable;

    public $searchable = ['contact_id', 'contact_name', 'contact_email', 'contact_subject', 'contact_handphone', 'contact_text'];

    protected $table = 'contacts';

    protected $primaryKey = 'contact_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contact_id', 
        'contact_name', 
        'contact_email', 
        'contact_subject', 
        'contact_handphone', 
        'contact_text',
        'contact_read',
    ];
}
