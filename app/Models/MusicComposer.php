<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MusicComposer extends Model
{
    use HasFactory;

    protected $table = 'm_musics_composers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'music_id',
        'composer_id'
    ];
}
