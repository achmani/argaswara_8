<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarouselHeader extends Model
{
    use HasFactory;

    protected $table = 'm_header_carousel';

    protected $primaryKey = 'header_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'header_id',
        'header_path',
        'header_link',
        'header_title'
    ];
}
