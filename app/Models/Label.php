<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Searchable;

class Label extends Model
{
    use HasFactory;
    use Searchable;

    public $searchable = ['label_id', 'label_value'];

    protected $table = 'm_labels';

    protected $primaryKey = 'label_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label_id',
        'label_value'
    ];
}
