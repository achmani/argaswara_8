<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Searchable;

class Pages extends Model
{
    use HasFactory;
    use Searchable;

    public $searchable = ['page_url', 'page_title', 'page_text'];

    protected $table = 'm_pages';

    protected $primaryKey = 'page_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_url',
        'page_img',
        'page_title',
        'page_text',
        'page_user_id',
        'page_count'
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'article_user_id');
    }
    
}
