<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HeaderMusic extends Model
{
    use HasFactory;

    protected $table = 'header_music';

    protected $primaryKey = 'header_music_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'header_music_id',
        'music_id',
        'music_sequence'
    ];

    public function music()
    {
        return $this->belongsTo('App\Models\Music','music_id');
    }

}
