<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Searchable;
use App\Casts\StringConcat;
use App\Casts\ComposerLink;

class Composers extends Model
{
    use HasFactory;
    use Searchable;

    public $searchable = ['composer_id', 'composer_img', 'composer_name', 'composer_desc'];

    protected $casts = [
        'url' => UrlPlay::class,
        'composer_link' => ComposerLink::class,
        'artist' => StringConcat::class
    ];

    protected $table = 'm_composers';

    protected $primaryKey = 'composer_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'composer_id',
        'composer_img',
        'composer_name',
        'composer_label',
        'composer_count',
        'composer_desc',
        'composer_popularity'
    ];
}
